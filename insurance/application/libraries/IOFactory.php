<?php
/**
 * Created by PhpStorm.
 * User: Amar Moufti
 * Date: 03.05.2018
 * Time: 10:53
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require_once ('PHPExcel/IOFactory.php');

class IOFactory extends PHPExcel_IOFactory {

    /**
     * Excel constructor.
     */
    public function __construct() {
        parent::__construct();

    }
}
?>