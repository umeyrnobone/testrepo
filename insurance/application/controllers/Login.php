<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
        $this->load->helper('crm_helper');

    }


	public function index() {
		if ($this->session->has_userdata('username_session')) {
            redirect('Dashboard');
		}
		$user_data = $this->User_model->get_duplicate_entries();

		$this->load->view('login/index');
	}

	public function my_cryption ( $string, $action = 'e', $code = 'standart') {

		switch ( $code ) {
			case "standart":
				$secret_key = 'thisIsmySecretKey:)';
				$secret_iv = 'thisIsmySecretIv:)';
				break;
			case 'hc_login':
				$secret_key = 'cre@t€L0g!nK€y123)';
				$secret_iv = 'cre@t€L0g!n!V123)';
				break;

			default:
				$secret_key = 'thisIsmySecretKey:)';
				$secret_iv = 'thisIsmySecretIv:)';
		}

		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

		if( $action == 'e' ) {
			$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
		}

		else if( $action == 'd' ){
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}

		return $output;
	}

	public function verifyUser() {

		$username = $this->form_validation->set_rules('username', 'Username', 'trim|required');
		$password = $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|max_length[12]');

		if ($this->form_validation->run() == FALSE) {

			$data = array(
				'errors' => validation_errors()
			);

			$this->session->set_flashdata($data);

            $this->template['content'] = $this->load->view('login/index', $data='', TRUE);
            $this->load->view('templates/main', $this->template);

		} else {


			$username = $this->input->post('username');
		    $password = $this->my_cryption($this->input->post('password'), 'e');

			if( $user = $this->User_model->check_validation($username, $password) ) {

				$this->session->set_userdata('username_session', $username);
				$this->session->set_userdata('user_type_session', $user->user_type);
				$this->session->set_userdata('user_id_session', $user->user_id );
				$this->session->set_userdata('user_fname_session', $user->user_firstname );
				$this->session->set_userdata('user_lname_session', $user->user_lastname );
				$this->session->set_userdata('user_avatar', $user->user_avatar );

				$message  = 'As salamu alaikum wa rahmatullahi wa barakatuh,<br><br>';
				$message .= 'Du erhältst diese Email, weil sich jemand mit Deinem Konto im CRM angemeldet hat.<br>';
				$message .= 'Solltest Du dies nicht gewesen sein, melde Dich bitte beim Systemadministrator. Im anderen Fall<br>';
				$message .= 'kannst Du diese Email ignorieren<br><br>';
				$message .= 'As salamu alaikum wa rahmatullahi wa barakatuh!<br><br>';

				$config = array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.gmail.com',
					'smtp_port' => '465',
					'charset' => 'utf-8',
					'wordwrap' => 'TRUE',
					'IsHTML' => 'TRUE',
					'mailtype' => 'html',
					'newline' => '\r\n',
					'smtp_user' => 'halalcheck4u@gmail.com',
					'smtp_pass' => 'help2hadj2019'
				);

				$this->load->library('email', $config);

				$this->email->set_newline("\r\n");
				$this->email->from('info@halalcheck4u.de', 'Halalcheck4u'); // change it to yours
				$this->email->to($user->user_email);// change it to yours
				$this->email->subject('Hast Du Dich soeben eingeloggt?');
				$this->email->message($message);
				$this->email->send();

				$this->User_model->update_login_date($username);
				redirect("Dashboard", 'refresh');

            } else {
				$data['error'] = '';
				$this->load->view('login/index', $data);
			}

		}

	}

	public function logout() {
		$this->session->unset_userdata('username_session');
		$this->session->unset_userdata('user_type_session');
		$this->session->unset_userdata('user_id_session');
		session_destroy();
		redirect('Login', 'refresh');
	}
}