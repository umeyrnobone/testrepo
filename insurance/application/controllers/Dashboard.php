<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
        $this->load->helper('crm_helper');
        $this->load->model('Client_model');
	}


    public function index() {
		$new_date = new DateTime();
		date_sub($new_date, date_interval_create_from_date_string("1 week"));

		$data['clients'] 		= $this->Client_model->get_insurance_clients();
		$data['new_clients'] 	= $this->Client_model->get_new_insurance_clients($new_date);
		$data['status']			= $this->Client_model->get_insurance_status_list();

		$c = -1;
		foreach($data['clients'] as $client){
			$data['clients'][++$c] = $client;
			$data['clients'][$c]->existing_insurances = $this->Client_model->get_client_advice_insurances($client->insurance_data_client_id);
		}
        $this->template['content']  = $this->load->view('dashboard/show_dashboard', $data, TRUE);
		$this->load->view('templates/main', $this->template);

	}

	public function show_clients() {

		$data['clients'] 		= $this->Client_model->get_insurance_clients();
		$data['status']			= $this->Client_model->get_insurance_status_list();

		$c = -1;
		foreach($data['clients'] as $client){
			$data['clients'][++$c] = $client;
			$data['clients'][$c]->existing_insurances = $this->Client_model->get_client_advice_insurances($client->insurance_data_client_id);
		}

        $this->template['content']  = $this->load->view('clients/clients_list', $data, TRUE);
        $this->load->view('templates/main', $this->template);
    }

	public function show_client_details($client_id){
		$client_id = my_cryption($client_id, 'd', 'hco_url');

		$data['client_info'] 	= $this->Client_model->get_client_by_id($client_id);
		$data['insurances']		= $this->Client_model->get_client_insurances($client_id);
		$data['ext_notes']	 	= $this->Client_model->get_client_external_notes($client_id);
        $data['documents'] 		= $this->Client_model->get_client_attachments($client_id);

        $c = -1;
        foreach($data['insurances'] as $insurance){
			$data['insurances'][++$c] = $insurance;
			$data['insurances'][$c]->history = $this->Client_model->get_insurance_history($insurance->insurance_data_id);
		}

		if(!empty($data['insurances'])){
			$this->template['content']  = $this->load->view('clients/client_details', $data, TRUE);
			$this->load->view('templates/main', $this->template);
		} else{
			redirect('Dashboard');
		}
	}

	public function show_case_details($case_id){
		$case_id = my_cryption($case_id, 'd', 'hco_url');

		$client_data = $data['client'] 	=  $this->Client_model->get_case_by_id($case_id);

		$data['ext_notes']	= $this->Client_model->get_client_external_notes_by_type($client_data->client_id, 'lawyer');
		$data['documents']	= $this->Client_model->get_client_attachments($client_data->client_id, 'Anwalt');

		if(!empty($data['client'])){
			$this->template['content']  = $this->load->view('clients/general_client_details', $data, TRUE);
			$this->load->view('templates/main', $this->template);
		} else{
			redirect('Dashboard');
		}
	}

	public function update_insurance_status(){
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$insurance_id 	= my_cryption($this->input->post('insurance_id'), 'd', 'hco_login');
			$notes 			= $this->input->post('update_notes');
			$status			= $this->input->post('loan_status');
			$update_date	= new DateTime();
			$loan_status = $this->Client_model->get_insurance_status_by_id($status)->insurance_status_name;

			$designed_notes ='<tr class="loan_tr">
								<td class="loan_td">
									<h5 class="loan_author notes_author_data">'.$this->session->userdata("user_fname_session").' '.$this->session->userdata("user_lname_session").'</h5>
									<h6 class="loan_status notes_author_data">Status: <b>'.$loan_status.'</b>, am '.$update_date->format('d.m.Y, H:i').' Uhr <i class="fas fa-clock"></i></h6>
									<p class="loan_notes">'.$notes.'</p>
								</td>
							</tr>';

			$this->db->trans_begin();

			$update_data = array(
				'insurance_data_status_id'	=> $status,
			);
			$this->Client_model->update_insurance_data_by_id($insurance_id, $update_data);

			$insert_data = array(
				'insurance_history_message' 	=> '<b>'.$loan_status.'</b>',
				'insurance_history_insurance' 	=> $insurance_id,
				'insurance_history_notes'		=> $designed_notes,
				'insurance_history_status'		=> $status,
				'insurance_history_added_by'	=> $this->session->userdata('user_id_session')
			);
			$this->Client_model->insert_insurance_history($insert_data);

			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_notes(){
		$client_id 	= $this->input->post('client_id');
		$client_id  = my_cryption($client_id, 'd', 'hco_url');
		$notes 		= $this->input->post('notes');
		$type		= $this->input->post('type');

		if($type != ''){
			$type = my_cryption($type, 'd', 'hco_url');
		} else{
			$type = 'insurance';
		}

		$insert_data = array(
			'c_notes_external_text' 		=> $notes,
			'c_notes_external_client_id'	=> $client_id,
			'c_notes_external_added_by'		=> $this->session->userdata('user_id_session'),
			'c_notes_external_reason'		=> $type
		);
		$this->Client_model->insert_external_notes($insert_data);

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function upload_insurance_attachments($client_id){
		$client_id = my_cryption($client_id, 'd', 'hco_url');
		$target_dir = "documents/clients/";
		$target_file = $target_dir . basename($_FILES["f"]["document"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
			echo "File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			echo "File is not an image.";
			$uploadOk = 0;
		}
}
	}
	public function add_loan_documents($loan_id,$client_id){
		if (isset($_FILES['loan_documents']) && $_FILES['loan_documents']['name'][0] != '') {

			$count_images = $_FILES['loan_documents']['name'];
			if (isset($count_images)) {

				$length = count($count_images);
				$path = 'dokuments/clients/' . $client_id . '/loans'.'/'.$loan_id;
				if (!is_dir($path)) {
					mkdir($path, 0777, true);
				}

				for ($i = 0; $i < $length; $i++) {
					$name = str_replace(array(' ', 'ö', 'ü', 'ä', '.', '/', '-', '<', '>', '?', '!', pathinfo($_FILES['loan_documents']['name'][$i], PATHINFO_EXTENSION)), '', $_FILES['loan_documents']['name'][$i]).'.'.pathinfo($_FILES['loan_documents']['name'][$i], PATHINFO_EXTENSION);

					if (is_file($path . '/' . $name)) {
						unlink($path . '/' . $name);
						$this->Client_model->delete_double_attachs($path.'/'.$name);
					}
					$_FILES['file']['name'] = $name;
					$_FILES['file']['type'] = $_FILES['loan_documents']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['loan_documents']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['loan_documents']['error'][$i];
					$_FILES['file']['size'] = $_FILES['loan_documents']['size'][$i];
					$config['remove_spaces'] = true;
					$config['upload_path'] = $path;
					$config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
					$config['max_size'] = '7800000';
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('file')) {
						/*$upload_error = array('error' => $this->upload->display_errors());
						var_dump($upload_error);
						die(); */
					} else {

						$insert_data = array('client_attach_client_id' => $client_id, 'c_attach_type' => 'Kredit', 'client_attach_path' => $path.'/'.$name, 'client_attach_edit_by' => $this->session->userdata("username_session"));
						$this->Client_model->insert_loan_attachment($insert_data);

					}
				}
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

    public function show_job_lost(){
		$data['type']		= 'Arbeitsrecht / Kündigung';
		$data['clients'] 	= $this->Client_model->get_lawyer_clients_by_types(array(1));

        $this->template['content']  = $this->load->view('clients/general_clients_list', $data, TRUE);
        $this->load->view('templates/main', $this->template);
    }

	public function general_questions(){
		$data['type']		= 'Rechtliches allgemein';
		$data['clients'] 	= $this->Client_model->get_lawyer_clients_by_types(array(2));
		$data['status']  	= $this->Client_model->get_lawyer_status();

		$this->template['content']  = $this->load->view('clients/general_clients_list', $data, TRUE);
		$this->load->view('templates/main', $this->template);
	}

	public function update_case_status(){

		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$case_id 		= $this->input->post('case_id');
			$notes 			= $this->input->post('update_notes');
			$status			= $this->input->post('case_status');
			$update_date	= new DateTime();
			$loan_status 	= $this->Client_model->get_loan_status_by_id($status)->c_loan_status_name;

			$this->db->trans_begin();

			$update_data = array(
				'lawyer_help_status' => $status,
			);
			$this->Client_model->update_case_by_id($case_id, $update_data);

			$insert_data = array(
				'l_help_history_case_id' 	=> $case_id,
				'l_help_history_notes'		=> $notes,
				'l_help_history_status'		=> $status,
				'l_help_history_added_by'	=> $this->session->userdata('user_id_session')
			);
			$this->Client_model->insert_lawyer_data('lawyer_help_history', $insert_data);

			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_lawyer_documents($case_id, $client_id){
		$case_id = my_cryption($case_id, 'd','hco_url');
		$client_id = my_cryption($client_id, 'd','hco_url');

		if (isset($_FILES['lawyer_docs']) && $_FILES['lawyer_docs']['name'][0] != '') {
			$count_images = $_FILES['lawyer_docs']['name'];
			if (isset($count_images)) {

				$length = count($count_images);
				$path = 'dokuments/clients/' . $client_id . '/lawyer/_'.$case_id;

				if (!is_dir($path)) {
					mkdir($path, 0777, true);
				}

				for ($i = 0; $i < $length; $i++) {
					$name = str_replace(array(' ', 'ö', 'ü', 'ä', '.', '/', '-', '<', '>', '?', '!', pathinfo($_FILES['lawyer_docs']['name'][$i], PATHINFO_EXTENSION)), '', $_FILES['lawyer_docs']['name'][$i]).'.'.pathinfo($_FILES['lawyer_docs']['name'][$i], PATHINFO_EXTENSION);

					if (is_file($path . '/' . $name)) {
						unlink($path . '/' . $name);
						$this->Client_model->delete_double_attachs($path.'/'.$name);
					}
					$_FILES['file']['name'] = $name;
					$_FILES['file']['type'] = $_FILES['lawyer_docs']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['lawyer_docs']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['lawyer_docs']['error'][$i];
					$_FILES['file']['size'] = $_FILES['lawyer_docs']['size'][$i];
					$config['remove_spaces'] = true;
					$config['upload_path'] = $path;
					$config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
					$config['max_size'] = '7800000';
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('file')) {
						/*$upload_error = array('error' => $this->upload->display_errors());
						var_dump($upload_error);
						die(); */
					} else {

						$insert_data = array('client_attach_client_id' => $client_id, 'c_attach_type' => 'Anwalt', 'client_attach_path' => $path.'/'.$name);
						$this->Client_model->insert_loan_attachment($insert_data);
					}
				}
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_lawyer_attachment(){
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			$path = $this->input->post('path');
			$path = my_cryption($path, 'd', 'hco_url');

			if(is_file($path) == true){
				unlink($path);
			}

			$this->Client_model->delete_double_attachs($path);
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function ajax_request($type, $insurance = ''){
		if($type == 'get_loan_history'){
			$insurance = my_cryption($insurance, 'd', 'hco_url');
			$insurance_text = $this->Client_model->get_insurance_by_id_custom('insurance_data_notes', $insurance)->insurance_data_notes;

			$insurance_data = $this->Client_model->get_insurance_history($insurance);

			$result = '
						<div class="row">
						<div class="col-md-12">
							<div class="col-md-6"><h3>Notizen von Berater</h3></div>
							<div class="col-md-6"><h3>Statusupdates</h3></div>
						</div>
							<hr>
						<div class="col-md-12">
						<div class="col-md-6">
							<p>'.$insurance_text.'</p>
						</div>
						<div class="col-md-6">
        	';

			foreach($insurance_data as $insurance){
				$result .= $insurance->insurance_history_notes;
				$result .= '<hr>';
			}


			$result .= '</div></div></div>';

			echo $result;
		}

		if($type == 'show_loan_history'){
			$loan_history = $this->Client_model->get_loan_history($loan);

			$response = '';

			foreach($loan_history as $history){
				$response .= $history->loan_history_notes;
				$response .= '<hr>';
			}

			echo $response;
		}

		if($type == 'show_case_history'){
			$case_id = my_cryption($loan, 'd', 'hco_url');
			$case_history = $this->Client_model->get_case_history($case_id);

			$response = '<div class="table-responsive">
							<table class="table">';
			$response .= '
				<tr>
					<th>Datum</th>
					<th>Status</th>
					<th>Notizen</th>
					<th>Eingefügt von</th>
				</tr>
			';
			if($case_history){
				foreach($case_history as $history){
					$response .= '
						<tr>
							<td>'.date("d.m.Y, H:i", strtotime($history->l_help_history_timestamp)).'</td>
							<td>Statusänderung zu: <b>'.$history->l_status_name.'</b></td>
							<td>'.$history->l_help_history_notes.'</td>
							<td>'.$history->user_firstname.' '.$history->user_lastname.'</td>
						</tr>
					';
				}
			}
			$response .= '</table>
					</div>';

			echo $response;
		}

	}
}
