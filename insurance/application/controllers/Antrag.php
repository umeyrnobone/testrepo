<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrag extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Client_model');
        $this->load->helper('crm_helper');

    }


	public function index() {

		if($this->input->get('cid') && $this->input->get('cnm')){
			if(!$this->input->get('tmsp')){
				redirect('Antrag?cid='.$this->input->get("cid").'&cnm='.$this->input->get("cnm").'&tmsp='.my_cryption(strtotime("now"), "e", "url"));
			}

			$client_id = my_cryption($this->input->get('cid'), 'd', 'url');
			$firstname = my_cryption($this->input->get('cnm'), 'd', 'url');

			$data['client_data'] = $this->Client_model->check_client_exist($client_id, $firstname);

			if(!empty($data['client_data'])){
				$data['loans']		 = $this->Client_model->get_client_loans($client_id);
				$data['documents'] 	 = $this->Client_model->get_client_attachments($client_id);

				$this->load->view('antrag/formular', $data);

			} else{
				redirect('Error');
			}

		} else{
			redirect('Error');
		}

	}

	public function my_cryption ( $string, $action = 'e', $code = 'standart') {

		switch ( $code ) {
			case "standart":
				$secret_key = 'thisIsmySecretKey:)';
				$secret_iv = 'thisIsmySecretIv:)';
				break;
			case 'hc_login':
				$secret_key = 'cre@t€L0g!nK€y123)';
				$secret_iv = 'cre@t€L0g!n!V123)';
				break;

			default:
				$secret_key = 'thisIsmySecretKey:)';
				$secret_iv = 'thisIsmySecretIv:)';
		}

		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

		if( $action == 'e' ) {
			$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
		}

		else if( $action == 'd' ){
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}

		return $output;
	}

	public function add_loan_documents($loan_id, $client_id){
		$loan_id = my_cryption($loan_id, 'd', 'url');
		$client_id = my_cryption($client_id, 'd', 'url');

		$client_data = $this->Client_model->get_client_by_id($client_id);

		if(!empty($client_data) && $loan_id > 0){

			if (isset($_FILES['loan_documents']) && $_FILES['loan_documents']['name'][0] != '') {
				$count_images = $_FILES['loan_documents']['name'];

				if (isset($count_images)) {
					$length = count($count_images);
					$path = 'dokuments/clients/' . $client_id . '/loans/_'.$loan_id;
					if (!is_dir($path)) {
						mkdir($path, 0777, true);
					}

					for ($i = 0; $i < $length; $i++) {
						$name = str_replace(array(' ', 'ö', 'ü', 'ä', '.', '/', '-', '<', '>', '?', '!', pathinfo($_FILES['loan_documents']['name'][$i], PATHINFO_EXTENSION)), '', $_FILES['loan_documents']['name'][$i]).'.'.pathinfo($_FILES['loan_documents']['name'][$i], PATHINFO_EXTENSION);

						if (is_file($path . '/' . $name)) {
							unlink($path . '/' . $name);
							$this->Client_model->delete_double_attachs($path.'/'.$name);
						}
						$_FILES['file']['name'] = $name;
						$_FILES['file']['type'] = $_FILES['loan_documents']['type'][$i];
						$_FILES['file']['tmp_name'] = $_FILES['loan_documents']['tmp_name'][$i];
						$_FILES['file']['error'] = $_FILES['loan_documents']['error'][$i];
						$_FILES['file']['size'] = $_FILES['loan_documents']['size'][$i];
						$config['remove_spaces'] = true;
						$config['upload_path'] = $path;
						$config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
						$config['max_size'] = '7800000';
						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('file')) {
							/*$upload_error = array('error' => $this->upload->display_errors());
							var_dump($upload_error);
							die(); */
							redirect('Errors');
						} else {

							$insert_data = array('client_attach_client_id' => $client_id, 'c_attach_type' => 'Kredit', 'client_attach_path' => $path.'/'.$name);
							$this->Client_model->insert_loan_attachment($insert_data);
						}
					}
				}
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else{
			redirect('Errors');
		}
	}

	public function delete_attachment(){
		$attach_id = my_cryption($this->input->post('attachment'), 'd');

		if($attach_id > 0){
			$this->Client_model->delete_client_attachment($attach_id);
		}

		redirect($_SERVER['HTTP_REFERER']);
	}
}
