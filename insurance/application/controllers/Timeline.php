<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeline extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Client_model');
		$this->load->helper('crm_helper');

	}

	public function index(){
		$data['status_list'] = $this->Client_model->get_insurance_status_list();

		$c = -1;
		foreach($data['status_list'] as $list) {
			$data['status_list'][++$c] = $list;
			$data['status_list'][$c]->insurance_data = $this->Client_model->get_insurances_by_status_id($list->insurance_status_id);

		}

		$this->template['content'] = $this->load->view('timeline/index', $data, TRUE);
		$this->load->view('templates/main', $this->template);
	}
}
