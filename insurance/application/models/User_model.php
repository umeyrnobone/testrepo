<?php
class User_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	public function check_validation($username, $password){
		
		$data = array('user_name' => $username, 'user_password' => $password, 'user_activate' => '1');
		return $this->db->where($data)->limit(1)->get('users')->row_object();
	}	

	public function get_users(){
		return $this->db->where('user_delete', 0)->where('user_activate', 1)->get('users')->result_object();
	}

    public function get_user_salary_option($user_id) {
 		return $this->db
            ->join('user_salary_options','u_s_option_user_id = '. $user_id)
            ->where('user_id', $user_id)
            ->get('users')
            ->row_object();
    }

    public function get_salary_users() {
 		return $this->db
            ->join('user_salary_options','u_s_option_user_id = user_id')
            ->where('user_delete', 0)
            ->where('user_activate', 1)
            ->get('users')
            ->result_object();
    }

	public function get_hc_investor_users(){
		return $this->db
            ->join('hc_user_accounts','hc_user_account_user_id = user_id')
            ->where('user_delete', 0)
            ->where('user_activate', 1)
            ->get('users')
            ->result_object();
	}

	public function get_inaia_users(){

        $subQuery =  $this->db->select('GROUP_CONCAT(DISTINCT(g_c_payment_adviser_id)) as ids')->from('gold_contract_payments')->get()->row_object()->ids;
		return $this->db
            ->select('*')
            ->where('user_delete', 0)
            ->where('user_id IN ('. $subQuery .')')
            ->get('users')
            ->result_object();
	}

	public function get_deleted_users(){
		return $this->db->where('user_delete', 1)->get('users')->result_object();
	}

	public function get_user_by_id($id){
		return $this->db->from('users')->where('user_id = ',$id)->get()->row_object();
	}
	public function get_user_by_ids($ids){
		return $this->db->from('users')->where_in('user_id ',explode(', ', $ids) )->get()->result_object();
	}

	public function insert_user($data){
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

    public function delete_user( $user_id){

        $this->db->where('user_id', $user_id);
        $this->db->delete('users');
    }

	public function update_user($id, $data){

		$this->db->where('user_id', $id);
		$this->db->update('users', $data);	
	}
	
	public function update_login_date($username){

		$this->db->set('user_last_login', 'NOW()', FALSE);
		$this->db->where('user_name', $username);
		$this->db->update('users');	
	}

    public function get_user_sp_level_units() {
        return $this->db
            ->join('sp_options','user_id = s_option_foreign_id' )
            ->where('s_option_key','level_units')
            ->get('users')
            ->result_object();
    }

    public function get_user_sp_silent_units() {
        return $this->db
            ->join('sp_options','user_id = s_option_foreign_id' )
            ->where('s_option_key','silent_units')
            ->get('users')
            ->result_object();
	}
	
	public function get_hc_user_profits( $type, $month, $year) {
		return $this->db
			->select('sum(hc_profit_amount) as monthly_profit ,hc_profit_date,
						( 
							SELECT sum(p1.hc_profit_amount) 
							FROM `hc_profits` p1
								JOIN `users` ON `users`.`user_id` = p1.`hc_profit_user_id` 
									WHERE `hc_profit_type` = "hc"
									AND `hc_profit_confirmed` = "eingezahlt" 
									AND p.hc_profit_user_id = p1.hc_profit_user_id
						) as sum_profits,
                        
						( 
							SELECT count(p2.hc_profit_amount) 
							FROM `hc_profits` p2
								JOIN `users` ON `users`.`user_id` = p2.`hc_profit_user_id` 
									WHERE `hc_profit_type` = "hc"
									AND `hc_profit_confirmed` = "angefordert" 
									AND p.hc_profit_user_id = p2.hc_profit_user_id
						) as count_requested,
						users.*'
			)
			->from('hc_profits p')
			->join('users','users.user_id = p.hc_profit_user_id')
			->where('hc_profit_type', $type)
			->where('Month(hc_profit_date)', $month)
			->where('Year(hc_profit_date)', $year)
			->where('hc_profit_confirmed ', 'eingezahlt')
			->where('user_id', $this->session->userdata('user_id_session'))
			->group_by('hc_profit_user_id')
			->get()
			->result_object();
	}

	public function insert_menu_options($data){
		$this->db->insert('user_options', $data);
	}

	public function get_user_option_value_by_foreign_key($foreign_id, $key){
		return $this->db
			->select('*')
			->from('user_options')
			->where('u_option_key', $key)
			->where('u_option_foreign_id', $foreign_id)
			->get()
			->row_object();
	}

	public function get_all_crm_parts(){
		return $this->db
			->select('*')
			->from('crm_parts')
			->get()
			->result_object();
	}

	public function update_user_option_by_foreign_id($foreign_id, $key, $data){
		$this->db->where('u_option_foreign_id', $foreign_id);
		$this->db->where('u_option_key', $key);
		$this->db->update('user_options', $data);
	}

	public function get_duplicate_entries(){
		return $this->db
			->select('user_firstname, count(CONCAT(user_firstname, "-", user_lastname)) as number, user_id')
			->from('users')
			->group_by('user_firstname, user_lastname')
			->get()
			->result_object();
	}

	public function get_contract_noti_users(){
		return $this->db
			->select('user_id')
			->from('users')
			->where('UNIX_TIMESTAMP(user_last_login) > '.strtotime('-5 days'))
			->get()
			->result_object();
	}

	public function get_user_by_email($email){
		return $this->db
			->select('*')
			->from('users')
			->where('user_email', $email)
			->where('user_delete', 0)
			->where('user_activate', 1)
			->get()
			->row_object();
	}

	public function get_user_permissions_by_uri($controller, $function){
		return $this->db
			->select('*')
			->from('user_page_permissions')
			->where('u_page_permission_controller', $controller)
			->where('u_page_permission_page', $function)
			->get()
			->row_object();
	}

	public function get_all_user_page_permissions(){
		return $this->db
			->select('*')
			->from('user_page_permissions')
			->get()
			->result_object();
	}

	public function update_user_page_permission_by_id($permission_id, $data){
		$this->db->where('u_page_permission_id', $permission_id);
		$this->db->update('user_page_permissions', $data);
	}

	public function get_all_product_noti_settings(){
		return $this->db
			->select('*')
			->from('user_product_noti_settings')
			->get()
			->result_object();
	}

	public function update_user_product_notif_settings_by_id($id, $data){
		$this->db->where('u_p_noti_id', $id);
		$this->db->update('user_product_noti_settings', $data);
	}

	public function get_product_noti_settings_by_key($key){
		return $this->db
			->select('*')
			->from('user_product_noti_settings')
			->where('u_p_noti_key', $key)
			->get()
			->row_object();
	}

	public function get_user_buttons($user){
		return $this->db
			->select('*')
			->from('user_options')
			->where('u_option_foreign_id', $user)
			->where("u_option_key LIKE '%_button' ")
			->get()
			->result_object();
	}

	public function delete_user_option_buttons($user){
		$this->db->where('u_option_foreign_id', $user);
		$this->db->where("u_option_key LIKE '%_button' ");
		$this->db->delete('user_options');
	}

	public function add_user_option($data){
		$this->db->insert('user_options', $data);
	}
}
