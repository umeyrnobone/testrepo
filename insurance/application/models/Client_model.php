<?php
class Client_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	public function get_clients_with_loans(){
		return $this->db
			->select('*, count(client_loan_id) as loans')
			->from('clients')
			->join('client_loans', 'client_loans.client_loan_client_id = clients.client_id')
			->where('client_delete', 0)
			->group_by('client_id')
			->get()
			->result_object();
	}

	public function get_new_insurance_clients($date){
		return $this->db
			->select('insurance_data.*, insurance.*, insurance_status.*, clients.client_firstname, clients.client_lastname, clients.client_email,
					clients.client_mobile, clients.client_phone,clients.client_id, clients.client_insurance_id')
			->from('insurance_data')
			->join('clients', 'clients.client_id = insurance_data.insurance_data_client_id')
			->join('insurance_status', 'insurance_status_id = insurance_data_status_id')
			->join('insurance', 'insurance_id = insurance_data_insurance')
			->order_by('insurance_data_update', 'ASC')
			->where('client_delete', 0)
			->where('unix_timestamp(insurance_data_timestamp) >= '.strtotime($date->format('Y-m-d')))
			->order_by('insurance_data_update', 'ASC')
			->get()
			->result_object();
	}

	public function get_client_by_id($client_id){
		return $this->db
			->select('*')
			->from('clients')
			->where('client_id', $client_id)
			->get()
			->row_object();
	}

	public function get_client_insurances($client_id){
		return $this->db
			->select('*')
			->from('insurance_data')
			->join('insurance', 'insurance.insurance_id = insurance_data.insurance_data_insurance')
			->where('insurance_data_client_id', $client_id)
			->get()
			->result_object();
	}

	public function get_insurance_clients(){
		return $this->db
			->select('insurance_data.*, insurance.*, insurance_status.*, c1.client_firstname, c1.client_lastname, c1.client_email, 
			c1.client_job, c1.client_mobile, c1.client_phone,c1.client_id, 
			c1.client_birthdate, c1.client_insurance_id, (select concat(user_firstname, " ", user_lastname) from client_advice join users on user_id = c_advice_user_id where c_advice_consultation = 1 AND c_advice_client_id = c1.client_id limit 1) as adviser ')
			->from('insurance_data')
			->join('clients c1', 'c1.client_id = insurance_data.insurance_data_client_id')
			->join('insurance_status', 'insurance_status_id = insurance_data_status_id')
			->join('insurance', 'insurance_id = insurance_data_insurance')
			->order_by('insurance_data_update', 'ASC')
			->get()
			->result_object();
	}

	public function get_insurance_status_list(){
		return $this->db
			->select('*')
			->from('insurance_status')
			->get()
			->result_object();
	}

	public function update_insurance_data_by_id($id, $data){
		$this->db->where('insurance_data_id', $id);
		$this->db->update('insurance_data', $data);
	}

	public function update_case_by_id($id, $data){
		$this->db->where('lawyer_help_id', $id);
		$this->db->update('client_lawyer_help', $data);
	}

	public function insert_insurance_history($data){
		$this->db->insert('insurance_history', $data);
	}

	public function get_insurance_by_id_custom($query, $id){
		return $this->db
			->select($query)
			->from('insurance_data')
			->where('insurance_data_id', $id)
			->get()
			->row_object();
	}

	public function get_insurance_history($id){
		return $this->db
			->select('*')
			->from('insurance_data')
			->join('insurance_history', 'insurance_history_insurance = insurance_data_id')
			->join('insurance_status', 'insurance_status_id = insurance_history_status')
			->join('users', 'users.user_id = insurance_history_added_by')
			->where('insurance_history_insurance', $id)
			->order_by('insurance_history_timestamp', 'ASC')
			->get()
			->result_object();
	}

	public function get_insurance_status_by_id($id){
		return $this->db
			->select('*')
			->from('insurance_status')
			->where('insurance_status_id', $id)
			->get()
			->row_object();
	}

	public function insert_external_notes($data){
		$this->db->insert('client_notes_external', $data);
	}

	public function get_client_external_notes($client){
		return $this->db
			->select('*')
			->from('client_notes_external')
			->where('c_notes_external_client_id', $client)
			->join('users', 'users.user_id = client_notes_external.c_notes_external_added_by')
			->order_by('c_notes_external_timestamp', 'DESC')
			->get()
			->result_object();
	}

	public function insert_loan_attachment($data){
		$this->db->insert('client_attachments', $data);
	}

	public function get_client_attachments($client_id, $case='Kredit'){
		return $this->db
			->select('*')
			->from('client_attachments')
			->where('client_attach_client_id', $client_id)
			->where('c_attach_type', $case)
			->get()
			->result_object();
	}

	public function delete_double_attachs($path){
		$this->db->where('client_attach_path', $path);
		$this->db->delete('client_attachments');
	}

	public function check_client_exist($id, $firstname){
		return $this->db
			->select('*')
			->from('clients')
			->where('client_id', $id)
			->where('client_firstname', $firstname)
			->get()
			->row_object();
	}

	public function delete_client_attachment($attach_id){
		$this->db->where('client_attach_id', $attach_id);
		$this->db->delete('client_attachments');
	}

    public function get_clients_by_job_type($type){
        return $this->db
            ->select('*')
            ->from('clients')
            ->where('client_job_type', $type)
            ->get()
            ->result_object();
    }

	public function get_general_lawyer_clients(){
		return $this->db
			->select('*')
			->from('client_lawyer_help')
			->join('clients', 'clients.client_id = lawyer_help_client_id')
			->get()
			->result_object();
	}

	public function get_lawyer_clients_by_types($types){
		return $this->db
			->select('*')
			->from('client_lawyer_help')
			->join('clients', 'clients.client_id = lawyer_help_client_id')
			->join('lawyer_help_types', 'l_help_type_id = lawyer_help_type')
			->join('lawyer_status', 'l_status_id = lawyer_help_status')
			->where_in('lawyer_help_type', $types)
			->get()
			->result_object();
	}

	public function get_case_by_id($case){
		return $this->db
			->select('*')
			->from('client_lawyer_help')
			->join('clients', 'clients.client_id = lawyer_help_client_id')
			->join('lawyer_help_types', 'lawyer_help_types.l_help_type_id = lawyer_help_type')
			->join('users', 'users.user_id = lawyer_help_added_by')
			->where('lawyer_help_id', $case)
			->get()
			->row_object();
	}

	public function get_client_external_notes_by_type($client_id, $type){
		return $this->db
			->select('*')
			->from('client_notes_external')
			->join('users', 'users.user_id = c_notes_external_added_by')
			->where('c_notes_external_client_id', $client_id)
			->where('c_notes_external_reason', $type)
			->get()
			->result_object();
	}

	public function get_lawyer_status(){
		return $this->db
			->select('*')
			->from('lawyer_status')
			->get()
			->result_object();
	}

	public function insert_lawyer_data($table, $data){
		$this->db->insert($table, $data);
	}

	public function get_case_history($case){
		return $this->db
			->select('*')
			->from('lawyer_help_history')
			->join('users', 'user_id = l_help_history_added_by')
			->join('lawyer_status', 'l_status_id = l_help_history_status')
			->where('l_help_history_case_id', $case)
			->order_by('l_help_history_timestamp', 'DESC')
			->get()
			->result_object();
	}

	public function get_client_advice_insurances($client){
		return $this->db
			->select('*')
			->from('advice_meta')
			->where('advice_meta_belongsTo', $client)
			->where('advice_meta_type', 'insurance')
			->get()
			->result_object();
	}

	public function get_insurances_by_status_id($status){
		return $this->db
			->select('insurance.*, insurance_data.*, clients.client_firstname, clients.client_lastname')
			->from('insurance_data')
			->join('clients', 'client_id = insurance_data_client_id')
			->join('insurance', 'insurance_id = insurance_data_insurance')
			->where('insurance_data_status_id', $status)
			->get()
			->result_object();
	}
}
