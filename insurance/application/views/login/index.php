<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WiseBoard : Login</title>
    <!-- REQUIRED STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <!-- REQUIRED PLUGINS -->
    <link href="<?=base_url('assets/dist/plugins/fontawesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/icheck/css/skins/all.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/rippler/css/rippler.min.css')?>" rel="stylesheet" type="text/css">
    <!-- main.min.css - WISEBOARD CORE CSS -->
    <link href="<?=base_url('assets/dist/css/main.min.css')?>" rel="stylesheet" type="text/css">
    <!-- Login css -->
    <link href="<?=base_url('assets/css/page-login3.min.css') ?>" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
    <div class="modal fade" id="modal-forgotPass" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h2>Forgot Password?</h2>
            <form action="<?= base_url('Emails/forgot_password') ?>" method="POST">
              <div class="form-group">
                <p>Email eingeben, mit der Du im CRM registriert bist.</p>
                <div class="input-icon-left m-top-15">
                  <i class="fa fa-envelope input-icon"></i>
                  <input type="text" class="form-control" placeholder="Email eingeben" name="email">
                </div>
              </div><!-- /.form-group -->

              <div class="btn-wrapper">
                <span>
                  <button type="button" class="btn btn-default-outline btn-block rounded" data-dismiss="modal">Abbrechen</button>
                </span>
                <span>
                  <button type="submit" class="btn btn-primary btn-block rounded">Absenden</button>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.modal -->

    <div class="container">
      <div class="login-wrapper">
        <div class="text-center">
          <img src="<?=base_url('assets/img/login-logo.png')?>" alt="logo">
        </div><!-- /.logo-wrapper -->

        <div class="panel">
          <div class="panel-body">
            <h3>Login to Inspire </h3>
            <?php if($this->session->flashdata("errors")): ?>
              <div style="color:black;" class="alert alert-warning">
            <?= $this->session->flashdata('errors'); ?>
            </div>
            <?php endif; ?>
            <?= validation_errors()?>
            <form class="login-form" method="POST" action="<?= base_url('Login/verifyUser'); ?>">
              <div class="form-group">
                <div class="input-icon-left">
                  <i class="fa fa-user input-icon"></i>
                  <input type="text" name="username" class="form-control rounded" placeholder="Enter Username">
                </div>
              </div><!-- /.form-group -->
              <div class="form-group">
                <div class="input-icon-left">
                  <i class="fa fa-lock input-icon"></i>
                  <input type="password" name="password" class="form-control rounded" placeholder="Enter Password">
                </div>
              </div><!-- /.form-group -->


              <button type="submit" class="btn btn-default btn-block rounded">Einloggen</button>
              <a href="#" class="m-top-5" data-toggle="modal" data-target="#modal-forgotPass">Passwort vergessen?</a>
            </form>
          </div><!-- /.panel-body -->

          <div class="panel-footer">
            <a href="page-register3.html">Account erstellen</a>
          </div><!-- /.panel-footer -->
        </div><!-- /.panel -->
      </div>
    </div><!-- /.container -->

    <!-- REQUIRED SCRIPTS -->
    <script src="<?=base_url('assets/dist/js/jquery.min.js')?>"></script>
    <script src="<?=base_url('assets/dist/js/bootstrap.min.js')?>"></script>
    <!-- REQUIRED PLUGINS -->
    <script src="<?=base_url('assets/dist/plugins/icheck/js/icheck.min.js')?>"></script>
    <script src="<?=base_url('assets/dist/plugins/rippler/js/jquery.rippler.min.js')?>"></script>
    <!-- main.min.js - WISEBOARD CORE SCRIPT -->
    <script src="<?=base_url('assets/dist/js/main.min.js')?>"></script>
    <script src="<?=base_url('assets/js/page-login.min.js')?>"></script>

	<?php if(isset($_GET['reset'])): ?>
		<script>
			$(document).ready(function(){
				$('#reset_password').modal();
			});
		</script>
  		<div class="modal fade" id="reset_password" role="dialog" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4>Passwort zurücksetzen</h4>
					</div>

					<div class="modal-body">
						<?php if($_GET['reset'] == 1): ?>
							Sollte diese Email hinterlegt sein, erhältst du in Kürze in sha Allah eine Email mit einem Link zur Zurücksetzung des Passworts.
						<?php elseif($_GET['reset'] == 2): ?>
							Dein Passwort wurde zurückgesetzt. Du erhältst Dein neues Passwort in sha Allah per Email.
						<?php else: ?>
							Es ist ein Fehler aufgetreten oder Dein Link ist abgelaufen! Bitte versuche es erneut oder melde dich beim Administrator.
						<?php endif; ?>
					</div>

					<div class="modal-footer">
						<a class="btn btn-primary pull-left" href="<?= base_url('Login') ?>">Okay</a>
					</div>
				</div>
			</div>
		</div>

	<?php endif; ?>
  </body>
</html>