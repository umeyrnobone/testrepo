<?php
/** dd
 * Created by PhpStorm.
 * User: benutzer
 * Date: 14.05.19
 * Time: 02:15
 */
setlocale(LC_MONETARY, 'nl_NL');
function check_get_id($id){
	if(isset($_GET['id'])){
		if($_GET['id'] == $id){
			return 'selected_element';
		} else{
			return '';
		}
	} else{
		return '';
	}
}
?>
<div class="panel-body hc-shop">
	<div class="tab-content">

	</div><!-- /.tab-content -->
</div>

<style>
	.hco-article-column{
		overflow: unset !important;
	}

	.hco-articles{
		overflow: unset !important;
	}
</style>

<link href="<?=base_url('assets/css/timeline.style.css')?>" rel="stylesheet">
<div class="row timeline" style="overflow: unset !important;">

	<?php foreach($status_list as $status): ?>
		<div style="width: 350px; float: left;">
			<div class="float hco-article-column">
				<div class="col-md-12 float-left hco-article-column-header">
					<h2 style="-webkit-text-stroke: 1.5px black;"><?= $status->insurance_status_name ?></h2>
				</div>
				<div class="col-md-12 hco-articles" data-status="Neu">
					<?php foreach ($status->insurance_data as $insurance): ?>
						<a class="hco-article-anchor tl_car car-data-anchor" data-status="Neu" data-client_id="<?= $insurance->insurance_data_client_id ?>" data-id="<?= $insurance->insurance_data_id ?>" data-client="<?= $insurance->client_firstname.' '.$insurance->client_lastname ?>" data-toggle="modal" data-target="#meta_data_modal">
							<div class="panel hco-article">
								<div class="panel-body shadowed <?= check_get_id($insurance->insurance_data_id) ?>">
									<h5 class="text-center" style="color: blue"><strong>#<?=$insurance->insurance_data_id?>. </strong><?=$insurance->client_firstname.' '.$insurance->client_lastname?></h5>
									<hr style="margin-top: 5px; margin-bottom: 5px;">

									<div class="col-md-12">
										<small>
											<i class="far fa-calendar-alt pull-left" style="margin-right: 5px"></i><span class="pull-left" style="margin-right: 30px;"><?=date('d.m.Y', strtotime($insurance->insurance_data_timestamp))?></span>
										</small>
										<small style="pull-right">
											<span class="text-right"><i class="fas fa-scroll"></i> <?= $insurance->insurance_text ?></span>
										</small>
									</div>
								</div>
							</div>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>
