      <div class="row">
        <div class="col-lg-6">
          <div class="panel panel-default-light panel-tasks">
            <div class="panel-heading">
              <div class="panel-title">
                <i class="fa fa-list-ul m-right-5"></i> Tickets
              </div><!-- /.panel-title -->
              <div class="panel-tools panel-action">
                <button class="btn btn-close"></button>
                <button class="btn btn-min"></button>
                <button class="btn btn-expand"></button>
              </div><!-- /.panel-tools -->
            </div><!-- /.panel-heading -->

            <div class="panel-body">
              <form autocomplete="off">
                <table class="table table-striped table-bordered" id="datatable-basic">
                  <thead>
                    <tr>
                      <th>Von</th>
                      <th>Titel</th>
                      <th>Datum</th>
                      <th>Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($tickets as $ticket): ?>
                      <tr>
                        <td><img style="height: 40px; width: 40px;" src="<?= base_url('images/users/').$ticket->user_avatar ?>" alt=""></td>
                        <td><?=$ticket->ticket_title ?></td>
                        <td><?=date('d.m.y', strtotime($ticket->ticket_timestamp)).' - ' .date('H:i', strtotime($ticket->ticket_timestamp)).' Uhr'?></td>
                        <td><a href="<?=base_url('Tickets/show_ticket/').$ticket->ticket_id?>" class="btn btn-sm btn-success rippler">Details</a></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </form>
            </div><!-- /.panel-body -->

            <div class="panel-footer">
              <a href="<?=base_url('tickets/my_tickets')?>">Alle anzeigen <i class="fa fa-angle-right"></i></a>
            </div><!-- /.panel-footer -->
          </div><!--/.panel-->
        </div><!-- /.col-lg-6 -->


        <div class="col-lg-6">
          <div class="panel panel-default-light panel-calendar">
            <div class="panel-heading">
              <div class="panel-title">
                <i class="fa fa-calendar-plus-o m-right-5"></i> Calendar
              </div><!-- /.panel-title -->
              <div class="panel-tools panel-action">
                <button class="btn btn-close"></button>
                <button class="btn btn-min"></button>
                <button class="btn btn-expand"></button>
              </div><!-- /.panel-tools -->
            </div><!-- /.panel-heading -->

            <div class="panel-body">
              <div id="calendar"></div>
            </div><!-- /.panel-body -->
          </div><!--/.panel-->
        </div>

      </div><!-- /.row -->

