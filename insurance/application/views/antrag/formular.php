<?php setlocale(LC_MONETARY, 'nl_NL'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/css/custom.style.css') ?>" type="text/css">

	<link rel="icon" href="<?=base_url('assets/img/title.png')?>">

	<!-- REQUIRED STYLES -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic" rel="stylesheet" type="text/css">

	<link href="<?=base_url('assets/dist/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
	<!-- REQUIRED PLUGINS -->
	<link href="<?=base_url('assets/dist/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
	<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

	<link href="<?=base_url('assets/dist/plugins/animate.css/css/animate.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/icheck/css/skins/all.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/select2/select2-bootstrap-theme/css/select2-bootstrap.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/summernote/css/summernote.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/flag-css/css/flag-css.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/fullcalendar/css/fullcalendar.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.min.css');?>" rel="stylesheet" type="text/css">

	<link href="<?=base_url('assets/dist/plugins/rippler/css/rippler.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/dist/plugins/datatables/css/dataTables.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php //base_url('assets/css/page-events.min.css');?>" rel="stylesheet" type="text/css">
	<!--      <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>-->

	<!-- main.min.css - WISEBOARD CORE CSS -->
	<link href="<?=base_url('assets/dist/css/main.min.css');?>" rel="stylesheet" type="text/css">
	<!-- plugins.min.css - ALL PLUGINS CUSTOMIZATIONS -->
	<link href="<?=base_url('assets/dist/css/plugins.min.css');?>" rel="stylesheet" type="text/css">
	<!-- admin.min.css - ADMIN LAYOUT -->
	<link href="<?=base_url('assets/css/admin.min.css');?>" rel="stylesheet" type="text/css">
	<!-- theme-default.min.css - DEFAULT THEME -->
	<link href="<?=base_url('assets/css/theme-default.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/page-inbox.min.css');?>" rel="stylesheet" type="text/css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link href="<?=base_url('assets/css/mecca.style.css');?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/custom.style.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background: #d9d9d9">

	<div class="container" style="background: #ffffff">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-center">
					<?= $client_data->client_firstname.' '.$client_data->client_lastname ?> <br><br>
					<span style="background: #d7d7d7;">
						<span>Zeit, bevor Zugriff abläuft:</span>
						<span id="hour">00</span>
						<span class="divider">:</span>
						<span id="minute">00</span>
						<span class="divider">:</span>
						<span id="second">00</span>
					</span>
				</h3>
				<hr>
				<div class="col-md-12" style="padding-top: 15px;">
					<?php foreach($loans as $loan): ?>
						<div class="col-md-6">
							<div class="panel panel-default-light border-default">
								<div class="panel-heading">
									<div class="panel-title">
										<i class="fa fa-institution m-right-6"></i> <?= $loan->client_loan_name ?>
									</div>
									<!-- /.panel-title -->
									<div class="panel-tools panel-action">
										<button class="btn btn-min"></button>
										<button class="btn btn-expand"></button>
									</div>
									<!-- /.panel-tools -->
								</div>
								<!-- /.panel-heading -->

								<div class="panel-body table-responsive">
									<table class="table table-hover">
										<tr>
											<td><i class="fa fa-euro"></i> Gesamtsumme</td>
											<td class="text-right"><?= money_format('%!n', $loan->client_loan_amount) ?>€</td>
										</tr>
										<tr>
											<td><i class="fa fa-euro"></i> Offene Summe</td>
											<td class="text-right"><?= money_format('%!n', $loan->client_loan_open_sum) ?>€</td>
										</tr>
										<tr>
											<td><i class="fa fa-calendar"></i> Beginn</td>
											<td class="text-right"><span class="hidden"><?= strtotime($loan->client_loan_begin) ?></span><?= date('d.m.Y', strtotime($loan->client_loan_begin)) ?></td>
										</tr>
										<tr>
											<td><i class="fa fa-calendar"></i> Ende</td>
											<td class="text-right"><span class="hidden"><?= strtotime($loan->client_loan_end) ?></span><?= date('d.m.Y', strtotime($loan->client_loan_end)) ?></td>
										</tr>
										<form id="loan_form_<?= $loan->client_loan_id ?>" action="<?= base_url('Antrag/add_loan_documents/'.my_cryption($loan->client_loan_id, 'e', 'url').'/'.my_cryption($loan->client_loan_client_id, 'e', 'url')) ?>" method="POST" enctype="multipart/form-data">
											<tr>
												<td>
													<label for="loan_files_<?= $loan->client_loan_id ?>" class="btn btn-primary">Dokumente auswählen<input type="file" class="hidden pull-right upload-photo" data-id="<?= $loan->client_loan_id ?>" id="loan_files_<?= $loan->client_loan_id ?>" style="opacity: 0" accept=".doc,.docx,.pdf,application/pdf,.png,.jpeg,.jpg" name="loan_documents[]" multiple></label>
												</td>
												<td style="text-align: right">
													<button class=" btn btn-primary"><i class="fa fa-upload"></i></button>
												</td>
											</tr>

										</form>
										<tr>
											<td><h4>Dokumente</h4></td>
										</tr>
										<?php foreach($documents as $doc): ?>
											<?php if(strpos($doc->client_attach_path, '/_'.$loan->client_loan_id.'/') == true): ?>
												<tr>
													<td>
														<a style="font-size: 17px;" href="<?= base_url($doc->client_attach_path) ?>" target="_blank">
															<?php if(strpos($doc->client_attach_path, '.pdf') == true): ?>
																<i class="fa fa-file-pdf"></i>
															<?php elseif(strpos($doc->client_attach_path, '.png') == true || strpos($doc->client_attach_path, '.jpg') == true || strpos($doc->client_attach_path, '.jpeg') == true): ?>
																<i class="fa fa-file-image"></i>
															<?php elseif(strpos($doc->client_attach_path, '.doc') == true || strpos($doc->client_attach_path,'.docx') == true): ?>
																<i class="fa fa-file-word"></i>
															<?php endif; ?>
															<?= str_replace('dokuments/clients/'.$doc->client_attach_client_id.'/loans/_'.$loan->client_loan_id.'/', '', $doc->client_attach_path) ?>
														</a>
													</td>
													<td>
														<a href="" class="pull-right del_attach_btn" data-toggle="modal" data-target="#delete_attachment" data-id="<?= my_cryption($doc->client_attach_id, 'e') ?>">
															<i class="fas fa-trash"></i>
														</a>
													</td>
												</tr>

											<?php endif; ?>
										<?php endforeach; ?>
									</table>
								</div>

							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="timeout_reminder" role="dialog" data-backdrop="static" style="background: #d3d3d3">
		<div class="modal-dialog" style="margin-top: 12%">
			<div class="modal-content">
				<div class="modal-header">
					Zeit abgelaufen!
				</div>

				<div class="modal-body">
					Deine 10 Minuten sind leider abgelaufen! Bitte benutze Deinen Link erneut, um weitere Dateien hochzuladen!
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<button type="button" class="btn btn-primary" id="finish_button">Okay</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="delete_attachment" role="dialog" data-backdrop="static">
		<div class="modal-dialog" style="margin-top: 12%">
			<div class="modal-content">
				<form action="<?= base_url('Antrag/delete_attachment') ?>" method="POST">
				<div class="modal-header">
					Dokument löschen?
				</div>

				<div class="modal-body">
					Soll das Dokument wirklich gelöscht werden?
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<div class="pull-left">
							<input type="hidden" id="del_attach_id" name="attachment">
							<input type="submit" class="btn btn-primary" value="Ja">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Abbrechen</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		var base_url = "<?php echo base_url();?>";
	</script>
	<!-- REQUIRED SCRIPTS -->
	<script src="<?=base_url('assets/dist/js/jquery.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/js/bootstrap.min.js');?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

	<!-- REQUIRED PLUGINS -->
	<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.resize.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.pie.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.time.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/flot/js/plugins/jquery.flot.orderBars.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/flot/js/plugins/jquery.flot.tooltip.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/jquery.sparkline/js/jquery.sparkline.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/moment/js/moment.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/jquery.slimscroll/js/jquery.slimscroll.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/bootstrap-switch/js/bootstrap-switch.min.js')?>"></script>
	<script src="<?=base_url('assets/dist/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')?>"></script>
	<script src="<?=base_url('assets/dist/plugins/jquery.succinct/js/jquery.succinct.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/icheck/js/icheck.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/js/bootstrap-datepicker.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/summernote/js/summernote.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/fullcalendar/js/fullcalendar.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/fullcalendar/js/de.js');?>"></script>
	<script src="<?=base_url('assets/dist/js/d3.js') ?>"></script>
	<script src="<?=base_url('assets/dist/js/d3pie.js') ?>"></script>

	<script src="<?=base_url('assets/dist/plugins/select2/js/select2.full.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/bootstrap-notify/js/bootstrap-notify.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/rippler/js/jquery.rippler.min.js');?>"></script>
	<script src="<?=base_url('assets/dist/plugins/datatables/js/dataTables.min.js')?>"></script>
	<script src="<?=base_url('assets/dist/plugins/jquery.inputmask/js/jquery.inputmask.bundle.min.js')?>"></script>
	<script src="<?=base_url('assets/dist/plugins/bootstrap-wizard/js/jquery.bootstrap.wizard.min.js')?>"></script>
	<script src="<?=base_url('assets/dist/plugins/jquery.validate/js/jquery.validate.min.js')?>"></script>


	<!-- main.min.js - WISEBOARD CORE SCRIPT -->
	<script src="<?=base_url('assets/dist/js/main.min.js');?>"></script>
	<!-- admin.min.js - GENERAL CONFIGURATION SCRIPT FOR THE PAGES -->
	<script src="<?=base_url('assets/js/admin.min.js');?>"></script>
	<script src="<?=base_url('assets/js/demo/demo-form.js');?>"></script>
	<script src="<?=base_url('assets/js/demo/demo-plugin-datatables-extensions.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin.js')?>"></script>
	<script src="<?=base_url('assets/js/chartsScripts.js')?>"></script>

	<script src="<?=base_url('assets/js/customScript.js')?>"></script>
	<script src="<?=base_url('assets/js/meccaScript.js')?>"></script>
	<script src="<?php //base_url('assets/js/vueScript.js')?>"></script>
	<script src="https://kit.fontawesome.com/4b225514b6.js" crossorigin="anonymous"></script>

	<script>
		$(document).ready(function(){

			if (localStorage.expiry || localStorage.countdown) {
				if(localStorage.countdown < 1 || localStorage.expiry < Date.now()){
					$('#timeout_reminder').modal('show');
				}
			}

			localStorage.expiry = "<?= date('F d, Y H:i:s', my_cryption($_GET['tmsp'], 'd', 'url') + 601) ?>";
			localStorage.countdown = <?= my_cryption($_GET['tmsp'], 'd', 'url') + 600 - strtotime('now') ?>;

			if(localStorage.countdown < 1){
				$('#timeout_reminder').modal('show');
			}

			const countDownDate = new Date(localStorage.getItem('expiry')).getTime();

			const myfunc = setInterval(() => {
				if(localStorage.getItem('countdown') > 0){
					const currentTime = new Date().getTime();
					const timeLeft = countDownDate - currentTime;
					const hour = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					const minute = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
					const second = Math.floor((timeLeft % (1000 * 60)) / 1000);

					document.getElementById("hour").innerHTML = hour < 10 ? "0" + hour.toString() : hour;
					document.getElementById("minute").innerHTML = minute < 10 ? "0" + minute.toString() : minute;
					document.getElementById("second").innerHTML = second < 10 ? "0" + second.toString() : second;
				}

				if(Number(localStorage.countdown) > 0){
					localStorage.countdown = localStorage.countdown - 1;
				}

				if(localStorage.getItem('countdown') < 1 || localStorage.getItem('expiry') < Date.now()){
					$('#timeout_reminder').modal('show');

				}

				console.log(localStorage.getItem('countdown') );
			}, 1000);

			$('#finish_button').click(function(){
				localStorage.removeItem('countdown');
				localStorage.removeItem('expiry');

				window.location.replace("https://halalcheck-anwalt.de/Errors");
			});
		})
	</script>
</body>
</html>
