<?php

function checkStatus($status, $current_status){
	if($status == $current_status){
		return 'selected';
	} else{
		return '';
	}
}

function statusColor($status){
	$status_array = array(
		1		=> '#D3D3D3', //grau
		2		=> '#D3D3D3', //grau
		3		=> '#fffaa1', // gelb
		4		=> '#B0FFAD', //grün
		5		=> '#B0FFAD', //grün
		6		=> '#ed6f6f', //rot
		7		=> '#82bfec', //blau
		8		=> '#B0FFAD', //grün
		9		=> '#ed6f6f', //rot
		10		=> '#82bfec', //blau
		11		=> '#B0FFAD', //grün
		12		=> '#B0FFAD', //grün
		13		=> '#CDB7F6', //violett
	);

	return $status_array[$status];
}

?>


<div class="row">
    <div class="col-sm-12">
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="<?= base_url('Dashboard') ?>"><i class="fa fa-home"></i>Dashboard</a></li>
                <li><a href="">Kunden liste</a>
            </ol>

        </div><!-- /.breadcrumb-wrapper -->
    </div>
</div>

<h3 class="text-center">Alle Kunden</h3>
<hr>
<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default-light border-default">
                <div class="panel-heading">
                    <div class="panel-title pull-left">
                        <i class="fa fa-table m-right-5"></i> Kunden mit Krediten
                    </div><!-- /.panel-title -->
                    <div class="panel-tools">
                        <div class="tools-content">
                            <!-- Though the light panel structure does not have div.tools-content (only default panel has), we put it here for the Datatables input that is appended in here via jQuery universal script dynamically -->
                            <!-- NOTE: THERE GOES SEARCH INPUT WHICH WE ARE APPENDING HERE VIA JQUERY -->
                        </div>
                    </div><!-- /.panel-tools -->
                </div><!-- /.panel-heading -->
				<div class="panel-body">
					<div class="table-responsive">

						<div class="table-responsive">

							<table class="table table-striped table-bordered datatable-buttons">
								<thead>
								<tr>
									<th>Kunde</th>
									<th>Alter</th>
									<th>Beruf</th>
									<th>Eingefügt seit</th>
									<th>Berater</th>
									<th>Bestandsversicherungen</th>
									<th>Status</th>
									<th>Rufnummer</th>
									<th>E-Mail</th>
									<th>Anmerkung</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($clients as $client): ?>
									<?php $insurances = explode(', ', $client->client_insurance_id); ?>
									<?php $age = DateTime::createFromFormat('d.m.Y', date('d.m.Y', strtotime($client->client_birthdate)))->diff(new DateTime('now'))->y; ?>
									<tr>
										<td><a href="<?= base_url('Dashboard/show_client_details/').my_cryption($client->client_id,'e','hco_url') ?>" target="_blank"><?= $client->client_firstname.' '.$client->client_lastname ?></a></td>
<!--										<td>--><?//= $client->insurance_text ?><!--</td>-->
										<td><?= $age ?></td>
										<td><?= $client->client_job ?></td>
										<td><?= date('d.m.Y', strtotime($client->insurance_data_timestamp)) ?></td>
										<td><?= $client->adviser ?></td>
										<td>
											<?php foreach($client->existing_insurances as $ex_ins): ?>
												<?= $ex_ins->advice_meta_key.',' ?>
											<?php endforeach ?>
										</td>
										<td style="width: 150px;">
														<span data-toggle="modal" data-id="<?= my_cryption($client->insurance_data_id, 'e', 'hco_urls') ?>" data-status="<?= $client->insurance_status_name ?>" data-target="#change_status_modal" class="label block car_status_changer" style="cursor: pointer; background: <?= statusColor($client->insurance_data_status_id) ?>">
															<?= $client->insurance_status_name ?>
														</span>
										</td>
										<td><?= $client->client_mobile != '' ? $client->client_mobile : $client->client_phone ?></td>
										<td><?= $client->client_email ?></td>
										<td><?= $client->insurance_data_notes?></td>
										<td>
											<a href="#" class="btn btn-primary show_loan_history" data-id="<?= my_cryption($client->insurance_data_id,'e','hco_url') ?>" data-toggle="modal" data-target="#loan_notes"><i class="far fa-sticky-note"></i></a>
											<a href="#" class="btn btn-primary add_loan_notes" data-id="<?= my_cryption($client->insurance_data_client_id,'e','hco_url') ?>" data-toggle="modal" data-target="#add_notes"><i class="fas fa-pen"></i></a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div> <!-- /.table-responisive -->
					</div> <!-- /.table-responisive -->
				</div><!-- /.panel-body -->
            </div><!--/.panel-->
        </div><!-- /.col-sm-12 -->
    </div>

</div>

<div class="modal fade" id="change_status_modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/update_insurance_status') ?>" method="POST">
				<div class="modal-header">
					<h4>Status ändern</h4>
				</div>

				<div class="modal-body">
					<table class="table">
						<tr>
							<td>Aktueller Status</td>
							<td><span id="current_status_span"></span></td>
						</tr>

						<tr>
							<td>Neuer Status</td>
							<td>
								<select name="loan_status" required>
									<option value="">Status auswählen</option>
									<?php foreach($status as $status): ?>
										<option value="<?= $status->insurance_status_id ?>"><?= $status->insurance_status_name ?></option>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>

						<tr>
							<td>Notizen</td>
							<td>
								<textarea name="update_notes" cols="30" rows="5" class="form-control" required></textarea>
							</td>
						</tr>
					</table>
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="meta_id" name="insurance_id">

						<input type="submit" class="btn btn-primary" value="Status ändern">
						<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="loan_notes" role="dialog">
	<div class="modal-dialog" style="width: 950px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Details</h5>
			</div>

			<div class="modal-body">
				<div id="loan_details">

				</div>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_notes" role="dialog">
	<div class="modal-dialog" style="width: 450px; height: 100px;">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/add_notes') ?>" method="POST">
				<div class="modal-header">
					<h5>Notizen einfügen</h5>
				</div>

				<div class="modal-body">
					<h4>Notizen zum Kunden einfügen</h4>
					<textarea name="notes"cols="30" rows="5" class="form-control client_notes"></textarea>
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="client_id" name="client_id">
						<input type="submit" value="Einfügen" class="btn btn-primary">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
					</div>
				</div>
		</div>
	</div>
</div>
