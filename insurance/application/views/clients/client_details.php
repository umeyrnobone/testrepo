<?php setlocale(LC_MONETARY, 'nl_NL'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="<?= base_url('Dashboard/show_clients') ?>"><i class="fa fa-home"></i>Mandanten Liste</a></li>
                <li><a href="">Mandanten liste</a>
            </ol>

        </div><!-- /.breadcrumb-wrapper -->
    </div>
</div>
<section>
	<div class="row">

		<h2 class="page-title text-center">
			<p>
				<?=$client_info->client_firstname.' '.$client_info->client_lastname; ?>
			</p>
		</h2>
		<hr>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-6"></i> Personendaten
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body">
					<table class="table table-responsive table-hover">
						<tbody>
						<tr>
							<td>Name</td>
							<td><?=$client_info->client_firstname.' '.$client_info->client_lastname; ?></td>
						</tr>

						<?php if($client_info->client_birthdate): ?>
							<tr>
								<td>Geburtsdatum</td>
								<td><?=  ($client_info->client_birthdate =='1970-01-01') ? '': date( 'd.m.Y' ,strtotime( $client_info->client_birthdate)); ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_nationality): ?>
							<tr>
								<td>Nationalität</td>
								<td><?=$client_info->client_nationality; ?> </td>
							</tr>
						<?php endif; ?>


						<?php if($client_info->client_job): ?>
							<tr>
								<td>Beruf</td>
								<td><?=$client_info->client_job; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_email): ?>
							<tr>
								<td>Email</td>
								<td><?=$client_info->client_email; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_phone): ?>
							<tr>
								<td>Festnetz</td>
								<td><?=$client_info->client_phone; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_mobile): ?>
							<tr>
								<td>Mobil</td>
								<td><?=$client_info->client_mobile; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_country): ?>
							<tr>
								<td>Land</td>
								<td><?=$client_info->client_country; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_state): ?>
							<tr>
								<td>Bundesland</td>
								<td><?=$client_info->client_state; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_zipcode): ?>
							<tr>
								<td>PLZ</td>
								<td><?=$client_info->client_zipcode; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_city): ?>
							<tr>
								<td>Stadt</td>
								<td><?=$client_info->client_city; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_street): ?>
							<tr>
								<td>Straße</td>
								<td><?=$client_info->client_street; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if(strtotime($client_info->client_last_contact)): ?>
							<tr>
								<td>Letzter kontakt:</td>
								<td><?=( date('d.m.Y', strtotime($client_info->client_last_contact)) == "0000-00-00" )? '---------': date('d.m.Y - H:i', strtotime($client_info->client_last_contact)).' Uhr'; ?> </td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>

			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Notizen der Berater
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->


				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-12">
							<div class="textareaElement" contenteditable>
								<?= $insurances[0]->insurance_data_notes ?>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4">
			<!--------------------------------------------- Loans ------------------------------------------------------>
			<?php foreach($insurances as $insurance): ?>

			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-6"></i><?=$insurance->insurance_text?>
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body">
					<table class="table table-responsive table-hover">
						<tbody>
							<?php if($insurance->insurance_data_notes): ?>
								<tr>
									<td>Notizen</td>
									<td><?=$insurance->insurance_data_notes; ?></td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
					<form action="<?= base_url('Dashboard/add_loan_documents/').$insurance->insurance_id.'/'.$client_info->client_id?>" method="POST" enctype="multipart/form-data">
						<input class="form-group" type="file" name="loan_documents[]" id=""  multiple="multiple">
						<input class="form-group" type="submit" value="Upload" name="submit">
					</form>
				</div>
			</div>
            <?php endforeach; ?>
			

			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Dokumente
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->


				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-12">
							<div class="textareaElement" contenteditable>
							<form action="<?= base_url('Dashboard/add_loan_documents/').$client_info->client_id?>" method="POST" enctype="multipart/form-data">
								<p>Bitte Datei auswählen</p>
								<input class="form-group" type="file" name="loan_documents[]" id=""  multiple="multiple">
								<input class="form-group" type="submit" value="Upload" name="submit">
							</form>
							</div>
						</div>
					</div>


				</div>
			</div>

        </div>

		<div class="col-xs-12 col-sm-6 col-md-4">
			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Neue Notizen hinzufügen
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<form action="<?= base_url('Dashboard/add_notes') ?>" method="POST">
					<div class="panel-body">
						<div class="form-group">
							<textarea name="notes"cols="30" rows="10" class="form-control client_notes" required></textarea>
						</div>
						<div class="form-group">
							<input type="hidden" id="client_id" name="client_id" value="<?= my_cryption($client_info->client_id, 'e', 'hco_url') ?>">
							<input type="submit" value="Notizen einfügen" class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>

			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Notizen zum Kunden
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->


				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-12">
							<div class="textareaElement" contenteditable>
								<?php foreach($ext_notes as $note): ?>
									<p class="hc_notes"><?= $note->user_firstname.' '.$note->user_lastname ?></p>
									<p class="hc_notes"><?= date('d.m.Y, H:i ', strtotime($note->c_notes_external_timestamp)) ?>Uhr</p>
									<p><?= $note->c_notes_external_text ?></p>
									<hr>
								<?php endforeach; ?>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="loan_history" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Historie</h5>
			</div>

			<div class="modal-body">
				<div id="loan_details">

				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Schließen</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="show_attachments" role="dialog">
	<div class="modal-dialog" style="width: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Dokumente</h4>
			</div>

			<div class="modal-body">
				<div id="doc_container"></div>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="delete_attachment" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/delete_lawyer_attachment') ?>" method="POST">
				<div class="modal-header">
					<h4>Dokument löschen</h4>
				</div>

				<div class="modal-body">
					Soll das Dokument wirklich gelöscht werden?
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="delete_path" name="path">
						<input type="submit" value="Ja" class="btn btn-primary">
						<button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<style>
    label {
        cursor: pointer;
        /* Style as you please, it will become the visible UI component. */
    }

    .upload-photo {
        opacity: 0;
        position: absolute;
        z-index: -1;
    }
</style>
