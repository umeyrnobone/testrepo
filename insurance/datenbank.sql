-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 01. Feb 2019 um 17:24
-- Server-Version: 10.1.34-MariaDB
-- PHP-Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `crm`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `advice_meta`
--

CREATE TABLE `advice_meta` (
  `advice_meta_id` int(11) NOT NULL,
  `advice_meta_belongsTo` int(11) NOT NULL,
  `advice_meta_key` varchar(255) NOT NULL,
  `advice_meta_value` decimal(8,2) DEFAULT NULL,
  `advice_meta_type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `affiliate_infos`
--

CREATE TABLE `affiliate_infos` (
  `a_i_id` int(11) NOT NULL,
  `a_i_website` varchar(255) NOT NULL,
  `a_i_foreign_id` int(11) NOT NULL,
  `a_i_user_number` int(11) NOT NULL,
  `a_i_client_id` int(11) NOT NULL,
  `a_i_amount` int(11) NOT NULL,
  `a_i_paid` tinyint(2) NOT NULL DEFAULT '0',
  `a_i_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `affiliate_users`
--

CREATE TABLE `affiliate_users` (
  `a_u_id` int(11) NOT NULL,
  `a_u_number` varchar(10) NOT NULL,
  `a_u_password` varchar(255) NOT NULL,
  `a_u_firstname` varchar(100) NOT NULL,
  `a_u_lastname` varchar(100) NOT NULL,
  `a_u_email` varchar(100) NOT NULL,
  `a_u_activated` tinyint(2) NOT NULL DEFAULT '0',
  `a_u_timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `avb_clients`
--

CREATE TABLE `avb_clients` (
  `avb_client_id` int(11) NOT NULL,
  `avb_client_firstname` varchar(50) NOT NULL,
  `avb_client_lastname` varchar(50) NOT NULL,
  `avb_client_phone` varchar(50) NOT NULL,
  `avb_client_email` varchar(50) NOT NULL,
  `avb_client_address` varchar(50) NOT NULL,
  `avb_client_city` varchar(50) NOT NULL,
  `avb_client_code` varchar(8) NOT NULL,
  `avb_client_user_id` int(11) NOT NULL,
  `avb_client_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `avb_documents`
--

CREATE TABLE `avb_documents` (
  `avb_docu_id` int(11) NOT NULL,
  `avb_docu_client_id` int(11) NOT NULL,
  `avb_docu_path` varchar(300) NOT NULL,
  `avb_docu_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `avb_partitions`
--

CREATE TABLE `avb_partitions` (
  `avb_part_id` int(11) NOT NULL,
  `avb_part_client_id` int(11) NOT NULL,
  `avb_part_percent` decimal(6,2) NOT NULL,
  `avb_part_type` varchar(20) DEFAULT NULL,
  `avb_part_status` tinyint(1) NOT NULL DEFAULT '1',
  `avb_part_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `avb_reservations`
--

CREATE TABLE `avb_reservations` (
  `avb_res_id` int(11) NOT NULL,
  `avb_res_client_id` int(11) NOT NULL,
  `avb_res_truck_name` smallint(2) NOT NULL,
  `avb_res_user_id` int(11) NOT NULL,
  `avb_res_tax_number` varchar(10) DEFAULT NULL,
  `avb_res_start_time` datetime NOT NULL,
  `avb_res_end_time` datetime NOT NULL,
  `avb_res_return_date` datetime NOT NULL,
  `avb_res_overtime` varchar(10) NOT NULL DEFAULT '0',
  `avb_res_tariff` varchar(20) NOT NULL,
  `avb_res_accessories` varchar(300) NOT NULL,
  `avb_res_belt_number` tinyint(2) NOT NULL,
  `avb_res_insurance` varchar(20) NOT NULL,
  `avb_res_insurance_price` decimal(6,2) DEFAULT NULL,
  `avb_res_notes` text,
  `avb_res_price` decimal(6,2) NOT NULL,
  `avb_res_regular_price` decimal(6,2) NOT NULL,
  `avb_res_kaution` decimal(6,2) DEFAULT NULL,
  `avb_res_paid` decimal(6,2) DEFAULT NULL,
  `avb_res_discount` decimal(6,2) NOT NULL,
  `avb_res_start_km` varchar(10) NOT NULL,
  `avb_res_end_km` varchar(10) NOT NULL,
  `avb_res_regular_km` varchar(10) NOT NULL,
  `avb_res_extra_km` varchar(5) NOT NULL DEFAULT '0',
  `avb_res_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `avb_res_canceled` tinyint(1) NOT NULL DEFAULT '0',
  `avb_res_closed` tinyint(1) NOT NULL DEFAULT '0',
  `avb_res_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Client - 1: Werkstatt',
  `avb_res_notice` text,
  `avb_res_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `avb_shares`
--

CREATE TABLE `avb_shares` (
  `avb_share_id` int(11) NOT NULL,
  `avb_share_reservation_id` int(11) NOT NULL,
  `avb_share_client_id` int(11) NOT NULL,
  `avb_share_part_id` int(11) NOT NULL,
  `avb_share_money` decimal(6,2) NOT NULL,
  `avb_share_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `avb_trucks`
--

CREATE TABLE `avb_trucks` (
  `avb_truck_id` tinyint(2) NOT NULL,
  `avb_truck_name` varchar(30) NOT NULL,
  `avb_truck_km` varchar(10) NOT NULL,
  `avb_truck_kennzeichen` varchar(10) NOT NULL,
  `avb_truck_status` varchar(10) NOT NULL,
  `avb_truck_mark` varchar(30) NOT NULL,
  `avb_truck_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bookings`
--

CREATE TABLE `bookings` (
  `booking_id` int(11) NOT NULL,
  `booking_client_id` int(11) NOT NULL,
  `booking_trip_id` int(11) NOT NULL,
  `booking_trip_category` varchar(10) NOT NULL,
  `booking_age_category` int(1) NOT NULL,
  `booking_room_occupancy` int(1) NOT NULL,
  `booking_rail_fly` varchar(30) NOT NULL,
  `booking_trip_price` decimal(8,2) NOT NULL,
  `booking_sum_price` double(6,2) NOT NULL,
  `booking_payment_reason` varchar(100) DEFAULT NULL,
  `booking_group` int(11) DEFAULT NULL,
  `booking_mahram_id` int(11) DEFAULT NULL,
  `booking_add_by` varchar(100) DEFAULT 'Online',
  `booking_cancel` datetime DEFAULT NULL,
  `booking_cancel_payment` decimal(10,2) DEFAULT NULL,
  `booking_discount` decimal(8,2) DEFAULT NULL,
  `booking_history` varchar(2000) DEFAULT NULL,
  `booking_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `booking_attachments`
--

CREATE TABLE `booking_attachments` (
  `b_attach_id` int(11) NOT NULL,
  `b_attach_booking_id` int(11) NOT NULL,
  `b_attach_folder` varchar(100) NOT NULL,
  `b_attach_path` varchar(256) NOT NULL,
  `b_attach_type` varchar(50) DEFAULT NULL,
  `b_attach_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `booking_documents`
--

CREATE TABLE `booking_documents` (
  `b_document_id` int(11) NOT NULL,
  `b_document_booking_id` int(11) NOT NULL,
  `b_document_name` varchar(50) NOT NULL,
  `b_document_required` tinyint(1) NOT NULL DEFAULT '1',
  `b_document_status_halal` varchar(50) DEFAULT NULL,
  `b_document_mail` tinyint(1) NOT NULL DEFAULT '0',
  `b_document_email_date` datetime DEFAULT NULL,
  `b_document_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `booking_payments`
--

CREATE TABLE `booking_payments` (
  `b_payment_id` int(11) NOT NULL,
  `b_payment_booking_id` int(11) NOT NULL,
  `b_payment_amount` decimal(6,2) NOT NULL,
  `b_payment_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: not confiemed; 1:confirmed; 99: Cancelled',
  `b_payment_paid_date` datetime DEFAULT NULL,
  `b_payment_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `building_form`
--

CREATE TABLE `building_form` (
  `building_id` int(11) NOT NULL,
  `building_project_id` int(11) NOT NULL,
  `building_type` varchar(255) NOT NULL,
  `building_free_date` date NOT NULL,
  `building_rooms` varchar(10) NOT NULL,
  `building_bedrooms` varchar(10) NOT NULL,
  `building_bathrooms` varchar(10) NOT NULL,
  `building_year` date NOT NULL,
  `building_floors` int(10) DEFAULT NULL,
  `building_area` varchar(10) DEFAULT NULL,
  `building_ground_area` varchar(10) DEFAULT NULL,
  `building_country` varchar(100) DEFAULT NULL,
  `building_state` varchar(100) DEFAULT NULL,
  `building_city` varchar(100) DEFAULT NULL,
  `building_code` varchar(10) DEFAULT NULL,
  `building_street` varchar(200) DEFAULT NULL,
  `building_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `car_form`
--

CREATE TABLE `car_form` (
  `car_id` int(11) NOT NULL,
  `car_project_id` int(11) NOT NULL,
  `car_make` int(10) NOT NULL,
  `car_model` int(10) NOT NULL,
  `car_available` tinyint(1) NOT NULL DEFAULT '1',
  `car_condition` varchar(50) NOT NULL,
  `car_price` varchar(20) DEFAULT NULL,
  `car_vehicle` varchar(100) NOT NULL,
  `car_registration` date NOT NULL,
  `car_kilometer` int(11) NOT NULL,
  `car_transmission` varchar(30) DEFAULT NULL,
  `car_cubic` varchar(20) DEFAULT NULL,
  `car_air_condition` varchar(75) DEFAULT NULL,
  `car_power` varchar(20) NOT NULL,
  `car_fuel` varchar(50) NOT NULL,
  `car_color` varchar(100) NOT NULL,
  `car_features` varchar(255) DEFAULT NULL,
  `car_sticker` varchar(30) DEFAULT NULL,
  `car_doors` varchar(10) DEFAULT NULL,
  `car_hu` date DEFAULT NULL,
  `car_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `car_bought_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `car_make`
--

CREATE TABLE `car_make` (
  `c_make_id` int(10) NOT NULL,
  `c_make_name` varchar(50) NOT NULL,
  `c_make_user` int(10) NOT NULL,
  `c_make_timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `car_model`
--

CREATE TABLE `car_model` (
  `c_model_id` int(10) NOT NULL,
  `c_model_name` varchar(50) NOT NULL,
  `c_model_make_id` int(10) NOT NULL,
  `c_model_user` int(10) NOT NULL,
  `c_model_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `car_pays`
--

CREATE TABLE `car_pays` (
  `car_pay_id` int(11) NOT NULL,
  `car_pay_sale_id` int(11) NOT NULL,
  `car_pay_date` date NOT NULL,
  `car_pay_rate` varchar(15) NOT NULL,
  `car_pay_paid` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `car_sales`
--

CREATE TABLE `car_sales` (
  `car_sale_id` int(11) NOT NULL,
  `car_sale_car_id` int(11) NOT NULL,
  `car_sale_client_id` int(11) NOT NULL,
  `car_sale_user_id` int(11) NOT NULL,
  `car_sale_contract_date` date NOT NULL,
  `car_sale_payment_start` date NOT NULL,
  `car_sale_payment_end` date NOT NULL,
  `car_sale_coast` varchar(20) NOT NULL,
  `car_sale_deposit` varchar(20) NOT NULL,
  `car_sale_rates` varchar(20) NOT NULL,
  `car_sale_last_rate` varchar(20) NOT NULL,
  `car_sale_rate_number` int(11) NOT NULL,
  `car_sale_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `car_search`
--

CREATE TABLE `car_search` (
  `car_search_id` int(11) NOT NULL,
  `car_search_name` varchar(250) NOT NULL,
  `car_search_type` tinyint(1) DEFAULT NULL,
  `car_search_belongs_to` int(20) NOT NULL,
  `car_search_mailing` varchar(5) NOT NULL DEFAULT '1',
  `car_search_make` int(11) DEFAULT NULL,
  `car_search_model` int(11) DEFAULT NULL,
  `car_search_condition` varchar(50) DEFAULT NULL,
  `car_search_vehicle` varchar(120) DEFAULT NULL,
  `car_search_transmission` varchar(100) DEFAULT NULL,
  `car_search_from_year` year(4) DEFAULT NULL,
  `car_search_until_year` year(4) DEFAULT NULL,
  `car_search_from_coast` varchar(15) DEFAULT NULL,
  `car_search_until_coast` varchar(15) DEFAULT NULL,
  `car_search_from_kilometer` varchar(10) DEFAULT NULL,
  `car_search_until_kilometer` varchar(10) DEFAULT NULL,
  `car_search_color` varchar(70) DEFAULT NULL,
  `car_search_fuel` varchar(50) DEFAULT NULL,
  `car_search_hu` date DEFAULT NULL,
  `car_search_sticker` varchar(50) DEFAULT NULL,
  `car_search_features` varchar(1000) DEFAULT NULL,
  `car_search_url` varchar(1000) NOT NULL,
  `car_search_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `clients`
--

CREATE TABLE `clients` (
  `client_id` int(15) NOT NULL,
  `client_gender` varchar(5) NOT NULL DEFAULT 'Herr',
  `client_firstname` varchar(100) NOT NULL,
  `client_lastname` varchar(100) NOT NULL,
  `client_email` varchar(50) DEFAULT NULL,
  `client_skype` varchar(100) DEFAULT NULL,
  `client_halalauto_password` varchar(1000) DEFAULT NULL,
  `client_logger` varchar(25) DEFAULT NULL,
  `client_birthdate` date DEFAULT NULL,
  `client_family_stand` varchar(20) DEFAULT NULL,
  `client_nationality` varchar(255) NOT NULL,
  `client_birth_city` varchar(100) DEFAULT NULL,
  `client_homeland` varchar(255) DEFAULT NULL,
  `client_tags` varchar(600) DEFAULT NULL,
  `client_notice` text,
  `client_capital` varchar(255) DEFAULT NULL,
  `client_invest_capital` varchar(50) DEFAULT NULL,
  `client_rest_income` decimal(6,2) DEFAULT NULL,
  `client_interest` varchar(255) DEFAULT NULL,
  `client_vl` varchar(200) DEFAULT NULL,
  `client_insurance_id` varchar(255) NOT NULL,
  `client_country` varchar(50) DEFAULT NULL,
  `client_state` varchar(50) DEFAULT NULL,
  `client_zipcode` varchar(15) DEFAULT NULL,
  `client_city` varchar(70) DEFAULT NULL,
  `client_street` varchar(50) DEFAULT NULL,
  `client_housenr` varchar(50) DEFAULT NULL,
  `client_phone` varchar(50) DEFAULT NULL,
  `client_mobile` varchar(50) DEFAULT NULL,
  `client_criteria` varchar(50) DEFAULT NULL,
  `client_custom_req` varchar(50) DEFAULT NULL,
  `client_custom_req2` varchar(50) DEFAULT NULL,
  `client_decision_maker` varchar(50) DEFAULT NULL,
  `client_alt_decision_maker` varchar(50) DEFAULT NULL,
  `client_mvv_notes` varchar(500) DEFAULT NULL,
  `client_req_question` varchar(10) DEFAULT NULL,
  `client_passport_number` varchar(30) DEFAULT NULL,
  `client_passport_expiry` date DEFAULT NULL,
  `client_residence_stat` varchar(30) DEFAULT NULL,
  `client_residence_expiry` date DEFAULT NULL,
  `client_recommended_by` varchar(100) DEFAULT NULL,
  `client_recommend_question` datetime DEFAULT NULL,
  `client_know_us_by` varchar(250) DEFAULT NULL,
  `client_job` varchar(100) DEFAULT NULL,
  `client_job_type` varchar(30) DEFAULT NULL,
  `client_bank_name` varchar(100) DEFAULT NULL,
  `client_iban` varchar(255) DEFAULT NULL,
  `client_bic` varchar(10) DEFAULT NULL,
  `client_kredit_owner` varchar(100) DEFAULT NULL,
  `client_register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_user_id` int(11) NOT NULL DEFAULT '1',
  `client_current_status_id` int(11) DEFAULT '10',
  `client_current_type_id` varchar(20) DEFAULT '1',
  `client_last_contact` datetime DEFAULT NULL,
  `client_adviser_noti` tinyint(1) NOT NULL DEFAULT '0',
  `client_generated_at` date DEFAULT NULL,
  `client_contact_counter` tinyint(2) NOT NULL DEFAULT '0',
  `client_inaia` tinyint(4) DEFAULT NULL,
  `client_hc_member` tinyint(2) DEFAULT NULL,
  `client_own_member` tinytext,
  `client_done` tinyint(2) DEFAULT NULL,
  `client_delete` tinyint(1) NOT NULL DEFAULT '0',
  `client_last_update` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_advice`
--

CREATE TABLE `client_advice` (
  `c_advice_id` int(11) NOT NULL,
  `c_advice_user_id` int(11) NOT NULL,
  `c_advice_client_id` int(11) NOT NULL,
  `c_advice_partner_job` varchar(100) DEFAULT NULL,
  `c_advice_kids_income` decimal(6,2) DEFAULT NULL,
  `c_advice_netto_income` decimal(6,2) DEFAULT NULL,
  `c_advice_other_income` decimal(6,2) DEFAULT NULL,
  `c_advice_total_income` decimal(6,2) DEFAULT NULL,
  `c_advice_total_outgoing` decimal(6,2) DEFAULT NULL,
  `c_advice_anlagen_coast` decimal(6,2) DEFAULT NULL,
  `c_advice_equity` decimal(6,2) DEFAULT NULL,
  `c_advice_property` decimal(6,2) DEFAULT NULL,
  `c_advice_owe` decimal(6,2) DEFAULT NULL,
  `c_advice_papers` decimal(6,2) DEFAULT NULL,
  `c_advice_h_paper` tinyint(1) NOT NULL DEFAULT '0',
  `c_advice_valuables` decimal(6,2) DEFAULT NULL,
  `c_advice_name_puffer` varchar(100) DEFAULT NULL,
  `c_advice_puffer_amount` decimal(6,2) DEFAULT NULL,
  `c_advice_puffer_home` decimal(6,2) DEFAULT NULL,
  `c_advice_goal_1` varchar(100) DEFAULT NULL,
  `c_advice_saving_amount_1` double(6,2) DEFAULT NULL,
  `c_advice_goal_2` varchar(100) DEFAULT NULL,
  `c_advice_saving_amount_2` decimal(6,2) DEFAULT NULL,
  `c_advice_goal_3` varchar(100) DEFAULT NULL,
  `c_advice_saving_amount_3` decimal(6,2) DEFAULT NULL,
  `c_advice_prio_1` varchar(100) DEFAULT NULL,
  `c_advice_prio_2` varchar(100) DEFAULT NULL,
  `c_advice_prio_3` varchar(100) DEFAULT NULL,
  `c_advice_prio_4` varchar(100) DEFAULT NULL,
  `c_advice_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_attachments`
--

CREATE TABLE `client_attachments` (
  `client_attach_id` int(11) NOT NULL,
  `client_attach_client_id` int(11) NOT NULL,
  `c_attach_type` varchar(15) NOT NULL,
  `client_attach_path` varchar(255) NOT NULL,
  `client_attach_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_children`
--

CREATE TABLE `client_children` (
  `c_child_id` int(11) NOT NULL,
  `c_child_client_id` int(11) NOT NULL,
  `c_child_name` varchar(200) NOT NULL,
  `c_child_birthdate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_details`
--

CREATE TABLE `client_details` (
  `c_detail_id` int(20) NOT NULL,
  `c_detail_client_id` int(11) NOT NULL,
  `c_detail_user_id` int(11) NOT NULL,
  `c_detail_adviser_id` int(11) DEFAULT NULL,
  `c_detail_status_id` int(11) NOT NULL,
  `c_detail_type_id` tinytext,
  `c_detail_uses` varchar(200) NOT NULL DEFAULT 'crm',
  `c_detail_changed_by` int(5) DEFAULT NULL,
  `c_detail_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `c_detail_notice` text,
  `c_detail_title` varchar(255) DEFAULT NULL,
  `c_detail_last_contact` datetime DEFAULT NULL,
  `c_detail_vip_status` varchar(100) DEFAULT NULL COMMENT '1:probleme, 2:Macht Stress , 3 :Beitrag nicht bezahlt',
  `c_detail_sent_from` int(11) NOT NULL DEFAULT '0',
  `c_detail_begin_date` datetime DEFAULT NULL,
  `c_detail_update` tinyint(4) NOT NULL DEFAULT '1',
  `c_details_meeting` tinyint(4) DEFAULT NULL,
  `c_detail_site` varchar(50) NOT NULL DEFAULT 'lokal',
  `c_detail_update_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_status`
--

CREATE TABLE `client_status` (
  `c_status_id` int(3) NOT NULL,
  `c_status_name` varchar(150) NOT NULL COMMENT '1-49: Client action , 50-99: Verwaltung , 100-150: VIPs'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `client_status`
--

INSERT INTO `client_status` (`c_status_id`, `c_status_name`) VALUES
(1, 'Neu'),
(5, 'Erster Termin'),
(10, 'Extra Termin '),
(11, 'Online Termin'),
(12, 'Rückruf'),
(13, 'Terminvereinabren'),
(15, 'kein Interesse'),
(20, ''),
(27, 'status ändern'),
(30, 'Interesse in der Zukunft'),
(31, 'Will nur Immo'),
(32, 'Meldet sich selbst'),
(33, 'Wartet auf Investment'),
(35, 'keine Kontaktdaten'),
(40, 'keine Reaktion'),
(50, 'Kunde nicht erreicht'),
(55, 'Termin nicht wahrgenommen'),
(56, 'Termin abgesagt'),
(60, 'Kunde wurde nicht angerufen'),
(65, 'Nicht erreicht, Email versandt'),
(100, 'Probleme'),
(105, 'Macht Stress'),
(110, 'Beitrag nicht bezahlt'),
(115, 'Gekündigt');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_types`
--

CREATE TABLE `client_types` (
  `c_type_id` int(3) NOT NULL,
  `c_type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `client_types`
--

INSERT INTO `client_types` (`c_type_id`, `c_type_name`) VALUES
(1, 'Lead'),
(10, 'Normal'),
(13, 'Silber'),
(15, 'INAIA'),
(16, 'Gold'),
(19, 'Platin'),
(20, 'Help2Hadj'),
(25, 'Halal Community'),
(26, 'Neutral Co'),
(30, 'Halalcheck\r\n'),
(35, 'Halal Auto'),
(40, 'Zakkatcheck');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `client_users`
--

CREATE TABLE `client_users` (
  `cu_id` int(11) NOT NULL,
  `cu_user_id` smallint(6) NOT NULL,
  `cu_client_id` int(11) NOT NULL,
  `cu_timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `comment_text` text NOT NULL,
  `comment_image` varchar(255) DEFAULT NULL,
  `comment_user_id` int(11) NOT NULL,
  `comment_foreign_id` int(11) NOT NULL,
  `comment_type` varchar(10) NOT NULL DEFAULT 'Ticket',
  `comment_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contract_gold`
--

CREATE TABLE `contract_gold` (
  `c_gold_id` int(11) NOT NULL,
  `c_gold_client_id` int(9) NOT NULL,
  `c_gold_amount` varchar(50) NOT NULL,
  `c_gold_runtime` int(11) NOT NULL,
  `c_gold_discount` varchar(15) NOT NULL,
  `c_gold_coast_type` varchar(20) NOT NULL,
  `c_gold_user_id` varchar(15) NOT NULL,
  `c_gold_child` varchar(15) DEFAULT NULL,
  `c_gold_contract_start` date NOT NULL,
  `c_gold_contract_end` date NOT NULL,
  `c_gold_payment_start` date NOT NULL,
  `c_gold_units` varchar(20) NOT NULL,
  `c_gold_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contract_gold_rates`
--

CREATE TABLE `contract_gold_rates` (
  `gold_rate_id` int(11) NOT NULL,
  `gold_rate_contract_id` int(10) NOT NULL,
  `gold_rate_user_id` int(11) NOT NULL,
  `gold_rate_begin` date NOT NULL,
  `gold_rate_end` date NOT NULL,
  `gold_rate_salary` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contract_inaia`
--

CREATE TABLE `contract_inaia` (
  `c_inaia_id` int(11) NOT NULL,
  `c_inaia_client_id` int(6) NOT NULL,
  `c_inaia_amount` varchar(20) NOT NULL,
  `c_inaia_commission` varchar(20) NOT NULL,
  `c_inaia_contract_start` date NOT NULL,
  `c_inaia_contract_end` date NOT NULL,
  `c_inaia_payment_start` date NOT NULL,
  `c_inaia_user_id` varchar(20) NOT NULL,
  `c_inaia_units` varchar(20) NOT NULL,
  `c_inaia_timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contract_problems`
--

CREATE TABLE `contract_problems` (
  `c_problem_id` int(11) NOT NULL,
  `c_problem_contract_id` int(11) NOT NULL,
  `c_problem_inaia` tinyint(1) NOT NULL DEFAULT '0',
  `c_problem_client` tinyint(1) NOT NULL DEFAULT '0',
  `c_problem_canceled` tinyint(1) NOT NULL DEFAULT '0',
  `c_problem_date` date NOT NULL,
  `c_problem_notice` varchar(1500) NOT NULL,
  `c_problem_user_id` int(11) NOT NULL,
  `c_problem_done` tinyint(1) NOT NULL DEFAULT '0',
  `c_problem_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contract_takaful`
--

CREATE TABLE `contract_takaful` (
  `c_takaful_id` int(11) NOT NULL,
  `c_takaful_client_id` int(11) NOT NULL,
  `c_takaful_amount` varchar(200) NOT NULL,
  `c_takaful_runtime` int(11) NOT NULL,
  `c_takaful_discount` varchar(15) DEFAULT NULL,
  `c_takaful_coast_type` varchar(20) NOT NULL,
  `c_takaful_user_id` varchar(20) NOT NULL,
  `c_takaful_person` varchar(100) NOT NULL,
  `c_takaful_contract_start` date NOT NULL,
  `c_takaful_contract_end` date NOT NULL,
  `c_takaful_end_price` varchar(20) NOT NULL,
  `c_takaful_payment_start` date NOT NULL,
  `c_takaful_units` varchar(20) NOT NULL,
  `c_takaful_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contract_takaful_rates`
--

CREATE TABLE `contract_takaful_rates` (
  `takaful_rate_id` int(11) NOT NULL,
  `takaful_rate_contract_id` int(11) NOT NULL,
  `takaful_rate_user_id` int(11) NOT NULL,
  `takaful_rate_begin` date NOT NULL,
  `takaful_rate_end` date NOT NULL,
  `takaful_rate_salary` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `current_price`
--

CREATE TABLE `current_price` (
  `c_price_id` int(11) NOT NULL,
  `c_price_name` varchar(20) NOT NULL,
  `c_price_gramm` decimal(4,2) NOT NULL,
  `c_price_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `emails`
--

CREATE TABLE `emails` (
  `email_id` int(11) NOT NULL,
  `email_from` int(11) NOT NULL,
  `email_to` int(11) NOT NULL,
  `email_website` varchar(50) NOT NULL,
  `email_department` varchar(250) NOT NULL,
  `email_subject` varchar(500) NOT NULL,
  `email_message` text NOT NULL,
  `email_seen` datetime DEFAULT NULL,
  `email_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `email_tracking`
--

CREATE TABLE `email_tracking` (
  `email_id` int(11) NOT NULL,
  `email_to` int(11) DEFAULT NULL,
  `email_from` varchar(100) DEFAULT NULL,
  `email_type` varchar(100) DEFAULT NULL,
  `email_seen_counter` smallint(6) NOT NULL DEFAULT '0',
  `email_deliver` tinyint(4) NOT NULL DEFAULT '0',
  `email_ip` varchar(10) DEFAULT NULL,
  `email_error` varchar(255) DEFAULT NULL,
  `email_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gold_contracts`
--

CREATE TABLE `gold_contracts` (
  `g_contract_id` int(11) NOT NULL,
  `g_contract_client_id` int(11) NOT NULL,
  `g_contract_added_by` int(6) NOT NULL,
  `g_contract_adviser_id` varchar(6) NOT NULL,
  `g_contract_amount` decimal(6,2) NOT NULL,
  `g_contract_years` int(6) NOT NULL,
  `g_contract_signature_date` date NOT NULL,
  `g_contract_start_date` date NOT NULL,
  `g_contract_payment_date` date NOT NULL,
  `g_contract_payment_status` varchar(30) NOT NULL,
  `g_contract_discount` decimal(6,2) DEFAULT NULL,
  `g_contract_cancelled` tinyint(1) DEFAULT '0',
  `g_contract_units` decimal(6,2) NOT NULL,
  `g_contract_notice` varchar(1500) DEFAULT NULL,
  `g_contract_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gold_contract_payments`
--

CREATE TABLE `gold_contract_payments` (
  `g_c_payment_id` int(11) NOT NULL,
  `g_c_payment_contract_id` int(11) NOT NULL,
  `g_c_payment_date` date NOT NULL,
  `g_c_payment_status` varchar(30) NOT NULL DEFAULT 'Gezahlt',
  `g_c_payment_adviser_id` int(6) NOT NULL,
  `g_c_payment_units` decimal(6,2) NOT NULL,
  `g_c_payment_percent` decimal(6,2) DEFAULT NULL,
  `g_c_payment_notice` varchar(1000) NOT NULL,
  `g_c_payment_expected_date` date NOT NULL,
  `g_c_payment_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halalcheck24`
--

CREATE TABLE `halalcheck24` (
  `h_check_id` int(11) NOT NULL,
  `h_check_firstname` varchar(255) NOT NULL,
  `h_check_lastname` varchar(255) NOT NULL,
  `h_check_email` varchar(255) NOT NULL,
  `h_check_job` varchar(255) NOT NULL,
  `h_check_girokonto` varchar(255) NOT NULL,
  `h_check_konten` varchar(255) NOT NULL,
  `h_check_kreditkart` tinyint(2) NOT NULL,
  `h_check_sparbuches` tinyint(2) NOT NULL,
  `h_check_gold` tinyint(2) NOT NULL,
  `h_check_aktien` tinyint(2) NOT NULL,
  `h_check_investor` tinyint(2) NOT NULL,
  `h_check_insurance` varchar(1000) NOT NULL,
  `h_check_financing` varchar(255) NOT NULL,
  `h_check_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halal_auto`
--

CREATE TABLE `halal_auto` (
  `h_car_id` int(10) NOT NULL,
  `h_car_belongsto` int(10) NOT NULL DEFAULT '0',
  `h_car_InsertedBy` int(10) NOT NULL,
  `h_car_make` int(10) NOT NULL,
  `h_car_model` int(10) NOT NULL,
  `h_car_title` varchar(250) DEFAULT NULL,
  `h_car_dealer` varchar(100) DEFAULT NULL,
  `h_car_description` text,
  `h_car_test_drive` text,
  `h_car_more_tips` text,
  `h_car_service_details` text,
  `h_car_available` tinyint(1) NOT NULL DEFAULT '1',
  `h_car_condition` varchar(30) DEFAULT NULL,
  `h_car_price` varchar(30) DEFAULT NULL,
  `h_car_vehicle` varchar(30) DEFAULT NULL,
  `h_car_registration` year(4) NOT NULL,
  `h_car_kilometer` int(6) DEFAULT NULL,
  `h_car_profile` varchar(250) DEFAULT NULL,
  `h_car_folder` varchar(250) NOT NULL,
  `h_car_transmission` varchar(30) DEFAULT NULL,
  `h_car_cubic` varchar(30) DEFAULT NULL,
  `h_car_air_condition` varchar(30) DEFAULT NULL,
  `h_car_power` varchar(30) DEFAULT NULL,
  `h_car_fuel` varchar(30) DEFAULT NULL,
  `h_car_color` varchar(30) DEFAULT NULL,
  `h_car_features` varchar(255) DEFAULT NULL,
  `h_car_sticker` varchar(30) DEFAULT NULL,
  `h_car_doors` varchar(30) DEFAULT NULL,
  `h_car_hu` date DEFAULT NULL,
  `h_car_accident` varchar(30) DEFAULT NULL,
  `h_car_bought_date` date DEFAULT NULL,
  `h_car_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halal_auto_attachments`
--

CREATE TABLE `halal_auto_attachments` (
  `hc_attach_id` int(10) NOT NULL,
  `hc_attach_car_id` int(10) NOT NULL,
  `hc_attach_folder` varchar(250) NOT NULL,
  `hc_attach_path` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halal_auto_message`
--

CREATE TABLE `halal_auto_message` (
  `h_a_id` int(10) NOT NULL,
  `h_a_name` varchar(250) NOT NULL,
  `h_a_email` varchar(100) NOT NULL,
  `h_a_email_from` varchar(150) DEFAULT NULL,
  `h_a_phone` varchar(20) NOT NULL,
  `h_a_subject` varchar(250) NOT NULL,
  `h_a_message` text NOT NULL,
  `h_a_type` tinyint(1) NOT NULL DEFAULT '0',
  `h_a_show` tinyint(1) NOT NULL DEFAULT '0',
  `h_a_show_user` int(10) DEFAULT NULL,
  `h_a_belongs_to` int(10) DEFAULT NULL,
  `h_a_email_deliver` tinyint(1) DEFAULT NULL,
  `h_a_email_error` varchar(250) DEFAULT NULL,
  `h_a_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halal_community`
--

CREATE TABLE `halal_community` (
  `h_community_id` int(11) NOT NULL,
  `h_community_client_id` int(11) NOT NULL,
  `h_community_type_id` tinyint(2) NOT NULL,
  `h_community_feese` decimal(8,2) NOT NULL,
  `h_community_user_id` int(11) NOT NULL,
  `h_community_notice` text,
  `h_community_history` text,
  `h_community_member_date` date NOT NULL,
  `h_community_available_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `h_community_status` varchar(50) NOT NULL DEFAULT 'Antrag gestellt',
  `h_community_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halal_community_bills`
--

CREATE TABLE `halal_community_bills` (
  `h_c_bill_id` int(11) NOT NULL,
  `h_c_bill_client_id` int(11) NOT NULL,
  `h_c_bill_description` varchar(200) NOT NULL,
  `h_c_bill_amount` decimal(8,2) NOT NULL,
  `h_c_bill_type` varchar(50) NOT NULL DEFAULT 'Einzahlung',
  `h_c_bill_date` date NOT NULL,
  `h_c_bill_paid_date` date DEFAULT NULL,
  `h_c_bill_notice` varchar(1000) DEFAULT NULL,
  `h_c_bill_user_id` int(11) NOT NULL DEFAULT '0',
  `h_c_bill_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `halal_community_types`
--

CREATE TABLE `halal_community_types` (
  `h_c_type_id` int(11) NOT NULL,
  `h_c_type_name` varchar(30) NOT NULL,
  `h_c_type_amount` decimal(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `halal_community_types`
--

INSERT INTO `halal_community_types` (`h_c_type_id`, `h_c_type_name`, `h_c_type_amount`) VALUES
(1, 'Direct Member', '0.00'),
(3, 'Flex Investor', '249.00'),
(4, 'Premium Investor', '249.00'),
(5, 'Neutral Investor', '33.20');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_attachments`
--

CREATE TABLE `hc_attachments` (
  `hc_attach_id` int(11) NOT NULL,
  `hc_attach_project_id` int(11) NOT NULL,
  `hc_attach_type` varchar(30) NOT NULL,
  `hc_attach_name` varchar(250) NOT NULL,
  `hc_attach_order` int(11) DEFAULT NULL,
  `hc_attach_path` varchar(250) NOT NULL,
  `hc_attach_user_id` int(11) NOT NULL,
  `hc_attach_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_investment_bills`
--

CREATE TABLE `hc_investment_bills` (
  `invest_bill_id` int(11) NOT NULL,
  `invest_bill_project_id` int(11) NOT NULL,
  `invest_bill_client_id` int(11) NOT NULL,
  `invest_bill_amount` decimal(8,2) DEFAULT NULL,
  `invest_bill_profit` decimal(10,0) DEFAULT NULL,
  `invest_bill_status` varchar(20) NOT NULL DEFAULT 'offen',
  `invest_bill_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_options`
--

CREATE TABLE `hc_options` (
  `hc_option_id` int(11) NOT NULL,
  `hc_option_description` varchar(255) NOT NULL,
  `hc_option_key` varchar(200) NOT NULL,
  `hc_option_value` varchar(255) NOT NULL,
  `hc_option_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `hc_options`
--

INSERT INTO `hc_options` (`hc_option_id`, `hc_option_description`, `hc_option_key`, `hc_option_value`, `hc_option_date`) VALUES
(1, 'Gebühren von HalalCommunity Kunde an HalalCommunity', 'hc_gebühren', '200', '2018-10-29 16:38:17'),
(2, 'Gebühren von HalalCommunity Kunde an Mitarbeiter', 'user_hc_gebühren', '49', '2018-10-29 16:38:17'),
(3, 'Gebühren von Inspire Kunde an HalalCommunity in Prozente               i', 'inspire_gebühren', '30.303030', '2018-10-29 16:38:17'),
(4, 'Gebühren von Inspire Kunde an Mitarbeiter der Kunde Gebracht hat in Prozente', 'user_inspire_gebühren', '28.7878788', '2018-10-29 16:38:17'),
(5, 'Anteil der Provision an Berater von ein Inspire Projekt', 'hco_inspire_adviser', '16.666666', '2018-12-26 18:30:07'),
(6, 'Anteil an Halalcommunity wenn ein Projekt verkauft wird', 'hco_commission', '42.424245', '2018-12-26 18:34:55'),
(7, 'Provision Anteil für Mitarbeiter von Gewinn eines Projekt aus siene Mitglieder', 'user_hc_invest_profit', '4', '2019-01-26 00:43:41');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_profits`
--

CREATE TABLE `hc_profits` (
  `hc_profit_id` int(11) NOT NULL,
  `hc_profit_user_id` int(11) DEFAULT NULL,
  `hc_profit_client_id` int(11) DEFAULT NULL,
  `hc_profit_amount` decimal(8,2) NOT NULL,
  `hc_profit_from_client` int(11) DEFAULT NULL,
  `hc_profit_from_project` int(11) DEFAULT NULL,
  `hc_profit_description` varchar(200) NOT NULL,
  `hc_profit_type` varchar(15) NOT NULL DEFAULT 'hc',
  `hc_profit_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_projects`
--

CREATE TABLE `hc_projects` (
  `hc_project_id` int(11) NOT NULL,
  `hc_project_title` varchar(255) NOT NULL,
  `hc_project_description` mediumtext NOT NULL,
  `hc_project_type` varchar(30) NOT NULL DEFAULT 'ander',
  `hc_project_avatar` varchar(255) NOT NULL,
  `hc_project_coast` decimal(10,2) NOT NULL,
  `hc_project_retail_sell` decimal(8,2) DEFAULT NULL,
  `hc_project_investors` tinyint(3) NOT NULL,
  `hc_project_start` date NOT NULL,
  `hc_project_end` date NOT NULL,
  `hc_project_status` varchar(20) NOT NULL,
  `hc_project_tags` varchar(255) DEFAULT NULL,
  `hc_project_added_by` int(11) NOT NULL,
  `hc_project_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_repayments`
--

CREATE TABLE `hc_repayments` (
  `hc_repayment_id` int(11) NOT NULL,
  `hc_repayment_client_id` int(11) NOT NULL,
  `hc_repayment_project_id` int(11) NOT NULL,
  `hc_repayment_amount` decimal(10,2) NOT NULL,
  `hc_repayment_invest` decimal(9,2) NOT NULL,
  `hc_repayment_profit` decimal(9,2) NOT NULL,
  `hc_repayment_end` date NOT NULL,
  `hc_repayment_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hc_user_members`
--

CREATE TABLE `hc_user_members` (
  `user_member_id` int(11) NOT NULL,
  `user_member_user_id` int(11) NOT NULL,
  `user_member_percent` decimal(12,6) NOT NULL,
  `user_member_type` varchar(30) NOT NULL,
  `user_member_status` varchar(50) DEFAULT NULL,
  `user_member_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `idea_form`
--

CREATE TABLE `idea_form` (
  `idea_id` int(10) NOT NULL,
  `idea_project_id` int(10) NOT NULL,
  `idea_come_from` text,
  `idea_special` text,
  `idea_behind` text,
  `idea_current_status` text,
  `idea_generate_sales` text,
  `idea_funding_levels` text,
  `idea_investors` text,
  `idea_start_date` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `inspire_template`
--

CREATE TABLE `inspire_template` (
  `hc_project_id` int(11) NOT NULL,
  `hc_project_title` varchar(255) NOT NULL,
  `hc_project_description` mediumtext NOT NULL,
  `hc_project_type` varchar(30) NOT NULL DEFAULT 'Inspire',
  `hc_project_avatar` varchar(255) NOT NULL,
  `hc_project_coast` decimal(8,2) NOT NULL,
  `hc_project_investors` tinyint(2) NOT NULL,
  `hc_project_start` date NOT NULL,
  `hc_project_end` date NOT NULL,
  `hc_project_status` varchar(20) NOT NULL,
  `hc_project_tags` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `inspire_template`
--

INSERT INTO `inspire_template` (`hc_project_id`, `hc_project_title`, `hc_project_description`, `hc_project_type`, `hc_project_avatar`, `hc_project_coast`, `hc_project_investors`, `hc_project_start`, `hc_project_end`, `hc_project_status`, `hc_project_tags`) VALUES
(1, 'Northgate', '\r\n<div class=\"pro-headline-opt\">Schönes Renault Laguna 2005</div>\r\n<div class=\"pro-sub-headline-opt\">Beschreibung</div>\r\n<div class=\"pro-description-opt\">Baujahr: 2005<br />Farbe: Schwarz<br />Km: 200000</div>\r\n', 'Inspire', '18.jpg', '4970.00', 6, '2018-10-01', '2022-10-01', 'Publish', 'Northgate,Miete,Wohnung');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `insurance`
--

CREATE TABLE `insurance` (
  `insurance_id` int(11) NOT NULL,
  `insurance_text` varchar(255) NOT NULL,
  `insurance_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Haram  - 1: Halal'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `invests`
--

CREATE TABLE `invests` (
  `invest_id` int(11) NOT NULL,
  `invest_project_id` int(11) NOT NULL,
  `invest_client_id` int(11) NOT NULL,
  `invest_amount` varchar(100) NOT NULL,
  `invest_step` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `invest_attachments`
--

CREATE TABLE `invest_attachments` (
  `in_attach_id` int(11) NOT NULL,
  `in_attach_invest_id` int(11) NOT NULL,
  `in_attach_name` varchar(255) NOT NULL,
  `in_attach_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_attach_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `know_us`
--

CREATE TABLE `know_us` (
  `know_us_id` int(3) NOT NULL,
  `know_us_name` varchar(50) NOT NULL,
  `know_us_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `know_us`
--

INSERT INTO `know_us` (`know_us_id`, `know_us_name`, `know_us_timestamp`) VALUES
(1, 'Facebook', '2017-11-21 21:50:12'),
(2, 'Google', '2017-11-21 22:04:42'),
(3, 'Muslim Mindeset', '2017-11-21 22:04:59'),
(4, 'GoMecca', '2017-11-23 10:32:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `level_units`
--

CREATE TABLE `level_units` (
  `l_unit_id` int(20) NOT NULL,
  `l_unit_user_id` int(5) NOT NULL,
  `l_unit_amount` decimal(10,0) NOT NULL,
  `l_unit_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `login`
--

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL,
  `login_client_id` int(11) NOT NULL,
  `login_username` varchar(255) NOT NULL,
  `login_password` varchar(255) NOT NULL,
  `login_site` varchar(255) NOT NULL,
  `login_avatar` varchar(255) DEFAULT NULL,
  `login_status` tinyint(1) NOT NULL DEFAULT '1',
  `login_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mecca_trips`
--

CREATE TABLE `mecca_trips` (
  `m_trip_id` int(11) NOT NULL,
  `m_trip_type` varchar(8) NOT NULL,
  `m_trip_title` varchar(80) NOT NULL,
  `m_trip_n_price` decimal(6,2) DEFAULT '0.00',
  `m_trip_c_price` decimal(6,2) DEFAULT '0.00',
  `m_trip_r_price` decimal(6,2) DEFAULT '0.00',
  `m_trip_organization_id` int(3) NOT NULL,
  `m_trip_added_by` int(4) NOT NULL,
  `m_trip_go_departure_airport` varchar(100) DEFAULT NULL,
  `m_trip_go_departure_date` datetime DEFAULT NULL,
  `m_trip_go_arrival_airport` varchar(100) DEFAULT NULL,
  `m_trip_go_arrival_date` datetime DEFAULT NULL,
  `m_trip_go_airport_stop` varchar(100) DEFAULT NULL,
  `m_trip_go_departure_date_stop` datetime DEFAULT NULL,
  `m_trip_go_arrival_date_stop` datetime DEFAULT NULL,
  `m_trip_return_departure_airport` varchar(100) DEFAULT NULL,
  `m_trip_return_departure_date` datetime DEFAULT NULL,
  `m_trip_return_arrival_airport` varchar(100) DEFAULT NULL,
  `m_trip_return_arrival_date` datetime DEFAULT NULL,
  `m_trip_return_airport_stop` varchar(100) DEFAULT NULL,
  `m_trip_return_departure_date_stop` datetime DEFAULT NULL,
  `m_trip_return_arrival_date_stop` datetime DEFAULT NULL,
  `m_trip_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mecca_trip_information`
--

CREATE TABLE `mecca_trip_information` (
  `mti_id` int(11) NOT NULL,
  `mti_ma_in_direct_flight` varchar(255) NOT NULL,
  `mti_ma_co_direct_flight` varchar(255) DEFAULT NULL,
  `mti_me_in_direct_flight` varchar(255) DEFAULT NULL,
  `mti_me_co_direct_flight` varchar(255) DEFAULT NULL,
  `mti_ma_in_baggage` varchar(255) DEFAULT NULL,
  `mti_ma_co_baggage` varchar(255) DEFAULT NULL,
  `mti_me_in_baggage` varchar(255) DEFAULT NULL,
  `mti_me_co_baggage` varchar(255) DEFAULT NULL,
  `mti_avatar` varchar(255) DEFAULT NULL,
  `mti_ma_in_transfer_from` varchar(255) DEFAULT NULL,
  `mti_ma_co_transfer_from` varchar(255) DEFAULT NULL,
  `mti_me_in_transfer_from` varchar(255) DEFAULT NULL,
  `mti_me_co_transfer_from` varchar(255) DEFAULT NULL,
  `mti_ma_in_hotel_star` varchar(255) DEFAULT NULL,
  `mti_ma_co_hotel_star` varchar(255) DEFAULT NULL,
  `mti_me_in_hotel_star` varchar(255) DEFAULT NULL,
  `mti_me_co_hotel_star` varchar(255) DEFAULT NULL,
  `mti_ma_in_distance_from` varchar(255) DEFAULT NULL,
  `mti_ma_co_distance_from` varchar(255) DEFAULT NULL,
  `mti_me_in_distance_from` varchar(255) DEFAULT NULL,
  `mti_me_co_distance_from` varchar(255) DEFAULT NULL,
  `mti_ma_in_day_at_hotel` varchar(255) DEFAULT NULL,
  `mti_ma_co_day_at_hotel` varchar(255) DEFAULT NULL,
  `mti_me_in_day_at_hotel` varchar(255) DEFAULT NULL,
  `mti_me_co_day_at_hotel` varchar(255) DEFAULT NULL,
  `mti_ma_in_food` varchar(255) DEFAULT NULL,
  `mti_ma_co_food` varchar(255) DEFAULT NULL,
  `mti_me_in_food` varchar(255) DEFAULT NULL,
  `mti_me_co_food` varchar(255) DEFAULT NULL,
  `mti_ma_in_wifi` varchar(255) DEFAULT NULL,
  `mti_ma_co_wifi` varchar(255) DEFAULT NULL,
  `mti_me_in_wifi` varchar(255) DEFAULT NULL,
  `mti_me_co_wifi` varchar(255) DEFAULT NULL,
  `mti_ma_in_visit_mazarat` varchar(255) DEFAULT NULL,
  `mti_ma_co_visit_mazarat` varchar(255) DEFAULT NULL,
  `mti_me_in_visit_mazarat` varchar(255) DEFAULT NULL,
  `mti_me_co_visit_mazarat` varchar(255) DEFAULT NULL,
  `mti_trip_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `meetings`
--

CREATE TABLE `meetings` (
  `meeting_id` int(11) NOT NULL,
  `meeting_title` varchar(255) DEFAULT NULL,
  `meeting_details_id` int(11) DEFAULT NULL,
  `meeting_user_id` int(11) NOT NULL,
  `meeting_client_id` int(11) NOT NULL,
  `meeting_status` tinyint(4) NOT NULL DEFAULT '0',
  `meeting_begin_date` datetime DEFAULT NULL,
  `meeting_end_date` datetime DEFAULT NULL,
  `meeting_location` varchar(10) DEFAULT 'Vor Ort',
  `meeting_confirmed` tinyint(1) DEFAULT NULL,
  `meeting_unconfirmed_reason` varchar(50) DEFAULT NULL,
  `meeting_unconfirmed_text` varchar(500) DEFAULT NULL,
  `meeting_unconfirmed_date` datetime DEFAULT NULL,
  `meeting_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `moslem_trips`
--

CREATE TABLE `moslem_trips` (
  `moslem_trip_id` int(11) NOT NULL,
  `moslem_trip_mecca_id` int(11) NOT NULL,
  `moslem_trip_add_by` int(11) NOT NULL,
  `moslem_trip_code` varchar(250) NOT NULL,
  `moslem_trip_opened` datetime DEFAULT NULL,
  `moslem_trip_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `notifications`
--

CREATE TABLE `notifications` (
  `noti_id` int(11) NOT NULL,
  `noti_type` varchar(100) NOT NULL,
  `noti_type_id` int(11) NOT NULL,
  `noti_from` int(11) NOT NULL,
  `noti_to` int(11) NOT NULL,
  `noti_client_id` int(11) DEFAULT NULL,
  `noti_seen` tinyint(1) NOT NULL DEFAULT '0',
  `noti_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `not_confirmed`
--

CREATE TABLE `not_confirmed` (
  `nc_id` int(11) NOT NULL,
  `nc_name` varchar(50) COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `online_messages`
--

CREATE TABLE `online_messages` (
  `o_message_id` int(11) NOT NULL,
  `o_message_site` varchar(255) NOT NULL,
  `o_message_name` varchar(255) NOT NULL,
  `o_message_email` varchar(255) NOT NULL,
  `o_message_question` text NOT NULL,
  `o_message_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `organizations`
--

CREATE TABLE `organizations` (
  `organization_id` int(3) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `organization_added_by` int(11) NOT NULL,
  `organization_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_title` varchar(255) NOT NULL,
  `project_description` text,
  `project_begin` date DEFAULT NULL,
  `project_runtime` date DEFAULT NULL,
  `project_folder` varchar(100) DEFAULT NULL,
  `project_profile` varchar(255) NOT NULL,
  `project_client_id` int(11) NOT NULL,
  `project_required_capital` varchar(20) NOT NULL,
  `project_has_capital` varchar(20) DEFAULT NULL,
  `project_price` varchar(20) DEFAULT NULL,
  `project_type` tinyint(3) NOT NULL,
  `project_contracts` varchar(255) NOT NULL,
  `project_tags` varchar(255) NOT NULL,
  `project_watches` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `project_attachments`
--

CREATE TABLE `project_attachments` (
  `attachment_id` int(11) NOT NULL,
  `attachment_project_id` int(11) NOT NULL,
  `attachment_type` tinyint(4) DEFAULT NULL COMMENT '1: Image, 2: Dokuments',
  `attachment_name` varchar(255) DEFAULT NULL,
  `attachment_extension` varchar(25) NOT NULL,
  `attachment_path` varchar(255) DEFAULT NULL,
  `attachment_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shared_contract_units`
--

CREATE TABLE `shared_contract_units` (
  `shared_id` int(100) NOT NULL,
  `shared_contract_id` int(10) NOT NULL,
  `shared_user_id` int(10) NOT NULL,
  `shared_units` varchar(10) DEFAULT NULL,
  `shared_contract_type` tinyint(3) NOT NULL,
  `shared_contract_start` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `signature`
--

CREATE TABLE `signature` (
  `signature_id` int(11) NOT NULL,
  `signature_title` varchar(250) NOT NULL,
  `signature_text` text NOT NULL,
  `signature_add_by` int(11) NOT NULL,
  `signature_for` tinyint(4) NOT NULL DEFAULT '1',
  `signature_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `silent_units`
--

CREATE TABLE `silent_units` (
  `s_unit_id` int(20) NOT NULL,
  `s_unit_user_id` int(5) NOT NULL,
  `s_unit_amount` double(10,2) NOT NULL,
  `s_unit_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tickets`
--

CREATE TABLE `tickets` (
  `ticket_id` int(11) NOT NULL,
  `ticket_from` int(11) NOT NULL,
  `ticket_to` varchar(30) NOT NULL,
  `ticket_title` varchar(255) DEFAULT NULL,
  `ticket_text` text,
  `ticket_status` tinyint(1) NOT NULL DEFAULT '1',
  `ticket_privacy` varchar(25) NOT NULL DEFAULT 'Public',
  `ticket_allow_comments` tinyint(1) NOT NULL,
  `ticket_open` tinyint(1) NOT NULL DEFAULT '1',
  `ticket_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `todos`
--

CREATE TABLE `todos` (
  `todo_id` int(15) NOT NULL,
  `todo_title` varchar(250) NOT NULL,
  `todo_text` text,
  `todo_status` tinyint(4) NOT NULL DEFAULT '0',
  `todo_by_user_id` int(11) NOT NULL,
  `todo_end_date` datetime DEFAULT NULL,
  `todo_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `todo_list`
--

CREATE TABLE `todo_list` (
  `todo_list_id` int(11) NOT NULL,
  `todo_list_text` varchar(255) NOT NULL,
  `todo_list_status` tinyint(4) NOT NULL DEFAULT '0',
  `todo_list_for_todo` int(11) NOT NULL,
  `todo_list_by_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `todo_users`
--

CREATE TABLE `todo_users` (
  `todo_user_id` int(11) NOT NULL,
  `todo_user_todo_id` int(11) DEFAULT NULL,
  `todo_user_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_firstname` varchar(150) CHARACTER SET latin1 NOT NULL,
  `user_lastname` varchar(150) CHARACTER SET latin1 NOT NULL,
  `user_name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `user_password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `user_email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT '3',
  `user_avatar` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT 'no-pic.jpg',
  `user_country` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `user_state` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `user_zipcode` varchar(8) CHARACTER SET latin1 DEFAULT NULL,
  `user_place` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `user_street` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `user_housenr` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `user_phone` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `user_mobile` varchar(20) CHARACTER SET latin1 NOT NULL,
  `user_activate` tinyint(4) NOT NULL DEFAULT '1',
  `user_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_notifications`
--

CREATE TABLE `user_notifications` (
  `u_noti_id` int(11) NOT NULL,
  `u_noti_type` int(2) NOT NULL,
  `u_noti_from` varchar(50) NOT NULL,
  `u_noti_to` int(11) NOT NULL,
  `u_noti_seen` tinyint(1) NOT NULL DEFAULT '0',
  `u_noti_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `zakat_calculator`
--

CREATE TABLE `zakat_calculator` (
  `zakat_id` int(11) NOT NULL,
  `zakat_email` varchar(200) NOT NULL,
  `zakat_client_id` int(11) NOT NULL,
  `zakat_bargeld` decimal(8,2) NOT NULL,
  `zakat_girokonto` decimal(8,2) NOT NULL,
  `zakat_aktien` decimal(8,2) NOT NULL,
  `zakat_sparbuch` decimal(8,2) NOT NULL,
  `zakat_beteiligungskonto` decimal(8,2) NOT NULL,
  `zakat_bausparvertrag` decimal(8,2) NOT NULL,
  `zakat_payback_schulden` decimal(8,2) NOT NULL,
  `zakat_rentenversicherung` decimal(8,2) NOT NULL,
  `zakat_kryptowaehrung` decimal(8,2) NOT NULL,
  `zakat_sharia_investment` decimal(8,2) NOT NULL,
  `zakat_non_sharia_investment` decimal(8,2) NOT NULL,
  `zakat_gold_detail` tinyint(1) NOT NULL DEFAULT '0',
  `zakat_gold_pauschal` decimal(8,2) NOT NULL,
  `zakat_goldsparplan` decimal(8,2) NOT NULL,
  `zakat_goldbarren` decimal(8,2) NOT NULL,
  `zakat_goldmuenzen` decimal(8,2) NOT NULL,
  `zakat_goldschmuck` decimal(8,2) NOT NULL,
  `zakat_goldschmuck_anlage` decimal(8,2) NOT NULL,
  `zakat_gold_dinge` decimal(8,2) NOT NULL,
  `zakat_gold_price` decimal(8,2) DEFAULT NULL,
  `zakat_silber_detail` tinyint(1) NOT NULL DEFAULT '0',
  `zakat_silber_pauschal` decimal(8,2) NOT NULL,
  `zakat_silberbarren` decimal(8,2) NOT NULL,
  `zakat_silbermuenzen` decimal(8,2) NOT NULL,
  `zakat_silberschmuck` decimal(8,2) NOT NULL,
  `zakat_silberschmuck_anlage` decimal(8,2) NOT NULL,
  `zakat_silber_dinge` decimal(8,2) NOT NULL,
  `zakat_silber_price` decimal(8,2) DEFAULT NULL,
  `zakat_kredit_schulden` decimal(8,2) NOT NULL,
  `zakat_kredit_datum` tinyint(4) NOT NULL,
  `zakat_bafoeg_schulden` decimal(8,2) NOT NULL,
  `zakat_bafoeg_datum` tinyint(4) NOT NULL,
  `zakat_andere_schulden` decimal(8,2) NOT NULL,
  `zakat_andere_datum` tinyint(4) NOT NULL,
  `zakat_haendler` varchar(10) NOT NULL,
  `zakat_rechtsschule` varchar(100) NOT NULL,
  `zakat_full` tinyint(2) NOT NULL DEFAULT '1',
  `zakat_step` int(3) DEFAULT NULL,
  `zakat_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `advice_meta`
--
ALTER TABLE `advice_meta`
  ADD PRIMARY KEY (`advice_meta_id`);

--
-- Indizes für die Tabelle `affiliate_infos`
--
ALTER TABLE `affiliate_infos`
  ADD PRIMARY KEY (`a_i_id`);

--
-- Indizes für die Tabelle `affiliate_users`
--
ALTER TABLE `affiliate_users`
  ADD PRIMARY KEY (`a_u_id`);

--
-- Indizes für die Tabelle `avb_clients`
--
ALTER TABLE `avb_clients`
  ADD PRIMARY KEY (`avb_client_id`);

--
-- Indizes für die Tabelle `avb_documents`
--
ALTER TABLE `avb_documents`
  ADD PRIMARY KEY (`avb_docu_id`);

--
-- Indizes für die Tabelle `avb_partitions`
--
ALTER TABLE `avb_partitions`
  ADD PRIMARY KEY (`avb_part_id`);

--
-- Indizes für die Tabelle `avb_reservations`
--
ALTER TABLE `avb_reservations`
  ADD PRIMARY KEY (`avb_res_id`);

--
-- Indizes für die Tabelle `avb_shares`
--
ALTER TABLE `avb_shares`
  ADD PRIMARY KEY (`avb_share_id`);

--
-- Indizes für die Tabelle `avb_trucks`
--
ALTER TABLE `avb_trucks`
  ADD PRIMARY KEY (`avb_truck_id`);

--
-- Indizes für die Tabelle `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indizes für die Tabelle `booking_attachments`
--
ALTER TABLE `booking_attachments`
  ADD PRIMARY KEY (`b_attach_id`);

--
-- Indizes für die Tabelle `booking_documents`
--
ALTER TABLE `booking_documents`
  ADD PRIMARY KEY (`b_document_id`);

--
-- Indizes für die Tabelle `booking_payments`
--
ALTER TABLE `booking_payments`
  ADD PRIMARY KEY (`b_payment_id`);

--
-- Indizes für die Tabelle `building_form`
--
ALTER TABLE `building_form`
  ADD PRIMARY KEY (`building_id`);

--
-- Indizes für die Tabelle `car_form`
--
ALTER TABLE `car_form`
  ADD PRIMARY KEY (`car_id`);

--
-- Indizes für die Tabelle `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`c_make_id`);

--
-- Indizes für die Tabelle `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`c_model_id`);

--
-- Indizes für die Tabelle `car_pays`
--
ALTER TABLE `car_pays`
  ADD PRIMARY KEY (`car_pay_id`);

--
-- Indizes für die Tabelle `car_sales`
--
ALTER TABLE `car_sales`
  ADD PRIMARY KEY (`car_sale_id`);

--
-- Indizes für die Tabelle `car_search`
--
ALTER TABLE `car_search`
  ADD PRIMARY KEY (`car_search_id`);

--
-- Indizes für die Tabelle `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indizes für die Tabelle `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indizes für die Tabelle `client_advice`
--
ALTER TABLE `client_advice`
  ADD PRIMARY KEY (`c_advice_id`);

--
-- Indizes für die Tabelle `client_attachments`
--
ALTER TABLE `client_attachments`
  ADD PRIMARY KEY (`client_attach_id`);

--
-- Indizes für die Tabelle `client_children`
--
ALTER TABLE `client_children`
  ADD PRIMARY KEY (`c_child_id`);

--
-- Indizes für die Tabelle `client_details`
--
ALTER TABLE `client_details`
  ADD PRIMARY KEY (`c_detail_id`);

--
-- Indizes für die Tabelle `client_status`
--
ALTER TABLE `client_status`
  ADD PRIMARY KEY (`c_status_id`);

--
-- Indizes für die Tabelle `client_types`
--
ALTER TABLE `client_types`
  ADD PRIMARY KEY (`c_type_id`);

--
-- Indizes für die Tabelle `client_users`
--
ALTER TABLE `client_users`
  ADD PRIMARY KEY (`cu_id`);

--
-- Indizes für die Tabelle `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indizes für die Tabelle `contract_gold`
--
ALTER TABLE `contract_gold`
  ADD PRIMARY KEY (`c_gold_id`);

--
-- Indizes für die Tabelle `contract_gold_rates`
--
ALTER TABLE `contract_gold_rates`
  ADD PRIMARY KEY (`gold_rate_id`);

--
-- Indizes für die Tabelle `contract_inaia`
--
ALTER TABLE `contract_inaia`
  ADD PRIMARY KEY (`c_inaia_id`);

--
-- Indizes für die Tabelle `contract_problems`
--
ALTER TABLE `contract_problems`
  ADD PRIMARY KEY (`c_problem_id`);

--
-- Indizes für die Tabelle `contract_takaful`
--
ALTER TABLE `contract_takaful`
  ADD PRIMARY KEY (`c_takaful_id`);

--
-- Indizes für die Tabelle `contract_takaful_rates`
--
ALTER TABLE `contract_takaful_rates`
  ADD PRIMARY KEY (`takaful_rate_id`);

--
-- Indizes für die Tabelle `current_price`
--
ALTER TABLE `current_price`
  ADD PRIMARY KEY (`c_price_id`);

--
-- Indizes für die Tabelle `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`email_id`);

--
-- Indizes für die Tabelle `email_tracking`
--
ALTER TABLE `email_tracking`
  ADD PRIMARY KEY (`email_id`);

--
-- Indizes für die Tabelle `gold_contracts`
--
ALTER TABLE `gold_contracts`
  ADD PRIMARY KEY (`g_contract_id`);

--
-- Indizes für die Tabelle `gold_contract_payments`
--
ALTER TABLE `gold_contract_payments`
  ADD PRIMARY KEY (`g_c_payment_id`);

--
-- Indizes für die Tabelle `halalcheck24`
--
ALTER TABLE `halalcheck24`
  ADD PRIMARY KEY (`h_check_id`);

--
-- Indizes für die Tabelle `halal_auto`
--
ALTER TABLE `halal_auto`
  ADD PRIMARY KEY (`h_car_id`);

--
-- Indizes für die Tabelle `halal_auto_attachments`
--
ALTER TABLE `halal_auto_attachments`
  ADD PRIMARY KEY (`hc_attach_id`);

--
-- Indizes für die Tabelle `halal_auto_message`
--
ALTER TABLE `halal_auto_message`
  ADD PRIMARY KEY (`h_a_id`);

--
-- Indizes für die Tabelle `halal_community`
--
ALTER TABLE `halal_community`
  ADD PRIMARY KEY (`h_community_id`);

--
-- Indizes für die Tabelle `halal_community_bills`
--
ALTER TABLE `halal_community_bills`
  ADD PRIMARY KEY (`h_c_bill_id`);

--
-- Indizes für die Tabelle `halal_community_types`
--
ALTER TABLE `halal_community_types`
  ADD PRIMARY KEY (`h_c_type_id`);

--
-- Indizes für die Tabelle `hc_attachments`
--
ALTER TABLE `hc_attachments`
  ADD PRIMARY KEY (`hc_attach_id`);

--
-- Indizes für die Tabelle `hc_investment_bills`
--
ALTER TABLE `hc_investment_bills`
  ADD PRIMARY KEY (`invest_bill_id`);

--
-- Indizes für die Tabelle `hc_options`
--
ALTER TABLE `hc_options`
  ADD PRIMARY KEY (`hc_option_id`);

--
-- Indizes für die Tabelle `hc_profits`
--
ALTER TABLE `hc_profits`
  ADD PRIMARY KEY (`hc_profit_id`);

--
-- Indizes für die Tabelle `hc_projects`
--
ALTER TABLE `hc_projects`
  ADD PRIMARY KEY (`hc_project_id`);

--
-- Indizes für die Tabelle `hc_repayments`
--
ALTER TABLE `hc_repayments`
  ADD PRIMARY KEY (`hc_repayment_id`);

--
-- Indizes für die Tabelle `hc_user_members`
--
ALTER TABLE `hc_user_members`
  ADD PRIMARY KEY (`user_member_id`);

--
-- Indizes für die Tabelle `idea_form`
--
ALTER TABLE `idea_form`
  ADD PRIMARY KEY (`idea_id`);

--
-- Indizes für die Tabelle `inspire_template`
--
ALTER TABLE `inspire_template`
  ADD PRIMARY KEY (`hc_project_id`);

--
-- Indizes für die Tabelle `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`insurance_id`);

--
-- Indizes für die Tabelle `invests`
--
ALTER TABLE `invests`
  ADD PRIMARY KEY (`invest_id`);

--
-- Indizes für die Tabelle `invest_attachments`
--
ALTER TABLE `invest_attachments`
  ADD PRIMARY KEY (`in_attach_id`);

--
-- Indizes für die Tabelle `know_us`
--
ALTER TABLE `know_us`
  ADD PRIMARY KEY (`know_us_id`);

--
-- Indizes für die Tabelle `level_units`
--
ALTER TABLE `level_units`
  ADD PRIMARY KEY (`l_unit_id`);

--
-- Indizes für die Tabelle `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indizes für die Tabelle `mecca_trips`
--
ALTER TABLE `mecca_trips`
  ADD PRIMARY KEY (`m_trip_id`);

--
-- Indizes für die Tabelle `mecca_trip_information`
--
ALTER TABLE `mecca_trip_information`
  ADD PRIMARY KEY (`mti_id`);

--
-- Indizes für die Tabelle `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`meeting_id`);

--
-- Indizes für die Tabelle `moslem_trips`
--
ALTER TABLE `moslem_trips`
  ADD PRIMARY KEY (`moslem_trip_id`);

--
-- Indizes für die Tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`noti_id`);

--
-- Indizes für die Tabelle `not_confirmed`
--
ALTER TABLE `not_confirmed`
  ADD PRIMARY KEY (`nc_id`);

--
-- Indizes für die Tabelle `online_messages`
--
ALTER TABLE `online_messages`
  ADD PRIMARY KEY (`o_message_id`);

--
-- Indizes für die Tabelle `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`organization_id`);

--
-- Indizes für die Tabelle `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indizes für die Tabelle `project_attachments`
--
ALTER TABLE `project_attachments`
  ADD PRIMARY KEY (`attachment_id`);

--
-- Indizes für die Tabelle `shared_contract_units`
--
ALTER TABLE `shared_contract_units`
  ADD PRIMARY KEY (`shared_id`);

--
-- Indizes für die Tabelle `signature`
--
ALTER TABLE `signature`
  ADD PRIMARY KEY (`signature_id`);

--
-- Indizes für die Tabelle `silent_units`
--
ALTER TABLE `silent_units`
  ADD PRIMARY KEY (`s_unit_id`);

--
-- Indizes für die Tabelle `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indizes für die Tabelle `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`todo_id`);

--
-- Indizes für die Tabelle `todo_list`
--
ALTER TABLE `todo_list`
  ADD PRIMARY KEY (`todo_list_id`);

--
-- Indizes für die Tabelle `todo_users`
--
ALTER TABLE `todo_users`
  ADD PRIMARY KEY (`todo_user_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indizes für die Tabelle `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`u_noti_id`);

--
-- Indizes für die Tabelle `zakat_calculator`
--
ALTER TABLE `zakat_calculator`
  ADD PRIMARY KEY (`zakat_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `advice_meta`
--
ALTER TABLE `advice_meta`
  MODIFY `advice_meta_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `affiliate_infos`
--
ALTER TABLE `affiliate_infos`
  MODIFY `a_i_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `affiliate_users`
--
ALTER TABLE `affiliate_users`
  MODIFY `a_u_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `avb_clients`
--
ALTER TABLE `avb_clients`
  MODIFY `avb_client_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `avb_documents`
--
ALTER TABLE `avb_documents`
  MODIFY `avb_docu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `avb_partitions`
--
ALTER TABLE `avb_partitions`
  MODIFY `avb_part_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `avb_reservations`
--
ALTER TABLE `avb_reservations`
  MODIFY `avb_res_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `avb_shares`
--
ALTER TABLE `avb_shares`
  MODIFY `avb_share_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `avb_trucks`
--
ALTER TABLE `avb_trucks`
  MODIFY `avb_truck_id` tinyint(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `bookings`
--
ALTER TABLE `bookings`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `booking_attachments`
--
ALTER TABLE `booking_attachments`
  MODIFY `b_attach_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `booking_documents`
--
ALTER TABLE `booking_documents`
  MODIFY `b_document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `booking_payments`
--
ALTER TABLE `booking_payments`
  MODIFY `b_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `building_form`
--
ALTER TABLE `building_form`
  MODIFY `building_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `car_form`
--
ALTER TABLE `car_form`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `car_make`
--
ALTER TABLE `car_make`
  MODIFY `c_make_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `car_model`
--
ALTER TABLE `car_model`
  MODIFY `c_model_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `car_pays`
--
ALTER TABLE `car_pays`
  MODIFY `car_pay_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `car_sales`
--
ALTER TABLE `car_sales`
  MODIFY `car_sale_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `car_search`
--
ALTER TABLE `car_search`
  MODIFY `car_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `client_advice`
--
ALTER TABLE `client_advice`
  MODIFY `c_advice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `client_attachments`
--
ALTER TABLE `client_attachments`
  MODIFY `client_attach_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `client_children`
--
ALTER TABLE `client_children`
  MODIFY `c_child_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `client_details`
--
ALTER TABLE `client_details`
  MODIFY `c_detail_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `client_status`
--
ALTER TABLE `client_status`
  MODIFY `c_status_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT für Tabelle `client_types`
--
ALTER TABLE `client_types`
  MODIFY `c_type_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT für Tabelle `client_users`
--
ALTER TABLE `client_users`
  MODIFY `cu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `contract_gold`
--
ALTER TABLE `contract_gold`
  MODIFY `c_gold_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `contract_gold_rates`
--
ALTER TABLE `contract_gold_rates`
  MODIFY `gold_rate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `contract_inaia`
--
ALTER TABLE `contract_inaia`
  MODIFY `c_inaia_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `contract_problems`
--
ALTER TABLE `contract_problems`
  MODIFY `c_problem_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `contract_takaful`
--
ALTER TABLE `contract_takaful`
  MODIFY `c_takaful_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `contract_takaful_rates`
--
ALTER TABLE `contract_takaful_rates`
  MODIFY `takaful_rate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `current_price`
--
ALTER TABLE `current_price`
  MODIFY `c_price_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `emails`
--
ALTER TABLE `emails`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `email_tracking`
--
ALTER TABLE `email_tracking`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `gold_contracts`
--
ALTER TABLE `gold_contracts`
  MODIFY `g_contract_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `gold_contract_payments`
--
ALTER TABLE `gold_contract_payments`
  MODIFY `g_c_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halalcheck24`
--
ALTER TABLE `halalcheck24`
  MODIFY `h_check_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halal_auto`
--
ALTER TABLE `halal_auto`
  MODIFY `h_car_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halal_auto_attachments`
--
ALTER TABLE `halal_auto_attachments`
  MODIFY `hc_attach_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halal_auto_message`
--
ALTER TABLE `halal_auto_message`
  MODIFY `h_a_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halal_community`
--
ALTER TABLE `halal_community`
  MODIFY `h_community_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halal_community_bills`
--
ALTER TABLE `halal_community_bills`
  MODIFY `h_c_bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `halal_community_types`
--
ALTER TABLE `halal_community_types`
  MODIFY `h_c_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `hc_attachments`
--
ALTER TABLE `hc_attachments`
  MODIFY `hc_attach_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `hc_investment_bills`
--
ALTER TABLE `hc_investment_bills`
  MODIFY `invest_bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `hc_options`
--
ALTER TABLE `hc_options`
  MODIFY `hc_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `hc_profits`
--
ALTER TABLE `hc_profits`
  MODIFY `hc_profit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `hc_projects`
--
ALTER TABLE `hc_projects`
  MODIFY `hc_project_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `hc_repayments`
--
ALTER TABLE `hc_repayments`
  MODIFY `hc_repayment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `hc_user_members`
--
ALTER TABLE `hc_user_members`
  MODIFY `user_member_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `idea_form`
--
ALTER TABLE `idea_form`
  MODIFY `idea_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `inspire_template`
--
ALTER TABLE `inspire_template`
  MODIFY `hc_project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `insurance`
--
ALTER TABLE `insurance`
  MODIFY `insurance_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `invests`
--
ALTER TABLE `invests`
  MODIFY `invest_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `invest_attachments`
--
ALTER TABLE `invest_attachments`
  MODIFY `in_attach_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `know_us`
--
ALTER TABLE `know_us`
  MODIFY `know_us_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `level_units`
--
ALTER TABLE `level_units`
  MODIFY `l_unit_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `login`
--
ALTER TABLE `login`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `mecca_trips`
--
ALTER TABLE `mecca_trips`
  MODIFY `m_trip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `mecca_trip_information`
--
ALTER TABLE `mecca_trip_information`
  MODIFY `mti_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `meetings`
--
ALTER TABLE `meetings`
  MODIFY `meeting_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `moslem_trips`
--
ALTER TABLE `moslem_trips`
  MODIFY `moslem_trip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `notifications`
--
ALTER TABLE `notifications`
  MODIFY `noti_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `not_confirmed`
--
ALTER TABLE `not_confirmed`
  MODIFY `nc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `online_messages`
--
ALTER TABLE `online_messages`
  MODIFY `o_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `organizations`
--
ALTER TABLE `organizations`
  MODIFY `organization_id` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `project_attachments`
--
ALTER TABLE `project_attachments`
  MODIFY `attachment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `shared_contract_units`
--
ALTER TABLE `shared_contract_units`
  MODIFY `shared_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `signature`
--
ALTER TABLE `signature`
  MODIFY `signature_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `silent_units`
--
ALTER TABLE `silent_units`
  MODIFY `s_unit_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `todos`
--
ALTER TABLE `todos`
  MODIFY `todo_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `todo_list`
--
ALTER TABLE `todo_list`
  MODIFY `todo_list_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `todo_users`
--
ALTER TABLE `todo_users`
  MODIFY `todo_user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `u_noti_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `zakat_calculator`
--
ALTER TABLE `zakat_calculator`
  MODIFY `zakat_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
