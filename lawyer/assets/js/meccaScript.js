$(document).ready(function(){

    // Generate modal when click button add discount in page booking details
    $('#addDiscount').click(function(event) {
        event.preventDefault(); // don't submit multiple times and prevent actual mehtod
        $('#addDiscountModel').modal('show'); //Open the model
    });

    // submitted yes to  add discount in page booking details
    $('#submitted_result_yes').click(function(event){
        $('#addDiscountModel').modal('hide');
        $('#addDiscount').submit();
    });

     // submitted edit add status(yes - no) to send email in new edit booking detail or no) in page booking details
    $('#edit_booking').click(function(event) {
        event.preventDefault(); // don't submit multiple times and prevent actual mehtod
        $('#myModalEdit').modal('show'); //Open the model
    });

    $('#singlebutton').click(function(event) {
        event.preventDefault(); // don't submit multiple times and prevent actual mehtod
        $('#myModal').modal('show'); //Open the model
    });


    $('#submitted_result').click(function(event){
        $('#myModal').modal('hide');
        var date = $('#current-status-call option:selected').text();
        $('#contact_form').submit();
    });


    $('#excel-submit-booking').click(function(event) {
        event.preventDefault();
        /* declare an checkbox array */
        window.location.href = base_url + 'Mecca/export_excel_airline/';
    });


    $('#change_trip_cat').change(function () {
        var cat = $(this).val();
        if( cat == 'Ohne Flug' ){
            cat = 'm_trip_n_price';
        }else if ( cat == 'Komfort'){
            cat = 'm_trip_c_price';
        }
        var trip_id = $('#change_trip_id').val();
        var val = [trip_id, cat];
        $.post(base_url + 'Mecca/ajax_requests/change_booking',{data:val},function(price){
            $('.change_child-selector').html(price);
        });
    });

    //Pass client id from landing page clients when confirming or declining them to modal
    $('.confirm-h2h-lp').click(function(){
        var id = $(this).data('id');
        $('#confirm-lp-id').val(id);
    });

    $('.decline-h2h-lp').click(function(){
        var id = $(this).data('id');
        $('#decline-lp-id').val(id);
    });
    


    // subitted the data to generate file excel general
    $('#submitted_generieren').click(function(event){
        $('#extract_excel_table_details_modal').modal('hide');
        $('#form_excel_generation').submit();
    });

 
  $(function () {
            $('#datetimepicker11').datetimepicker({
                daysOfWeekDisabled: [0, 6]
            });
        });  
        
  
   
     });
 
 $(document).ready(function(){
    $('.show_edit_option').click(function(event){
     
        var id = $(this).data('id');
           
        $('.span_option_'+ id).hide();
        $('.text_option_'+ id).removeClass('hidden').focus();
        $('.text_option_'+ id).focusout(function() {
            var option = $(this).val();
            
            $(this).hide();
            $('.span_option_'+ id).text(option);
            $('.span_option_'+ id).show();
            var val = [option, id];
        
            $.post( base_url + 'Mecca/ajax_requests/edit_option_value',{data:val},function(data){
             
               // console.log(data);
                //window.location.reload(true);
            });
        });
    });  
    });
    
    
    
    


/////  We add this functoion to filter the page and we use it in booking list  AA    ///////    
 $('#trip_type_booking_list').change(function(){
                                 
                var organization = $("#organization_name_booking_list").val();
                var trip_type = $('#trip_type_booking_list').val();
                var val = [organization,trip_type];
                    
        $.post(base_url + 'Mecca/ajax_requests/get_trips_by_organization_with_type',{data:val},function(trip_title_option){
            var ensure =  $('#trip_title_option').html(trip_title_option);
            if(trip_title_option == ''){
                $('#trip_title_show_booking_list').fadeOut();
            } else{
                                              
                $('#trip_title_show_booking_list').fadeIn(20);
                $('#trip_title_option').html(trip_title_option);
            }
        });

    });


    $('#organization_name_booking_list').change(function(){
        $('#trip_title_show_booking_list').fadeOut();
    });

/////  We add this functoion to filter the page and we use it in booking list  ZZ    ///////



// Get trips for excel file  //
    $('#extract_excel_table_details').click(function(event) {
        event.preventDefault(); // don't submit multiple times and prevent actual mehtod
        $('#extract_excel_table_details_modal').show(); //Open the model
        $('#trip_type').change(function(){
            $('#trip_title_show').fadeOut();
            
            var organization = $('#organization_name').val();
            var trip_type = $('#trip_type').val();
            var val = [organization,trip_type];
            $.post(base_url + 'Mecca/ajax_requests/get_trips_by_organization_with_type',{data:val},function(trip_title){
                var ensure =  $('#trip_title').html(trip_title);
                if(trip_title == ''){
                    $('#trip_title_show').fadeOut();
                } else{
                    $('#trip_title_show').fadeIn();
                    $('#trip_title').html(trip_title);
                }
            });
        });

        //// Bring Another solution ***** before uploading
        $('#organization_name').change(function(){
            
            $('#trip_title_show').fadeOut();
            var organization = $("#organization_name").val();
            var trip_type = $('#trip_type').val();
            var val = [organization,trip_type];
            $.post(base_url + 'Mecca/ajax_requests/get_trips_by_organization_with_type',{data:val},function(trip_title){
                var ensure =  $('#trip_title').html(trip_title);
                if(trip_title == ''){
                    $('#trip_title_show').fadeOut();
                } else{
                    $('#trip_title_show').fadeIn();
                    $('#trip_title').html(trip_title);
                }
            });
        });
    });
    
// Get trips for excel file  //



// Generate the simple fck editor in oage edt trip  //  
$('.summernote-h2h').summernote({
  placeholder: 'Dein Text',  
  height: 150,   //set editable area's height
 
});
$('#trip_statistic').change(function(){

            var trip_statistic = $(this).val();
            window.location.href= base_url + 'Mecca/trip_statistic/'+trip_statistic;
            
});