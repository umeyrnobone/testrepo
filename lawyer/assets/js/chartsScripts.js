$(document).ready(function(){
    if ($("#capital-pie").length) {

        $.ajax({
            url: base_url + "HalalCommunity/charts/hc-overview",
            type: "GET",
            dataType: "json",
            success: function (data) {

                new Chart(document.getElementById("capital-pie"), {
                    type: 'pie',
                    data: {
                        labels: ["Eingezahlt", "Investition", "zur Verfügung", "Gebühren", "Insgesamt Investiert", "Gewinn", "Insgesamt ausgezahlt"],
                        datasets: [{
                            label: "Population (millions)",
                            backgroundColor: ["#77bdeb", "#013a5e","#fdc506","#fa983a","#ff6b6b", "#0be881", "#eb3b5a"],
                            data: [data['payments'], data['investments'], data['available'], data['fees'], data['all_investments'], data['profit'], data['repayments']]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Gesammteinzahlungen Halalcommunity'
                        },
                        tooltips: {
                            callbacks: {
                                // this callback is used to create the tooltip label
                                label: function(tooltipItem, data) {
                                    // get the data label and data value to display
                                    // convert the data value to local string so it uses a comma seperated number
                                    var dataLabel = data.labels[tooltipItem.index];
                                    var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]+ ' €';

                                    // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
                                    if (Chart.helpers.isArray(dataLabel)) {
                                        // show value on first line of multiline label
                                        // need to clone because we are changing the value
                                        dataLabel = dataLabel.slice();
                                        dataLabel[0] += value;
                                    } else {
                                        dataLabel += value;
                                    }

                                    // return the text to display on the tooltip
                                    return dataLabel;
                                }
                            }
                        }
                    }
                });


            },
            error: function () {
                console.log('Chart Daten konnten nicht geladen werden...');
            }
        });
    }

    if ($("#accounts-pie").length) {

        var hc_ck     = getCheckedHcFilter();
        var from_date = $('.from-date').val();
        var until_date = $('.until-date').val();
        var val = [hc_ck, from_date, until_date];

        $.post( base_url + "HalalCommunity/charts/hc_accounts" ,{data: val},function(data){

            var result = JSON.parse(data);
            new Chart(document.getElementById("accounts-pie"), {

                type: 'pie',
                data: {
                    labels: ["Auszahlung", "Investition", "Rückgabe", "Gewinn", "Einzahlung", "Einzahlung - Reinvestition"],
                    datasets: [{
                        label: "Population (millions)",
                        backgroundColor: ["#77bdeb", "#013a5e","#fdc506","#fa983a","#ff6b6b", "#0be881", "#eb3b5a"],
                        data: [result['paid'], result['investment'], result['repayment'], result['profit'], result['deposit'], result['reinvestment']]
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Gesammteinzahlungen Halalcommunity'
                    },
                    tooltips: {
                        callbacks: {
                            // this callback is used to create the tooltip label
                            label: function(tooltipItem, data) {
                                // get the data label and data value to display
                                // convert the data value to local string so it uses a comma seperated number
                                var dataLabel = data.labels[tooltipItem.index];
                                var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]+ ' €';

                                // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
                                if (Chart.helpers.isArray(dataLabel)) {
                                    // show value on first line of multiline label
                                    // need to clone because we are changing the value
                                    dataLabel = dataLabel.slice();
                                    dataLabel[0] += value;
                                } else {
                                    dataLabel += value;
                                }

                                // return the text to display on the tooltip
                                return dataLabel;
                            }
                        }
                    }
                }
            });

        });

    }

    if ($("#hc_member-pie").length) {

        $.post( base_url + "HalalCommunity/charts/hc_members" ,function(data){

            var result = JSON.parse(data);
            var keys   = result['keys'];
            var values = result['values'];

            new Chart(document.getElementById("hc_member-pie"), {

                type: 'pie',
                data: {
                    labels: keys,
                    datasets: [{
                        label: "Population (millions)",
                        backgroundColor: ["#77bdeb", "#013a5e","#fdc506","#fa983a","#ff6b6b", "#0be881", "#eb3b5a"],
                        data: values
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Gesammteinzahlungen Halalcommunity'
                    },
                    tooltips: {
                        callbacks: {
                            // this callback is used to create the tooltip label
                            label: function(tooltipItem, data) {
                                // get the data label and data value to display
                                // convert the data value to local string so it uses a comma seperated number
                                var dataLabel = data.labels[tooltipItem.index];
                                var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]+ ' €';

                                // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
                                if (Chart.helpers.isArray(dataLabel)) {
                                    // show value on first line of multiline label
                                    // need to clone because we are changing the value
                                    dataLabel = dataLabel.slice();
                                    dataLabel[0] += value;
                                } else {
                                    dataLabel += value;
                                }

                                // return the text to display on the tooltip
                                return dataLabel;
                            }
                        }
                    }
                }
            });

        });

    }



    function getCheckedHcFilter(){
        /* declare an checkbox array */
        var chkArray = [];

        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        $(".account-cb:checked").each(function() {
            chkArray.push($(this).val());
        });

        /* we join the array separated by the comma */
        var selected;
        return selected = chkArray.join(',') ;

    }
});

// var resp ;
// $.ajax({
//     url: base_url + "HalalCommunity/ajax_request/get_accounts",
//     type: 'GET',
//     dataType: 'json',
//     async: false,
//     success : function(data) {
//         resp: {data};
//     }
// });
// console.log(resp);
// var dataSet = [
//     [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800" ]
// ];
//
// var table = $('#accountDt').DataTable({
//     data: dataSet,
//     columns: [
//         { title: 'h_c_bill_id'}
//     ]
// });



//
// $('#account-cb').on('change', function() {
//     if ($(this).is(':checked')) {
//         $.fn.dataTable.ext.search.push(
//             function(settings, data, dataIndex) {
//                 return data[0] > 27
//             }
//         )
//     } else {
//         $.fn.dataTable.ext.search.pop()
//     }
//     table.draw();
// });




// var ckbox = $('.account-cb');
// $('.account-cb').click(function () {
//
//     var a = [];
//     $(".account-cb:checked").each(function() {
//         a.push($(this).val());
//     });
//
//     $.ajax({
//         url: base_url + "HalalCommunity/overview/hc-overview",
//         type: "GET",
//         dataType: "json",
//         success: function (data) {
//
//         }
//     });
//
// });



