(function($) {

    "use strict";

    // Modals
    $('[data-toggle="tooltip"]').tooltip();
    $("#set-event-modal").modal({
        backdrop: 'static'
    });

    $("#set-unit-modal").modal({
        backdrop: 'static'
    });

    //Initializing the plugin ON/OFF Button
    $(".bs-switch-example").bootstrapSwitch();

    $(function(){
        // Icheck
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal-grey',
            radioClass: 'iradio_minimal-grey'
        });

        // Datatables
        //Setting default values
        $.extend( $.fn.dataTable.defaults,{
            dom:  '<"row" <"col-sm-6" l> <"col-sm-6" f> >'+ // Top section structure(search etc.)
                '<"row" <"col-sm-12" t> >'+                       // Table
                '<"row m-top-10" <"col-sm-6" i> <"col-sm-6" p> >',         // Bottom section structure (pagination etc.)
            autoWidth: false,
            order: [ [0, 'desc'] ],
            pagingType: 'full_numbers',
            language: {
                lengthMenu: '_MENU_',
                info: 'Showing page _PAGE_ of _PAGES_',
                infoEmpty: 'No records available',
                infoFiltered: '(From _MAX_ total records)',
                search: '_INPUT_',
                searchPlaceholder: 'Search...',
                paginate: {
                    next:       '<i class="fa fa-angle-right"></i>',
                    previous:   '<i class="fa fa-angle-left"></i>'
                },
            }
        });

        // Basic example

        $('#investor-list-table').DataTable(
            {
                "order": [[ 2, 'desc' ], [ 3, 'asc' ]],
                "pageLength": 10
            }
        );

        $('#investor-list').DataTable(
            {
                "order": [[ 4, "asc"], [ 0, "desc" ]],
                "pageLength": 1000
            }
        );

		$('.max_table').DataTable(
			{
				"order": [[ 4, "asc"], [ 0, "desc" ]],
				"pageLength": 1000
			}
		);

        // Checkbox example
        var dtCheckbox = $('.datatable-checkbox');
        dtCheckbox.DataTable({
            order: [ [1, 'desc'] ],
            columnDefs:[{
                targets: 0,
                orderable: false
            }],
            initComplete: function(settings, json){
                $('.icheck').on('ifChecked', function(event){
                    var thCheckbox = $(this).closest('th');
                    var tdCheckbox = $(this).closest('.table').find('tbody input.icheck');

                    if(thCheckbox.length)
                        tdCheckbox.iCheck('check');
                });

                $('.icheck').on('ifUnchecked', function(event){
                    var thCheckbox = $(this).closest('th');
                    var tdCheckbox = $(this).closest('.table').find('tbody input.icheck');

                    if(thCheckbox.length)
                        tdCheckbox.iCheck('uncheck');
                });
            }
        });

        // Vertical scroll example
        $('#datatable-vertical').DataTable({
            scrollY: '200px',
            scrollCollapse: true
        });

        // Horizontal scroll example
        $('#datatable-horizontal').DataTable({
            scrollX: true
        });

        // Row group example
        var dtRowGroup = $('#datatable-row-group');
        dtRowGroup.DataTable({
            columnDefs: [
                {
                    'visible': false,
                    'targets': 2
                }
            ],
            order: [ [2, 'asc'] ],
            displayLength: 25,
            drawCallback: function(settings){
                var api = this.api();
                var rows = api.rows({page:'current'}).nodes();
                var last=null;

                api.column(2, {page:'current'}).data().each(function(group, i){
                    if(last !== group) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        $("#summernote").summernote({
            placeholder: 'Produkt beschreibung',
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']]
            ],
            height: 150
        });

        $("#project-description").summernote({
            placeholder: 'Produkt beschreibung',
            toolbar: [
                ['font', [ 'underline']]
            ],
            height: 50
        });

        $(".summernote-h2h").summernote({
            placeholder: 'Dein Text ...',
            height: 150
        });


        var haram = $('#haram-value').attr('data-id');
        var halal = $('#halal-value').attr('data-id');

        var pie = new d3pie("pieChart", {
            "header": {
                "title": {
                    "text": "Versicherung",
                    "color": "#1d2ba5",
                    "fontSize": 23,
                    "font": "cinzel"
                },
                "subtitle": {
                    "text": "Deine % der Haram Versicherung",
                    "color": "#6581e8",
                    "fontSize": 8,
                    "font": "georgia"
                },
                "location": "pie-center",
                "titleSubtitlePadding": 13
            },
            "footer": {
                "color": "#999999",
                "fontSize": 10,
                "font": "georgia",
                "location": "bottom-center"
            },
            "size": {
                "canvasHeight": 300,
                "canvasWidth": 355,
                "pieInnerRadius": "80%",
                "pieOuterRadius": "76%"
            },
            "data": {
                "sortOrder": "value-desc",
                "content": [
                    {
                        "label": "Halal",
                        "value": parseInt(halal),
                        "color": "#0d9f5d"
                    },
                    {
                        "label": "Haram",
                        "value": parseInt(haram),
                        "color": "#ef2828"
                    }
                ]
            },
            "labels": {
                "outer": {
                    "format": "label-percentage2",
                    "pieDistance": 20
                },
                "inner": {
                    "format": "none"
                },
                "mainLabel": {
                    "color": "#1863d9",
                    "fontSize": 15
                },
                "percentage": {
                    "color": "#999999",
                    "fontSize": 11,
                    "decimalPlaces": 0
                },
                "value": {
                    "color": "#cccc43",
                    "fontSize": 11
                },
                "lines": {
                    "enabled": true,
                    "style": "straight"
                }
            },
            "tooltips": {
                "enabled": true,
                "type": "placeholder",
                "string": "{label}: {value}",
                "styles": {
                    "fadeInSpeed": 958,
                    "backgroundColor": "#241515",
                    "backgroundOpacity": 0.67,
                    "color": "#eefdfc",
                    "borderRadius": 3,
                    "fontSize": 15,
                    "padding": 7
                }
            },
            "effects": {
                "pullOutSegmentOnClick": {
                    "effect": "linear",
                    "speed": 400,
                    "size": 8
                }
            },
            "misc": {
                "colors": {
                    "segmentStroke": "#dfd4d4"
                },
                "canvasPadding": {
                    "top": 0,
                    "right": 0,
                    "bottom": 0,
                    "left": 0
                },
                "pieCenterOffset": {
                    "y": -20
                }
            }
        });

        // Order by the grouping
        dtRowGroup.find('tbody').on( 'click', 'tr.group', function(){
            var currentOrder = dtRowGroup.DataTable().order()[0];
            if(currentOrder[0] === 2 && currentOrder[1] === 'asc'){
                dtRowGroup.DataTable().order([2, 'desc']).draw();
            }else{
                dtRowGroup.DataTable().order([2, 'asc' ]).draw();
            }
        });

        // Select2
        $('select').select2({
            theme: 'bootstrap'
        });
    });


    // Initializing Inputmask( DATE dd/mm/yyyy)
    $("input[data-inputmask]").inputmask();

    // Bootstrap-notify plugin
    // Setting defaults
    $.notifyDefaults({
        offset: {
            y: 65,
            x: 20
        },
        onShow: function(){
            // Removes inline css that gives Bootstrap-notify plugin to the close button
            $(this).find('.close').removeAttr('style');
        }
    });

    // Bootstrap-notify examples
    // Show Add Ticket
    if( $('.ticket-page').hasClass('show-add-ticket') ){

        $.notify('Es wurde ein <strong>neues Ticket </strong> erfolgreich eingefügt.',{
            type: 'success',
            template: '<div data-notify="container" class="col-sm-4 p-0">'+
                '<div class="alert bg-{0} alert-dismissible"><button type="button" class="close" data-notify="dismiss">×</button>{2}</div>'+
                '</div>'
        });
    }

    $(function(){
        // Minimal skin (ICHECK Color )
        $('input.icheck-minimal-grey').iCheck({
            checkboxClass: 'icheckbox_minimal-grey',
            radioClass: 'iradio_minimal-grey'
        });

        $('input.icheck-minimal-blue').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('input.icheck-minimal-green').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass: 'iradio_minimal-green'
        });

        $('input.icheck-minimal-aero').iCheck({
            checkboxClass: 'icheckbox_minimal-aero',
            radioClass: 'iradio_minimal-aero'
        });

        $('input.icheck-minimal-orange').iCheck({
            checkboxClass: 'icheckbox_minimal-orange',
            radioClass: 'iradio_minimal-orange'
        });

        $('input.icheck-minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });

        // Square skin
        $('input.icheck-square-grey').iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        });

        $('input.icheck-square-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });

        $('input.icheck-square-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });

        $('input.icheck-square-aero').iCheck({
            checkboxClass: 'icheckbox_square-aero',
            radioClass: 'iradio_square-aero'
        });

        $('input.icheck-square-orange').iCheck({
            checkboxClass: 'icheckbox_square-orange',
            radioClass: 'iradio_square-orange'
        });

        $('input.icheck-square-red').iCheck({
            checkboxClass: 'icheckbox_square-red',
            radioClass: 'iradio_square-red'
        });

        // Flat skin
        $('input.icheck-flat-grey').iCheck({
            checkboxClass: 'icheckbox_flat-grey',
            radioClass: 'iradio_flat-grey'
        });

        $('input.icheck-flat-blue').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });

        $('input.icheck-flat-green').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('input.icheck-flat-aero').iCheck({
            checkboxClass: 'icheckbox_flat-aero',
            radioClass: 'iradio_flat-aero'
        });

        $('input.icheck-flat-orange').iCheck({
            checkboxClass: 'icheckbox_flat-orange',
            radioClass: 'iradio_flat-orange'
        });

        $('input.icheck-flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });

        // Line skin
        $('.icheck-line-grey').each(function(){
            var self = $(this),
                // Following the Bootstrap markup structure and being already located in the <label> we use a <small> that acts as a <label>.
                small = self.next(),
                small_text = small.text();

            small.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-grey',
                radioClass: 'iradio_line-grey',
                insert: '<div class="icheck_line-icon"></div>' + small_text
            });
        });

        $('.icheck-line-blue').each(function(){
            var self = $(this),
                // Following the Bootstrap markup structure and being already located in the <label> we use a <small> that acts as a <label>.
                small = self.next(),
                small_text = small.text();

            small.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-blue',
                radioClass: 'iradio_line-blue',
                insert: '<div class="icheck_line-icon"></div>' + small_text
            });
        });

        $('.icheck-line-green').each(function(){
            var self = $(this),
                // Following the Bootstrap markup structure and being already located in the <label> we use a <small> that acts as a <label>.
                small = self.next(),
                small_text = small.text();

            small.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-green',
                radioClass: 'iradio_line-green',
                insert: '<div class="icheck_line-icon"></div>' + small_text
            });
        });

        $('.icheck-line-aero').each(function(){
            var self = $(this),
                // Following the Bootstrap markup structure and being already located in the <label> we use a <small> that acts as a <label>.
                small = self.next(),
                small_text = small.text();

            small.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-aero',
                radioClass: 'iradio_line-aero',
                insert: '<div class="icheck_line-icon"></div>' + small_text
            });
        });

        $('.icheck-line-orange').each(function(){
            var self = $(this),
                // Following the Bootstrap markup structure and being already located in the <label> we use a <small> that acts as a <label>.
                small = self.next(),
                small_text = small.text();

            small.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-orange',
                radioClass: 'iradio_line-orange',
                insert: '<div class="icheck_line-icon"></div>' + small_text
            });
        });

        $('.icheck-line-red').each(function(){
            var self = $(this),
                // Following the Bootstrap markup structure and being already located in the <label> we use a <small> that acts as a <label>.
                small = self.next(),
                small_text = small.text();

            small.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-red',
                radioClass: 'iradio_line-red',
                insert: '<div class="icheck_line-icon"></div>' + small_text
            });
        });
    });


    $("#datetimepicker").datetimepicker({
        language:  'de',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        format: "dd.mm.yyyy h:i"
    });
    $("#datetimepicker2").datetimepicker({
        language:  'de',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        format: "dd.mm.yyyy h:i"
    });
    $("#datetimepicker3").datetimepicker({
        language:  'de',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        format: "dd.mm.yyyy h:i"
    });
    $("#datetimepicker4").datetimepicker({
        language:  'de',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        format: "dd.mm.yyyy h:i"
    });
    $(".datetimepickerClass").datetimepicker({
        language:  'de',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        format: "dd.mm.yyyy h:i"
    });

    $(function(){
        // Summernote example
        $("#summernote-editor").summernote();

    });


})(jQuery);




(function($) {
    "use strict";

    $(function(){

        /* initialize the calendar
        -----------------------------------------------------------------*/
        var myDate = new Date();
        var currentYear = myDate.getFullYear();
        var currentMonth = myDate.getMonth() + 1;
        currentMonth = (currentMonth < 10) ? 0 + '' + currentMonth: currentMonth;


        $("#dash-calendar").fullCalendar({
            header: {
                left: "prev,next,title",
                right: "month,agendaWeek,agendaDay,listWeek"
            },
            defaultView: 'agendaWeek',
            lang: 'de',
            editable: true,
            droppable: false, // this allows things to be dropped onto the calendar
            eventLimit: true, // allow "more" link when too many events

            events:  '/calendar/get_meetings',
            /*    events:  'https://myadenya.de/calendar/get_all_events',*/
            eventClick: function(calEvent) {
                window.location.href =  base_url + 'events/all_metings/' + calEvent.id;

            }

        });

        $("#avb-calendar").fullCalendar({
            header: {
                left: "prev,next,title",
                right: "month,agendaWeek,agendaDay,listWeek"
            },
            defaultView: 'agendaWeek',
            lang: 'de',
            editable: true,
            droppable: true,
            eventLimit: true,
            selectable:true,

            events:  '/calendar/show_avb_reservation',
            eventClick: function(calEvent) {
                window.location.href =  base_url + 'AVB/reservation_details/' + calEvent.id;
            },

            select: function ( start, end, jsEvent, view ){

                $('#AVBEventModal').modal('show');
                $('#AVBEventModal').find('#start_time').val(start);
                var s_day = moment(start ).format('DD.MM.YYYY HH:mm');
                var e_day = moment(end ).format('DD.MM.YYYY HH:mm');

                $('#AVBEventModal').find('#start_time').val(s_day);
                $('#AVBEventModal').find('#end_time').val(e_day);
            }


        });


        $("#my-calendar").fullCalendar({
            header: {
                left: "prev,next,title",
                right: "month,agendaWeek,agendaDay"
            },
            lang: 'de',
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar
            eventLimit: true, // allow "more" link when too many events
            events:  '/calendar/get_user_meetings',
            /*    events:  'https://myadenya    .de/calendar/get_all_events',*/
            eventClick: function(calEvent) {
                window.location.href =  base_url + 'events/all_metings/' + calEvent.id;
            },
        });
    });

    $('.mont-datepicker').datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        maxViewMode: 0,
        todayBtn: true,
        clearBtn: true,
        keyboardNavigation: false,
        autoclose: true,
        todayHighlight: true
    });

    $('#example1').tagsinput({
        tagClass: 'label label-success-outline',

    });


})(jQuery);



