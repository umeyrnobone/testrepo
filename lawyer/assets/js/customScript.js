$(document).ready(function(){
	$(document).on('click', '.car_status_changer', function(){
		var status = $(this).data('status');
		var meta_id = $(this).data('id');

		$('#meta_status').val(status);
		$('#current_status_span').text(status);
		$('#meta_id').val(meta_id);
	});

	$(document).on('click', '.show_loan_history', function(){
		var loan = $(this).data('id');

		$.post(base_url + 'Dashboard/ajax_request/get_loan_history/'+loan, function(response){
			$('#loan_details').empty();
			$('#loan_details').append(response);
		});
	});

	$(document).on('click', '.add_loan_notes', function () {
		var client = $(this).data('id');

		$('#client_id').val(client);
	});

	$('.display_documents').click(function(){
		var loan 	= $(this).data('id');
		var client 	= $(this).data('client');
		var data	= [loan, client];

		$.post(base_url + 'Dashboard/ajax_request/get_attachments',{data:data},function(response){
			$('#doc_container').empty();
			$('#doc_container').append(response);
		});
	});

	$('.client_notes').summernote({
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['fontsize', ['fontsize']],
			['color', ['color']]
		]
	});

	$('.mandant_notes').summernote({
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['fontsize', ['fontsize']],
			['color', ['color']]
		]
	});

	$('.upload-photo').change(function(){
		var id = $(this).data('id');

		$('#loan_form_'+id).submit();
	});

	$('.del_attach_btn').click(function(){
		var id = $(this).data('id');

		$('#del_attach_id').val(id);
	});

	/*
	*
	* ==== General cases ===
	*
	 */

	$(document).on('click', '.show_case_updates', function () {
		var case_id = $(this).data('id');

		$.post(base_url + 'Dashboard/ajax_request/show_case_history/'+case_id, function(response){
			var data = response;

			$('#case_history').empty();
			$('#case_history').append(data);

		});
	});

	$(document).on('click', '.add_case_notes', function () {
		var client = $(this).data('id');

		$('#client_id').val(client);
	});

	$(document).on('click', '.delete_lawyer_doc', function(){
		var path = $(this).data('path');

		$('#delete_path').val(path);
	});
});
