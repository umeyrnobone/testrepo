<?php
if (!$this->session->has_userdata('username_session')) {
    header("location: ".base_url('login/+'));
    exit;
}

if($this->session->userdata('user_type_session') == 1){
    $user_type = "Admin";
} elseif($this->session->userdata('user_type_session') == 2 ) {
    $user_type = "Verwalter";
}elseif($this->session->userdata('user_type_session') == 3 ) {
    $user_type = "Berater";
}elseif($this->session->userdata('user_type_session') == 101 ) {
	$user_type = "Gast";
} else {
    header("location: ".base_url('login/logout'));
    exit;
}

if($this->session->userdata('user_advice_client_id') && $this->session->userdata('user_advice')){
    redirect('Advice/to_page/financehouse/'.$this->session->userdata('user_advice_client_id'));
}

$user_menu = $this->session->userdata('user_menu');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRM</title>
    <link rel="icon" href="<?=base_url('assets/img/title.png')?>">

    <!-- REQUIRED STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic" rel="stylesheet" type="text/css">

    <link href="<?=base_url('assets/dist/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <!-- REQUIRED PLUGINS -->
    <link href="<?=base_url('assets/dist/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <link href="<?=base_url('assets/dist/plugins/animate.css/css/animate.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/icheck/css/skins/all.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/select2/select2-bootstrap-theme/css/select2-bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/summernote/css/summernote.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/flag-css/css/flag-css.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/fullcalendar/css/fullcalendar.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.min.css');?>" rel="stylesheet" type="text/css">

    <link href="<?=base_url('assets/dist/plugins/rippler/css/rippler.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/dist/plugins/datatables/css/dataTables.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php //base_url('assets/css/page-events.min.css');?>" rel="stylesheet" type="text/css">
    <!--      <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>-->

    <!-- main.min.css - WISEBOARD CORE CSS -->
    <link href="<?=base_url('assets/dist/css/main.min.css');?>" rel="stylesheet" type="text/css">
    <!-- plugins.min.css - ALL PLUGINS CUSTOMIZATIONS -->
    <link href="<?=base_url('assets/dist/css/plugins.min.css');?>" rel="stylesheet" type="text/css">
    <!-- admin.min.css - ADMIN LAYOUT -->
    <link href="<?=base_url('assets/css/admin.min.css');?>" rel="stylesheet" type="text/css">
    <!-- theme-default.min.css - DEFAULT THEME -->
    <link href="<?=base_url('assets/css/theme-default.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/page-inbox.min.css');?>" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="<?=base_url('assets/css/mecca.style.css');?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url('assets/css/custom.style.css');?>" rel="stylesheet" type="text/css">
    <?php if($this->uri->segment(2) == 'show_ticket' || $this->uri->segment(2) == 'add_ticket'):?>
        <link href="<?=base_url('assets/css/asksheikh.css');?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url('assets/css/asksheikh_base.css');?>" rel="stylesheet" type="text/css">
    <?php endif; ?>
	<?php if($this->session->userdata('inspire_button') != 1): ?>
		<style>
			.inspire-button{
				pointer-events: none;
			}
		</style>
	<?php endif; ?>

	<?php if($this->session->userdata('halalco_button') != 1): ?>
		<style>
			.halalco-button{
				pointer-events: none;
			}
		</style>
	<?php endif; ?>

	<style>
		#joy_div{
			position: absolute;
			z-index: 1;
			top: 20%;
			left: 35%;
			width: 600px;
			height: 600px;
			background: url('https://myadenya.de/assets/img/einfach_easy.gif') no-repeat;
			background-size: 100%;
		}
	</style>
</head>
<body>

<!-- TOP NAVIGATION STARTS -->
<nav class="navbar navbar-fixed-top navbar-top" <?php if($this->session->userdata('user_extra_advice')): ?> style="background-color: red;" <?php endif; ?>>
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?=base_url('Dashboard')?>">
                <img src="<?=base_url('assets/img/logo.svg')?>" alt="logo">
            </a>
            <button type="button" class="sidebar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div><!-- /.navbar-header -->


        <ul class="nav navbar-nav navbar-right">
            <?php if($this->session->userdata('user_extra_advice')): ?>
                <input type="hidden" id="advice_hours" value="<?= intval(date('H', strtotime('now') - $this->session->userdata('user_advice_begin') -3600)) ?>">
                <input type="hidden" id="advice_minutes" value="<?= intval(date('i', strtotime('now') - $this->session->userdata('user_advice_begin'))) ?>">
                <input type="hidden" id="advice_seconds" value="<?= intval(date('s', strtotime('now') - $this->session->userdata('user_advice_begin'))) ?>">
                <li><a><p style="font-size: 23px;"><b>BERATUNG LÄUFT NOCH! : </b></p></a></li>
                <li><a style="font-size: 23px;" href="<?= base_url('Dashboard/show_client_details/'.$this->session->userdata('user_advice_client_id')) ?>" data-toggle="modal" class="btn btn-danger">Seit <time></time></a></li>
            <?php endif; ?>

            <!-- User Profile -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?php if($this->uri->segment(1) == "Dashboard" && $this->uri->segment(2) == ""):?>
                    <img src="assets/images/blank_profilepic.png" alt="avatar" class="avatar">
                <?php endif;?>
                <?php if($this->uri->segment(2) != ""):?>
                    <img src="../assets/images/blank_profilepic.png" alt="avatar" class="avatar">
                <?php endif;?>
                </a>
                <ul class="dropdown-menu dropdown-menu-profile">
                    <li class="dropdown-header"><div style="font-size:16px;" class="text-center"><p class="badge badge-danger"><?= $this->session->userdata("username_session") ?></p></div>Meine Details</li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                            <?php if($this->uri->segment(1) == "Dashboard" && $this->uri->segment(2) == ""):?>
                                <img src="assets/images/blank_profilepic.png" alt="avatar" class="media-object img-circle">
                            <?php endif;?>
                            <?php if($this->uri->segment(2) != ""):?>
                                <img src="../assets/images/blank_profilepic.png" alt="avatar" class="media-object img-circle">
                            <?php endif;?>
                            </div>
                        </div><!-- /.media -->
                    </li>

                    <li>
                        <a href="<?=base_url('Login/logout');?>">
                        <i class="fas fa-sign-out-alt"></i> Abmelden
                        </a>
                    </li>
                </ul>
            </li><!-- /.dropdown -->

            <li class="dropdown" style="display: none">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-align-justify"></i>
                </a>
                <div class="sidebar-extra">
                    <div class="sidebar-extra-scroller">
                        <ul class="nav nav-tabs solid-tabs">
                            <li class="active">
                                <a href="#sidebar-timeline">Timeline</a>
                            </li>
                            <li>
                                <a href="#sidebar-mail">Mail</a>
                            </li>
                            <li>
                                <a href="#sidebar-settings">Settings</a>
                            </li>
                        </ul>


                    </div><!-- /.sidebar-extra-scroller -->
                </div><!-- /.sidebar-extra -->
            </li><!-- /.dropdown -->


        </ul><!-- /.navbar-nav -->

    </div>
</nav>
<!-- TOP NAVIGATION ENDS -->

<!-- CONTENT WRAPPER STARTS ( INCLUDES LEFT SIDEBAR AND CONTENT PART OF THE PAGE ) -->
<div class="content-wrapper">
    <!-- SIDEBAR STARTS -->
    <div class="sidebar">
        <ul class="sidebar-nav">
            <li>
                <a href="<?=base_url('Dashboard')?>">
                    <i class="fa fa-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url('Dashboard/show_clients')?>">
                    <i class="fa fa-users"></i>
                    <span>Kunden</span>
                </a>
            </li>

            <li>
                <a href="<?=base_url('Dashboard/show_job_lost')?>">
                    <i class="fa fa-users"></i>
                    <span>Arbeitsrecht/Kündigung</span>
                </a>
            </li>

			<li>
				<a href="<?=base_url('Dashboard/general_questions')?>">
					<i class="fa fa-gavel"></i>
					<span> Rechtliches Allgemein</span>
				</a>
			</li>
            
            <li>
				<a href="<?=base_url('Dashboard/new_lead')?>">
					<i class="fa fa-user-plus"></i>
					<span> Neuer Kunde</span>
				</a>
			</li>

            <li style="position:fixed; top:96%;">
				<a href="/insurance">
                <i class="fas fa-arrow-alt-circle-right"></i>					
                <span> Versicherungs CRM</span>
				</a>
			</li>
        </ul>
    </div>
    <!-- SIDEBAR ENDS -->

    <!-- CONTENT STARTS -->
    <div class="content">
        <?= $content ?>
    </div>
    <!-- CONTENT ENDS -->
</div>
<!-- CONTENT WRAPPER ENDS -->

<div class="modal fade" id="ticket_modal">
    <form action="<?= base_url('Tickets/add_ticket') ?>" method="POST" autocomplete="off">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-12">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Ticket erstellen</h3>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h4>Ticket an:</h4>
                                <select name="users[]" class="form-control" id="ticket-to-template">
                                    <option value="">Mitarbeiter auswählen</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <h4>Fällig am:</h4>
                                <input type="text" name="expire" class="datetimepicker form-control">
                            </div>

                            <div class="col-md-4">
                                <h4>Kunde, um den es geht:</h4>
                                <select name="tagged-client" id="ticket_client">
                                    <option value="">Kunden auswählen</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <h4>Titel:</h4>
                                <input type="text" name="title" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <h4>Nachricht:</h4>
                                <textarea class="form-control" name="message" cols="10" rows="15"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <h4>Ticket für alle sichtbar?</h4>
                                <input type="radio" name="privacy" value="0" id="secret-no"><label class="radio-label" for="secret-no">Ja</label>
                                <input type="radio" name="privacy" value="1" id="secret-yes"><label class="radio-label" for="secret-yes">Nein</label>

                                <input type="hidden" value="1" name="comments">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-halalcheck pull-left">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="hco_application_modal">
    <div class="modal-dialog modal-sm" role="dialog" style="margin-top: 13%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Neuer Halalco Antrag</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Antragsteller:</h5>

                        <form action="<?= base_url('Clients/redirect_to_details') ?>" method="POST" target="_blank">
                            <input type="hidden" id="hc_client_id" name="hc_client_id">
                            <div class="col-md-7" style="padding-left: 0;">
                                <input type="text" class="form-control" readonly id="application_name">
                            </div>
                            <div class="col-md-5" style="padding-right: 0;">
                                <button class="btn btn-primary pull-right">Zum Kunden</button>
                            </div>
                        </form>
                        <h5>Antragsdatum:</h5>
                        <input type="text" class="form-control" readonly id="application_date">
                        <h5>Hinzugefügt von:</h5>
                        <input type="text" class="form-control" readonly id="application_added_by">
                        <h5>Begründung der Absage:</h5>
                        <textarea class="form-control" cols="30" rows="6" id="reason_text"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <form action="<?= base_url('HalalCommunity/manage_newest_application') ?>" method="POST" target="_blank">
                    <input type="hidden" id="application_id" name="hc_id">
                    <input type="hidden" id="reason" name="reason">
                    <button class="btn btn-primary dismiss_application_modal pull-left" data-dismiss="modal" name="decision" value="confirm">Bestätigen</button>
                    <button class="btn btn-primary dismiss_application_modal" name="decision" value="postpone" style="margin-right: 3%; margin-left: 2%">Später</button>
                    <button class="btn btn-primary dismiss_application_modal" name="decision" value="decline">Ablehnen</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reminder_modal" data-backdrop="static">
    <div class="modal-dialog" role="dialog" style="margin-top: 13%">
        <form action="<?= base_url('Notifications/manage_reminder') ?>" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="reminder_from"></h5>
                </div>

                <div class="modal-body">
                    <p id="reminder_message"></p>
                </div>

                <div class="modal-footer">
                    <input type="hidden" id="reminder_id" name="reminder_id">
                    <input type="hidden" id="reminder_action" name="reminder_action">
                    <button class="btn btn-primary pull-left reminder-input" value="finished">Erledigt</button>
                    <button class="btn btn-primary pull-left reminder-input" value="snooze" style="margin-left: 1%">Erneut erinnern</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="create_reminder_modal">
    <div class="modal-dialog" role="dialog" style="margin-top: 11%; width: 25%">
        <form action="<?= base_url('Notifications/insert_reminder') ?>" method="POST" autocomplete="off">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>
                        Erinnerung einfügen
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </h5>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Empfänger</h5>
                            <div class="col-md-6" style="padding-left: 0;">
                                <select name="reminder_to" id="reminder-recipient-select" class="form-control" required>
                                    <option value="">Bitte auswählen</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5>Alle wie viel Minuten soll die Benachrichtigung wiederholt werden?</h5>
                            <div class="col-md-6" style="padding-left: 0;">
                                <select name="reminder_snooze" class="form-control" required>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="60">60</option>
                                    <option value="120">120</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5>Erste Erinnerung am</h5>
                            <div class="col-md-6" style="padding-left: 0;">
                                <input type="text" class="datetimepicker form-control" name="reminder_date" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5>Bei regelmäßiger Aufgabe bitte Zeitspanne auswählen</h5>
                            <div class="col-md-2" style="padding-left: 0">
                                <select name="reminder_interval_number">
                                    <option value=""></option>
                                    <?php for($i = 1; $i <= 31; $i++): ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select name="reminder_interval_span" class="form-control">
                                    <option value=""></option>
                                    <option value="days">Tag(e)</option>
                                    <option value="months">Monat(e)</option>
                                    <option value="weeks">Woche(n)</option>
                                    <option value="years">Jahr(e)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5>Nachricht</h5>
                            <textarea name="reminder_text" cols="30" rows="5" class="form-control" required></textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" value="Erinnerung einfügen" class="btn btn-primary pull-left">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="notes_reminder_modal" role="dialog" data-backdrop="static" style="z-index: 1600">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Du musst noch Notizen ausfüllen</h4>
			</div>

			<div class="modal-body">
				<h4>Kunde: <span id="reminder_client" style="color: #1d80d0"></span></h4>

				<p>
					As salamu alaikum wa rahmatullahi wa barakatuh <?= $this->session->userdata('user_fname_session') ?>,<br><br>

					Für den oben genannten Kunden müssen noch Notizen ausgefüllt werden! Du kannst dies entweder jetzt machen, oder dich in 1 Minute nochmal daran erinnern lassen!
				</p>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<a href="" class="btn btn-primary" id="enter_notes_btn">Jetzt ausfüllen</a>
					<a href="" class="btn btn-default" id="postpone_notes_btn">Nochmal erinnern</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    var base_url = "<?php echo base_url();?>";
</script>
<!-- REQUIRED SCRIPTS -->
<script src="<?=base_url('assets/dist/js/jquery.min.js');?>"></script>
<script src="<?=base_url('assets/dist/js/bootstrap.min.js');?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

<!-- REQUIRED PLUGINS -->
<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.resize.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.pie.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/flot/js/jquery.flot.time.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/flot/js/plugins/jquery.flot.orderBars.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/flot/js/plugins/jquery.flot.tooltip.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/jquery.sparkline/js/jquery.sparkline.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/moment/js/moment.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/jquery.slimscroll/js/jquery.slimscroll.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/bootstrap-switch/js/bootstrap-switch.min.js')?>"></script>
<script src="<?=base_url('assets/dist/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')?>"></script>
<script src="<?=base_url('assets/dist/plugins/jquery.succinct/js/jquery.succinct.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/icheck/js/icheck.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');?>"></script>
<script src="<?=base_url('assets/dist/js/bootstrap-datepicker.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/summernote/js/summernote.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/fullcalendar/js/fullcalendar.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/fullcalendar/js/de.js');?>"></script>
<script src="<?=base_url('assets/dist/js/d3.js') ?>"></script>
<script src="<?=base_url('assets/dist/js/d3pie.js') ?>"></script>

<script src="<?=base_url('assets/dist/plugins/select2/js/select2.full.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/bootstrap-notify/js/bootstrap-notify.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/rippler/js/jquery.rippler.min.js');?>"></script>
<script src="<?=base_url('assets/dist/plugins/datatables/js/dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/dist/plugins/jquery.inputmask/js/jquery.inputmask.bundle.min.js')?>"></script>
<script src="<?=base_url('assets/dist/plugins/bootstrap-wizard/js/jquery.bootstrap.wizard.min.js')?>"></script>
<script src="<?=base_url('assets/dist/plugins/jquery.validate/js/jquery.validate.min.js')?>"></script>


<!-- main.min.js - WISEBOARD CORE SCRIPT -->
<script src="<?=base_url('assets/dist/js/main.min.js');?>"></script>
<!-- admin.min.js - GENERAL CONFIGURATION SCRIPT FOR THE PAGES -->
<script src="<?=base_url('assets/js/admin.min.js');?>"></script>
<script src="<?=base_url('assets/js/demo/demo-form.js');?>"></script>
<script src="<?=base_url('assets/js/demo/demo-plugin-datatables-extensions.js')?>"></script>
<script src="<?=base_url('assets/js/plugin.js')?>"></script>
<script src="<?=base_url('assets/js/chartsScripts.js')?>"></script>

<script src="<?=base_url('assets/js/customScript.js')?>"></script>
<script src="<?=base_url('assets/js/meccaScript.js')?>"></script>
<script src="<?php //base_url('assets/js/vueScript.js')?>"></script>
<script src="https://kit.fontawesome.com/4b225514b6.js" crossorigin="anonymous"></script>
</body>
</html>
