<?php

function checkStatus($status, $current_status){
	if($status == $current_status){
		return 'selected';
	} else{
		return '';
	}
}

function statusColor($status){
	$status_array = array(
		1	=> '#D3D3D3',
		2	=> '#D3D3D3',
		3	=> '#fffaa1',
		4	=> '#ed6f6f',
		5	=> '#c6f7d6',
		6	=> '#ed6f6f'
	);

	return $status_array[$status];
}

?>

<div class="row">
	<div class="col-sm-12">
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="<?= base_url('Dashboard') ?>"><i class="fa fa-home"></i>Dashboard</a></li>
				<li><a href="">Kunden liste</a>
			</ol>

		</div><!-- /.breadcrumb-wrapper -->
	</div>
</div>

<h3 class="text-center"><?= $type ?></h3>
<hr>
<div class="col-md-12">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title pull-left">
						<i class="fa fa-table m-right-5"></i> <?= $type ?>
					</div><!-- /.panel-title -->
					<div class="panel-tools">
						<div class="tools-content">
							<!-- Though the light panel structure does not have div.tools-content (only default panel has), we put it here for the Datatables input that is appended in here via jQuery universal script dynamically -->
							<!-- NOTE: THERE GOES SEARCH INPUT WHICH WE ARE APPENDING HERE VIA JQUERY -->
						</div>
					</div><!-- /.panel-tools -->
				</div><!-- /.panel-heading -->
				<div class="panel-body">
					<div class="table-responsive">

						<div class="table-responsive">

							<table class="table table-striped table-bordered datatable-buttons">
								<thead>
								<tr>
									<th>Kunde</th>
									<th>Rufnummer</th>
                                    <th>E-Mail</th>
									<th>Beruf</th>
									<th>Rechtsart des Anliegens</th>
                                    <th>Rechtschutz vorhanden</th>
									<th>Status</th>
                                    <th style="width: 350px;">Aktion</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($clients as $client): ?>
                                    <?php $insurances = explode(', ', $client->client_insurance_id); ?>
									<tr>
										<td><a href="<?= base_url('Dashboard/show_case_details/').my_cryption($client->lawyer_help_id, 'e', 'hco_url') ?>" target="_blank"><?= $client->client_firstname.' '.$client->client_lastname ?></a></td>
										<td><?= $client->client_mobile != '' ? $client->client_mobile : $client->client_phone ?></td>
                                        <td><?= $client->client_email ?></td>
										<td><?= $client->client_job ?></td>
										<td><?= $client->l_help_type_name ?></td>
                                        <td><?php if(in_array(3, $insurances) == true){echo 'Vorhanden';} else{echo '-';} ?></td>
										<td style="width: 150px;">
											<span data-toggle="modal" data-id="<?= $client->lawyer_help_id ?>" data-status="<?= $client->l_status_name ?>" data-target="#change_status_modal" class="label block car_status_changer" style="cursor: pointer; background: <?= statusColor($client->lawyer_help_status) ?>">
												<?= $client->l_status_name ?>
											</span>
										</td>
										<td>
											<a href="#" class="btn btn-primary show_case_updates" data-id="<?= my_cryption($client->lawyer_help_id, 'e', 'hco_url') ?>" data-toggle="modal" data-target="#case_notes">Statusänderungen anzeigen</a>
											<a href="#" class="btn btn-primary add_case_notes" data-id="<?= my_cryption($client->client_id, 'e', 'hco_url') ?>" data-toggle="modal" data-target="#add_notes">Notizen einfügen</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div> <!-- /.table-responisive -->
					</div> <!-- /.table-responisive -->
				</div><!-- /.panel-body -->
			</div><!--/.panel-->
		</div><!-- /.col-sm-12 -->
	</div>

</div>

<div class="modal fade" id="change_status_modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/update_case_status') ?>" method="POST">
				<div class="modal-header">
					<h4>Status ändern</h4>
				</div>

				<div class="modal-body">
					<table class="table">
						<tr>
							<td>Aktueller Status</td>
							<td><span id="current_status_span"></span></td>
						</tr>

						<tr>
							<td>Neuer Status</td>
							<td>
								<select name="case_status" required>
									<option value="">Status auswählen</option>
									<?php foreach($status as $status): ?>
										<option value="<?= $status->l_status_id ?>"><?= $status->l_status_name ?></option>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>

						<tr>
							<td>Notizen</td>
							<td>
								<textarea name="update_notes" cols="30" rows="5" class="form-control" required></textarea>
							</td>
						</tr>
					</table>
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="meta_id" name="case_id">

						<input type="submit" class="btn btn-primary" value="Status ändern">
						<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="case_notes" role="dialog">
	<div class="modal-dialog" style="width: 950px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Details</h5>
			</div>

			<div class="modal-body">
				<div id="case_history">

				</div>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_notes" role="dialog">
	<div class="modal-dialog" style="width: 450px; height: 100px;">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/add_notes') ?>" method="POST">
				<div class="modal-header">
					<h5>Notizen einfügen</h5>
				</div>

				<div class="modal-body">
					<h4>Notizen zum Kunden einfügen</h4>
					<textarea name="notes"cols="30" rows="5" class="form-control client_notes"></textarea>
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" name="type" value="<?= my_cryption('lawyer', 'e', 'hco_url'); ?>">
						<input type="hidden" id="client_id" name="client_id">
						<input type="submit" value="Einfügen" class="btn btn-primary">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
					</div>
				</div>
		</div>
	</div>
</div>
