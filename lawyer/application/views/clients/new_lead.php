<style>

.puw-header{
    text-transform: uppercase;
    font-weight: 800;
    padding-top: 10px;
    padding-left: 15px;

}

label{
    color:grey;    
}

.puw{
    box-shadow: 1px 6px 20px grey;
    border-radius:6px;
    padding: 0 15px 15px 15px;
    margin-top: 20px;
    margin-left:-5px;
    margin-right:-5px;

}

.form-group{
    padding-top:7px;

}

.form-control,span{
    border-radius:5px;
}

.save:hover{
    color:white;
    height: 60px;
    width: 60px;
    padding-top: 12px;
    font-size:12px;
    text-decoration: none;
}

.save{
    font-weight: bold;
	position:fixed;
	bottom:56px;
	right: 18px;
	text-decoration: none;
    padding-top: 10px;
    padding-left: 3px;
    text-align: center;
    color: white;
    font-size: 11px;
    letter-spacing: 2px;
    text-transform: uppercase;
    width: 50px;
    height: 50px;
    background-color:rgb(66, 184, 50);
}

.save-icon{
    font-size:15px;
}

.submit-button{
    position:fixed;
    float:right;
    right:20px;
    font-size:15px;
    bottom:5px;

}


</style>
<br>
<?php if($this->session->flashdata('errors')): ?>
            <div style="color:black;" class="alert alert-warning">
            <?= $this->session->flashdata('errors'); ?>
            </div>
<?php endif; ?>
<form id="form_lead" action="<?= base_url("Dashboard/set_new_lead");?>") method="post">
    <div class="col-md-4 col-lg-4">
        <!--PERSÖNLICHE DATEN-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">PERSÖNLICHE DATEN</h5>
                <hr style="width: 95%;">
                <div class="col-md-6">
                    <label>Geschlecht</label>
                </div>
                <div class="col-md-6">
                <label for="male">Herr</label>
                <input id="male" name="client_gender" value="male" type="radio">
                <label for="female">Frau</label>
                <input id="female" name="client_gender" value="female" type="radio">
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="firstname">Vorname</label> <br>
                    <input class="form-control" type="text" name="client_firstname" id="client_firstname" placeholder="Vorname">
                </div>
                <div class="col-md-6">
                    <label for="lastname">Nachname</label> <br>
                    <input class="form-control input" type="text" name="client_lastname" id="lastname" placeholder="Nachname">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Familienstand</label>
                    <select name="client_family_stand">
                        <option disabled selected>Familienstand...</option>
                        <option value="ledig">Ledig</option>
                        <option value="verheiratet">Verheiratet</option>
                        <option value="geschieden">Geschieden</option>
                    </select>
                </div>    
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="birthday">Geburtsdatum</label> <br>
                    <input class="form-control" type="date" id="birthday" name="client_birthdate" placeholder="z.B 21.01.2001">
                </div>
                <div class="col-md-6">
                    <label for="birthday">Geburtsort</label> <br>
                    <input class="form-control" type="text" id="birthlocation" name="client_birth_city" placeholder="z.B Deutschland">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="birthday">Nationalität</label> <br>
                    <input class="form-control" type="text" id="nationality" name="client_nationality" placeholder="z.B Kurde">
                </div>
                <div class="col-md-6">
                    <label for="birthday">Herkunftsland</label> <br>
                    <input class="form-control" type="text" id="homeland" name="client_homeland" placeholder="z.B Kurdistan">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Berufs-Typ</label><br>
                    <select name="client_job_type">
                        <option disabled selected>Bitte auswählen...</option>
                        <option value="Selbstständig">Selbstständig</option>
                        <option value="Angestellt">Angestellt</option>
                        <option value="Student">Student</option>
                        <option value="Azubi">Azubi</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Beruf</label> <br>
                    <select name="client_job">
                        <option disabled selected>Bitte auswählen...</option>
                        <option value="Programmierer">Programmierer</option>
                        <option value="Polizist">Polizist</option>
                    </select>
                </div>
            </div>    
        </div>
        <!--PERSÖNLICHE DATEN ENDS-->

        <!--KINDER-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">Kinder</h5>
                <hr style="width: 95%;">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Anzahl der Kinder</label>
                </div>
                <div class="col-md-6">
                    <select name="client_kids">
                        <option selected disabled>Bitte auswählen...</option>
                        <option value="1">abc</option>
                        <option value="2">def</option>
                    </select>
                </div>
            </div>
        </div>
        <!--KINDER ENDS-->

        <!--UPLOAD-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">Bilder / Dokumete Hochladen</h5>
                <hr style="width: 95%;">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="file" name="client_file" id="">
                </div>
            </div>
        </div>
        <!--UPLOAD ENDS-->
    </div>

        
    <div class="col-md-4 col-lg-4">
        <!--KONTAKT DATEN-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">KONTAKT DATEN</h5>
                <hr style="width: 95%;">
                <div class="col-md-12">    
                    <label for="email">Email</label>
                    <input class="form-control" type="text" id="email" name="client_email" placeholder="z.B test@gmail.com">
                </div>
            </div>    
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="mobil">Mobil</label> <br>
                    <input class="form-control" type="text" id="mobil" name="client_mobile" placeholder="z.B 0178 4344 5035">
                </div> 
                <div class="col-md-6">
                    <label for="festnetz">Festnetz</label> <br>
                    <input class="form-control" type="text" id="festnetz" name="client_phone" placeholder="z.B 0176 5793 5035">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="country">Land</label> <br>
                    <input class="form-control" type="text" id="country" name="client_country" placeholder="z.B Deutschland">
                </div>    
                <div class="col-md-6">
                    <label>Bundesland</label>
                    <select name="client_state">
                        <option disabled selected>Bitte auswählen...</option>
                        <option value="Schleswig-Holstein">Schleswig-Holstein</option>
                        <option value="Hamburg">Hamburg</option>
                        <option value="Bremen">Bremen</option>
                        <option value="Mecklenburg-Vorpommern">Mecklenburg-Vorpommern</option>
                        <option value="Brandenbrug">Brandenbrug</option>
                        <option value="Berlin">Berlin</option>
                        <option value="Sachsen-Anhalt">Sachsen-Anhalt</option>
                        <option value="Sachsen">Sachsen</option>
                        <option value="Thüringen">Thüringen</option>
                        <option value="Niedersachsen">Niedersachsen</option>
                        <option value="Nordrhein-Westfalen">Nordrhein-Westfalen</option>
                        <option value="Hessen">Hessen</option>
                        <option value="Rheinland-Pfalz">Rheinland-Pfalz</option>
                        <option value="Saarland">Saarland</option>
                        <option value="Bayern">Bayern</option>
                        <option value="Baden-Württemberg">Baden-Württemberg</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-5">
                    <label for="plz">Postleitzahl</label> <br>
                    <input class="form-control" type="text" id="plz" name="client_zipcode" placeholder="z.B 44384">
                </div>
                <div class="col-md-7">
                    <label for="city">Stadt</label> <br>
                    <input class="form-control" type="text" id="city" name="client_city" placeholder="Dortmund">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="street">Straße</label> <br>
                    <input class="form-control" type="text" id="street" name="client_street" placeholder="z.B Siemenstraße 4">
                </div>
            </div>
        </div>
        <!--KONTAKT DATEN ENDS-->

        <!--EXTRA INFORMATIONEN-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">Extra informationen</h5>
                <hr style="width: 95%;">
                <div class="col-md-6">
                    <label>Kennt uns durch</label>
                    <select name="client_know_us_by">
                        <option selected disabled>Bitte auswählen...</option>
                        <option value="abc">abc</option>
                        <option value="def">def</option>
                    </select>  
                </div> 
                <div class="col-md-6">
                    <label>Oder&nbspindividueller&nbspeintrag</label>
                    <input type="text" class="form-control" placeholder="z.B Türkei Urlaub" name="client_know_us_by_custom" id="">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Generiert am</label>
                    <input type="date" class="form-control" name="client_generated_at" placeholder="20.9.2020" id="">
                </div> 
                <div class="col-md-6">
                    <label>Empfholen von</label>
                    <select name="client_recommended_by">
                        <option selected disabled>Bitte auswählen...</option>
                        <option value="1">abc</option>
                        <option value="2">def</option>
                    </select>  
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Notizen zum Kunden</label>
                </div>
                <div class="col-md-12">
                <textarea name="client_notice" class="client_notes" id="client_notes" cols="30" rows="30"></textarea>
                </div>
            </div>
        </div>
        <!--EXTRA INFORMATION ENDS-->
    </div>

    <div class="col-md-4 col-lg-4">
        <!--VORBERATUNG-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">VORBERATUNG</h5>
                <hr style="width: 95%;">
                <div class="form-group col-md-12">
                <label>Erfahrungen mit Islamic Finance</label>
                </div>
                <hr style="width: 95%;">
                <div class="col-md-12">
                    <textarea class="client_notes" name="client_experience_islamic_finance" id="client_notes" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Rechtschule</label>
                    <select name="client_madhab" id="">
                        <option value selected disabled>Bitte auswählen</option>
                        <option value="Hanafīya">Hanafi</option>
                        <option value="Mālikiya">Mālikiya</option>
                        <option value="Schāfiʿīya">Schāfiʿīya</option>
                        <option value="Hanbalīya">Hanbalīya</option>
                        <option value="Madhablos">Madhablos</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Wer entscheidet?</label>
                    <select name="client_decision_maker">
                        <option value selected disabled>Bitte auswählen</option>
                        <option value="1">Eltern</option>
                        <option value="2"></option>
                        <option value="3">hij</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Anderer&nbspEntscheidungspartner</label>
                    <select name="client_alt_decision_maker">
                        <option value selected disabled>Bitte auswählen</option>
                        <option value="1">abc</option>
                        <option value="2">def</option>
                        <option value="3">hij</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                <label>3 Wünsche an den Halalcheck</label><br>
                <label>1. Wunsch</label>
                <input type="text" name="client_first_wish" class="form-control">
                </div>
                <div class="col-md-12">
                    <label>2. Wunsch</label>
                <input type="text" name="client_second_wish" class="form-control">
                </div>
                <div class="col-md-12">
                    <label>3. Wunsch</label>
                <input type="text" name="client_third_wish" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Produktkriterien</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Flexibilität"> <label>Flexibilität</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Transparenz"> <label>Transparenz</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Geld abrufbar"> <label>Geld abrufbar</label><br>
                    <input type="checkbox" id="client_custom_req" name="client_custom_req"><input class="form-control" name="client_custom_req" type="text">
                </div>
                <div class="col-md-6">
                    <label></label><br>
                    <input type="checkbox" name="client_criteria[]" value="Islamkonform"> <label>Islamkonform</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Hohe Renche"> <label>Hohe Renche</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Geringe Kosten"> <label>Geringe Kosten</label><br>
                    <input type="checkbox" id="client_custom_req2" name="client_custom_req2"><input class="form-control" name="client_custom_req2" type="text">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Notizen</label>
                    <textarea class="client_notes" name="c_detail_notice" id="client_notes" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Antwort auf Bedingungsfrage</label> 
                    <label>Ja</label> 
                    <input type="radio" value="yes" name="client_req_question" id="yes"> 
                    <label>Nein</label> 
                    <input type="radio" value="no" name="client_req_question" id="no"> 
                </div>
                <div class="col-md-12">
                    <label>Bestätigung Bedingungsfrage</label> 
                    </label>Ja</label> 
                    <input type="radio" value="yes" name="client_req_question_confirm" id="yes"> 
                    <label>Nein</label> 
                    <input type="radio" value="no" name="client_req_question_confirm" id="no"> 
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Eigenkapital</label> 
                    <input type="text" name="client_capital" placeholder="z.B 9389€">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Versicherungen</label><br>
                    <input type="checkbox" name="client_ins[]" value="1"> <label>Kapital-Lebensversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="2"><label>Risiko-Lebensversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="3"> <label>Rechtsschutzversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="4"> <label>Rentenversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="5"> <label>Hausratversicherung</label><br>
                </div>
                    <div class="col-md-6">
                    <input type="checkbox" name="client_ins[]" value="6"> <label>Wohngebäudeversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="7"> <label>Privathaftpflichtversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="8"> <label>Zahnzusatzversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="9"> <label>KFZ Haftpflichtversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="10"> <label>Private Krankenversicherung</label><br>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Interesse</label><br>
                    <input type="checkbox" name="client_inter[]" value="1"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="2"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="3"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="4"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="5"> <label>Interessen</label><br>
                </div>
                <div class="col-md-6">
                    <input type="checkbox" name="client_inter[]" value="6"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="7"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="8"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="9"> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="10"> <label>Interessen</label><br>
                </div>
            </div>
        </div>
        <!--VORBERrATUNG ENDS-->
    </div>  
    <button class="submit-button btn btn-success">Submit</button>
</form>

<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
<script>

$(document).ready(function(){
    $("#submit").mouseover(function(){
        $(this).css("cursor","pointer")
    })

    $("#client_custom_req2").is("name","client_custom_req2"){
        alert("hi");
    }
});
</script>




