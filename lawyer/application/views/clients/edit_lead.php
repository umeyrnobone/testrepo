<?php 
function echoChecked($current_state, $arr, $row) {
    if(in_array($current_state, explode(', ',$arr->$row) )) { // in_array('KfZ', explode(", ", $checkbox->client_criteria)
        echo 'checked';
    }
}
?>
<style>

.puw-header{
    text-transform: uppercase;
    font-weight: 800;
    padding-top: 10px;
    padding-left: 15px;

}

label{
    color:grey;    
}

.puw{
    box-shadow: 1px 6px 20px grey;
    border-radius:6px;
    padding: 0 15px 15px 15px;
    margin-top: 20px;
    margin-left:-5px;
    margin-right:-5px;

}

.form-group{
    padding-top:7px;

}

.form-control,span{
    border-radius:5px;
}

.save:hover{
    color:white;
    height: 60px;
    width: 60px;
    padding-top: 12px;
    font-size:12px;
    text-decoration: none;
}

.save{
    font-weight: bold;
	position:fixed;
	bottom:56px;
	right: 18px;
	text-decoration: none;
    padding-top: 10px;
    padding-left: 3px;
    text-align: center;
    color: white;
    font-size: 11px;
    letter-spacing: 2px;
    text-transform: uppercase;
    width: 50px;
    height: 50px;
    background-color:rgb(66, 184, 50);
}

.save-icon{
    font-size:15px;
}

.submit-button{
    position:fixed;
    float:right;
    right:20px;
    font-size:15px;
    bottom:5px;

}

</style>


<h2 style="margin-top:30px; padding-bottom:0px;" class="text-center">
			<p>
				<?=$client_info->client_firstname.' '.$client_info->client_lastname;?> 
			</p>
</h2>
<br>
<?php if($this->session->flashdata('errors')): ?>
            <div style="color:black;" class="alert alert-warning">
            <?= $this->session->flashdata('errors'); ?>
            </div>
<?php endif; ?>
<form id="form_lead" action="<?= base_url("Dashboard/set_edit_lead/").my_cryption($client_info->client_id,'e','hco_url') ?>" method="post">
    <div class="col-md-4 col-lg-4">
        <!--PERSÖNLICHE DATEN-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">PERSÖNLICHE DATEN</h5>
                <hr style="width: 95%;">
                <div class="col-md-6">
                    <label>Geschlecht</label>
                </div>
                <div class="col-md-6">
                <label for="male">Herr</label>
                <input id="male" name="client_gender" value="male"  type="radio"<?php if($client_info->client_gender == "male"):?>checked<?php endif; ?>>
                <label for="female">Frau</label>
                <input id="female" name="client_gender" value="female" type="radio"<?php if($client_info->client_gender == "female"):?>checked<?php endif; ?>>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="firstname">Vorname</label> <br>
                    <input class="form-control" type="text" name="client_firstname" id="client_firstname" value="<?= $client_info->client_firstname?>" placeholder="Vorname">
                </div>
                <div class="col-md-6">
                    <label for="lastname">Nachname</label> <br>
                    <input class="form-control input" type="text" name="client_lastname" id="lastname" value="<?= $client_info->client_lastname?>" placeholder="Nachname">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Familienstand</label>
                    <select name="client_family_stand">
                        <option disabled selected>Familienstand...</option>
                        <option value="ledig"       <?php if($client_info->client_family_stand == "ledig"):?>selected<?php endif; ?>>Ledig</option>
                        <option value="verheiratet" <?php if($client_info->client_family_stand == "verheiratet"):?>selected<?php endif; ?>>Verheiratet</option>
                        <option value="geschieden"  <?php if($client_info->client_family_stand == "geschieden"):?>selected<?php endif; ?>>Geschieden</option>
                    </select>
                </div>    
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="birthday">Geburtsdatum</label> <br>
                    <input class="form-control" type="date" id="birthday" name="client_birthdate" value="<?= $client_info->client_birthdate?>" placeholder="z.B 21.01.2001">
                </div>
                <div class="col-md-6">
                    <label for="birthday">Geburtsort</label> <br>
                    <input class="form-control" type="text" id="birthlocation" name="client_birth_city" value="<?= $client_info->client_birth_city?>" placeholder="z.B Deutschland">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="birthday">Nationalität</label> <br>
                    <input class="form-control" type="text" id="nationality" name="client_nationality" value="<?= $client_info->client_nationality  ?>"placeholder="z.B Kurde">
                </div>
                <div class="col-md-6">
                    <label for="birthday">Herkunftsland</label> <br>
                    <input class="form-control" type="text" id="homeland" name="client_homeland" value="<?= $client_info->client_homeland?>" placeholder="z.B Kurdistan">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Berufs-Typ</label><br>
                    <select name="client_job_type">
                        <option disabled selected>Bitte auswählen...</option>
                        <option value="Selbstständig"   <?php if($client_info->client_job_type == "Selbstständig"):?>selected<?php endif; ?>>Selbstständig</option>
                        <option value="Angestellt"      <?php if($client_info->client_job_type == "Angestellt"):?>selected<?php endif; ?>>Angestellt</option>
                        <option value="Student"         <?php if($client_info->client_job_type == "Student"):?>selected<?php endif; ?>>Student</option>
                        <option value="Azubi"           <?php if($client_info->client_job_type == "Azubi"):?>selected<?php endif; ?>>Azubi</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Beruf</label> <br>
                    <select name="client_job">
                        <option disabled selected>Bitte auswählen...</option>
                        <option value="Programmierer"   <?php if($client_info->client_job == "Programmierer"):?>selected<?php endif; ?>>Programmierer</option>
                        <option value="Polizist"        <?php if($client_info->client_job == "Polizist"):?>selected<?php endif; ?>>Polizist</option>
                    </select>
                </div>
            </div>    
        </div>
        <!--PERSÖNLICHE DATEN ENDS-->

        <!--KINDER-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">Kinder</h5>
                <hr style="width: 95%;">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Anzahl der Kinder</label>
                </div>
                <div class="col-md-6">
                    <select name="client_kids">
                        <option selected disabled>Bitte auswählen...</option>
                        <option value="1">abc</option>
                        <option value="2">def</option>
                    </select>
                </div>
            </div>
        </div>
        <!--KINDER ENDS-->

        <!--UPLOAD-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">Bilder / Dokumete Hochladen</h5>
                <hr style="width: 95%;">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="file" name="client_file" id="">
                </div>
            </div>
        </div>
        <!--UPLOAD ENDS-->
    </div>

        
    <div class="col-md-4 col-lg-4">
        <!--KONTAKT DATEN-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">KONTAKT DATEN</h5>
                <hr style="width: 95%;">
                <div class="col-md-12">    
                    <label for="email">Email</label>
                    <input class="form-control" type="text" id="email" name="client_email" value="<?= $client_info->client_email?>" placeholder="z.B test@gmail.com">
                </div>
            </div>    
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="mobil">Mobil</label> <br>
                    <input class="form-control" type="text" id="mobil" name="client_mobile" value="<?= $client_info->client_mobile?>" placeholder="z.B 0178 4344 5035">
                </div> 
                <div class="col-md-6">
                    <label for="festnetz">Festnetz</label> <br>
                    <input class="form-control" type="text" id="festnetz" name="client_phone" value="<?= $client_info->client_phone?>" placeholder="z.B 0176 5793 5035">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="country">Land</label> <br>
                    <input class="form-control" type="text" id="country" name="client_country" value="<?= $client_info->client_country?>" placeholder="z.B Deutschland">
                </div>    
                <div class="col-md-6">
                    <label>Bundesland</label>
                    <select name="client_state">
                        <option disabled selected>Bitte auswählen...</option>
                        <option value="Schleswig-Holstein"      <?php if($client_info->client_state == "Schleswig-Holstein"):?>selected<?php endif; ?>>Schleswig-Holstein</option>
                        <option value="Hamburg"                 <?php if($client_info->client_state == "Hamburg"):?>selected<?php endif; ?>>Hamburg</option>
                        <option value="Bremen"                  <?php if($client_info->client_state == "Bremen"):?>selected<?php endif; ?>>Bremen</option>
                        <option value="Mecklenburg-Vorpommern"  <?php if($client_info->client_state == "Mecklenburg-Vorpommern"):?>selected<?php endif; ?>>Mecklenburg-Vorpommern</option>
                        <option value="Brandenbrug"             <?php if($client_info->client_state == "Brandenbrug"):?>selected<?php endif; ?>>Brandenbrug</option>
                        <option value="Berlin"                  <?php if($client_info->client_state == "Berlin"):?>selected<?php endif; ?>>Berlin</option>
                        <option value="Sachsen-Anhalt"          <?php if($client_info->client_state == "Sachsen-Anhalt"):?>selected<?php endif; ?>>Sachsen-Anhalt</option>
                        <option value="Sachsen"                 <?php if($client_info->client_state == "Sachsen"):?>selected<?php endif; ?>>Sachsen</option>
                        <option value="Thüringen"               <?php if($client_info->client_state == "Thüringen"):?>selected<?php endif; ?>>Thüringen</option>
                        <option value="Niedersachsen"           <?php if($client_info->client_state == "Niedersachsen"):?>selected<?php endif; ?>>Niedersachsen</option>
                        <option value="Nordrhein-Westfalen"     <?php if($client_info->client_state == "Nordrhein-Westfalen"):?>selected<?php endif; ?>>Nordrhein-Westfalen</option>
                        <option value="Hessen"                  <?php if($client_info->client_state == "Hessen"):?>selected<?php endif; ?>>Hessen</option>
                        <option value="Rheinland-Pfalz"         <?php if($client_info->client_state == "Rheinland-Pfalz"):?>selected<?php endif; ?>>Rheinland-Pfalz</option>
                        <option value="Saarland"                <?php if($client_info->client_state == "Saarland"):?>selected<?php endif; ?>>Saarland</option>
                        <option value="Bayern"                  <?php if($client_info->client_state == "Bayern"):?>selected<?php endif; ?>>Bayern</option>
                        <option value="Baden-Württemberg"       <?php if($client_info->client_state == "Baden-Württemberg"):?>selected<?php endif; ?>>Baden-Württemberg</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-5">
                    <label for="plz">Postleitzahl</label> <br>
                    <input class="form-control" type="text" id="plz" name="client_zipcode" value="<?= $client_info->client_zipcode?>" placeholder="z.B 44384">
                </div>
                <div class="col-md-7">
                    <label for="city">Stadt</label> <br>
                    <input class="form-control" type="text" id="city" name="client_city" value="<?= $client_info->client_city?>" placeholder="Dortmund">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="street">Straße</label> <br>
                    <input class="form-control" type="text" id="street" name="client_street" value="<?= $client_info->client_street?>" placeholder="z.B Siemenstraße 4">
                </div>
            </div>
        </div>
        <!--KONTAKT DATEN ENDS-->
        
        <!--EXTRA INFORMATIONEN-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">Extra informationen</h5>
                <hr style="width: 95%;">
                <div class="col-md-6">
                    <label>Kennt uns durch</label>
                    <select name="client_know_us_by">
                        <option selected disabled>Bitte auswählen...</option>
                        <option value="abc" <?php if($client_info->client_know_us_by == "abc"):?>selected<?php endif;?>>abc</option>
                        <option value="def" <?php if($client_info->client_know_us_by == "def"):?>selected<?php endif;?>>def</option>
                    </select>  
                </div> 
                <div class="col-md-6">
                    <label>Oder&nbspindividueller&nbspeintrag</label>
                    <input type="text" class="form-control" placeholder="z.B Türkei Urlaub" name="client_know_us_by_custom" value="<?= $client_info->client_know_us_by_custom?>" id="">
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Generiert am</label>
                    <input type="date" class="form-control" name="client_generated_at" placeholder="20.9.2020" value="<?= $client_info->client_generated_at?>" id="">
                </div> 
                <div class="col-md-6">
                    <label>Empfholen von</label>
                    <select name="client_recommended_by">
                        <option selected disabled>Bitte auswählen...</option>
                        <option value="1"   <?php if($client_info->client_recommended_by == "1"):?>selected<?php endif;?>>1</option>
                        <option value="2"   <?php if($client_info->client_recommended_by == "2"):?>selected<?php endif;?>>2</option>
                    </select>  
                </div> 
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Notizen zum Kunden</label>
                </div>
                <div class="col-md-12">
                <textarea value="<?= $ext_notes->c_notes_external_text?>" id=""></textarea>
                </div>
            </div>
        </div>
        <!--EXTRA INFORMATION ENDS-->
    </div>

    <div class="col-md-4 col-lg-4">
        <!--VORBERATUNG-->
        <div class="puw">
            <div class="form-group row">
                <h5 class="puw-header">VORBERATUNG</h5>
                <hr style="width: 95%;">
                <div class="form-group col-md-12">
                <label>Erfahrungen mit Islamic Finance</label>
                </div>
                <hr style="width: 95%;">
                <div class="col-md-12">
                    <textarea class="client_notes" name="client_experience_islamic_finance"  id="client_notes" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Rechtschule</label>
                    <select name="client_madhab" id="">
                        <option value selected disabled>Bitte auswählen</option>
                        <option value="hanafi"    <?php if($client_info->client_madhab == "hanafi"):?>selected<?php endif;?>>Hanafīya</option>
                        <option value="maliki"    <?php if($client_info->client_madhab == "maliki"):?>selected<?php endif;?>>Mālikiya</option>
                        <option value="schafi"    <?php if($client_info->client_madhab == "schafi"):?>selected<?php endif;?>>Schāfiʿīya</option>
                        <option value="hanbali"   <?php if($client_info->client_madhab == "hanbali"):?>selected<?php endif;?>>Hanbalīya</option>
                        <option value="madhablos" <?php if($client_info->client_madhab == "madhablos"):?>selected<?php endif;?>>Madhablos</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Wer entscheidet?</label>
                    <select name="client_decision_maker">
                        <option value selected disabled>Bitte auswählen</option>
                        <option value="1"   <?php if($client_info->client_decision_maker == "1"):?>selected<?php endif;?>>Eltern</option>
                        <option value="2"   <?php if($client_info->client_decision_maker == "2"):?>selected<?php endif;?>>Chef</option>
                        <option value="3"   <?php if($client_info->client_decision_maker == "3"):?>selected<?php endif;?>>Selber</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Anderer&nbspEntscheidungspartner</label>
                    <select name="client_alt_decision_maker">
                        <option value selected disabled>Bitte auswählen</option>
                        <option value="1"   <?php if($client_info->client_alt_decision_maker == "1"):?>selected<?php endif;?>>abc</option>
                        <option value="2"   <?php if($client_info->client_alt_decision_maker == "2"):?>selected<?php endif;?>>def</option>
                        <option value="3"   <?php if($client_info->client_alt_decision_maker == "3"):?>selected<?php endif;?>>hij</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div style="margin-bottom:20px;" class="col-md-12">
                <label style="margin-bottom:20px;">3 Wünsche an den Halalcheck</label><br>
                <label>1. Wunsch</label>
                <input type="text" name="client_first_wish" value="<?= $client_info->client_first_wish?>" class="form-control">
                </div>
                <div style="margin-bottom:20px;" class="col-md-12">
                    <label>2. Wunsch</label>
                <input type="text" name="client_second_wish" value="<?= $client_info->client_second_wish?>" class="form-control">
                </div>
                <div class="col-md-12">
                    <label>3. Wunsch</label>
                <input type="text" name="client_third_wish" value="<?= $client_info->client_third_wish?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Produktkriterien</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Flexibilität" <?php  echoChecked('Flexibilität', $checkbox, 'client_criteria');?>> <label>Flexibilität</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Transparenz"  <?php  echoChecked('Transparenz', $checkbox, 'client_criteria');?>> <label>Transparenz</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Geld abrufbar"<?php  echoChecked('Geld abrufbar', $checkbox, 'client_criteria');?>> <label>Geld abrufbar</label><br>
                    <input type="checkbox" name="" value=""><input class="form-control" name="client_custom_req" value="<?= $client_info->client_custom_req?>" type="text">
                </div>
                <div class="col-md-6">
                    <label></label><br>
                    <input type="checkbox" name="client_criteria[]" value="Islamkonform"    <?php  echoChecked('Geld abrufbar', $checkbox, 'client_criteria');?>> <label>Islamkonform</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Hohe Renche"     <?php  echoChecked('Hohe Renche', $checkbox, 'client_criteria');?>> <label>Hohe Renche</label><br>
                    <input type="checkbox" name="client_criteria[]" value="Geringe Kosten"  <?php  echoChecked('Geringe Kosten', $checkbox, 'client_criteria');?>> <label>Geringe Kosten</label><br>
                    <input type="checkbox" name="" value=""><input class="form-control" type="text" value="<?= $client_info->client_custom_req2?>" name="client_custom_req2">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Notizen</label>
                    <textarea class="client_notes" name="c_detail_notice" value="" id="client_notes" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Antwort auf Bedingungsfrage</label> 
                    <label>Ja</label> 
                    <input type="radio" value="yes" name="client_req_question" id="yes"   <?php if($client_info->client_req_question == "yes"):?>checked<?php endif;?>> 
                    <label>Nein</label> 
                    <input type="radio" value="no" name="client_req_question" id="no"     <?php if($client_info->client_req_question == "no"):?>checked<?php endif;?>> 
                </div>
                <div class="col-md-12">
                    <label>Bestätigung Bedingungsfrage</label> 
                    </label>Ja</label> 
                    <input type="radio" value="yes" name="client_req_question_confirm" id="yes" <?php if($client_info->client_req_question_confirm == "yes"):?>checked<?php endif;?>> 
                    <label>Nein</label> 
                    <input type="radio" value="no" name="client_req_question_confirm" id="no"   <?php if($client_info->client_req_question_confirm == "no"):?>checked<?php endif;?>> 
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label>Eigenkapital</label> 
                    <input type="text" name="client_capital" value="<?= $client_info->client_capital?>" placeholder="z.B 9389€">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Versicherungen</label><br>
                    <input type="checkbox" name="client_ins[]" value="1"    <?php  echoChecked('1', $checkbox, 'client_insurance_id');?>><label>Kapital-Lebensversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="2"    <?php  echoChecked('2', $checkbox, 'client_insurance_id');?>><label>Risiko-Lebensversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="3"    <?php  echoChecked('3', $checkbox, 'client_insurance_id');?>> <label>Rechtsschutzversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="4"    <?php  echoChecked('4', $checkbox, 'client_insurance_id');?>> <label>Rentenversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="5"    <?php  echoChecked('5', $checkbox, 'client_insurance_id');?>> <label>Hausratversicherung</label><br>
                </div>
                    <div class="col-md-6">
                    <input type="checkbox" name="client_ins[]" value="6"    <?php  echoChecked('6', $checkbox, 'client_insurance_id');?>> <label>Wohngebäudeversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="7"    <?php  echoChecked('7', $checkbox, 'client_insurance_id');?>> <label>Privathaftpflichtversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="8"    <?php  echoChecked('8', $checkbox, 'client_insurance_id');?>> <label>Zahnzusatzversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="9"    <?php  echoChecked('9', $checkbox, 'client_insurance_id');?>> <label>KFZ Haftpflichtversicherung</label><br>
                    <input type="checkbox" name="client_ins[]" value="10"   <?php  echoChecked('10', $checkbox, 'client_insurance_id');?>> <label>Private Krankenversicherung</label><br>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Interesse</label><br>
                    <input type="checkbox" name="client_inter[]" value="1"  <?php  echoChecked('1', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="2"  <?php  echoChecked('2', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="3"  <?php  echoChecked('3', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="4"  <?php  echoChecked('4', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="5"  <?php  echoChecked('5', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                </div>
                <div class="col-md-6">
                    <input type="checkbox" name="client_inter[]" value="6"  <?php  echoChecked('6', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="7"  <?php  echoChecked('7', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="8"  <?php  echoChecked('8', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="9"  <?php  echoChecked('9', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                    <input type="checkbox" name="client_inter[]" value="10"  <?php  echoChecked('10', $checkbox, 'client_interest');?>> <label>Interessen</label><br>
                </div>
            </div>
        </div>
        <!--VORBERATUNG ENDS-->
    </div>  
    <button class="submit-button btn btn-success">Änderung annehmen</button>
</form>

<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
<script>

$(document).ready(function(){
    $("#submit").mouseover(function(){
        $(this).css("cursor","pointer")
    })

});
</script>




