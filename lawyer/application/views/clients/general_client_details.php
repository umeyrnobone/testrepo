^<?php setlocale(LC_MONETARY, 'nl_NL'); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="<?= base_url('Dashboard/show_clients') ?>"><i class="fa fa-home"></i>Kunden Liste</a></li>
				<li><a href="">Kunden liste</a>
			</ol>

		</div><!-- /.breadcrumb-wrapper -->
	</div>
</div>
<section>
	<div class="row">

		<h2 class="page-title text-center">
			<p>
				<?=$client->client_firstname.' '.$client->client_lastname; ?>
			</p>
		</h2>
		<hr>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-6"></i> Personendaten
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body">
					<table class="table table-responsive table-hover">
						<tbody>
						<tr>
							<td>Name</td>
							<td><?=$client->client_firstname.' '.$client->client_lastname; ?></td>
						</tr>

						<?php if($client->client_birthdate): ?>
							<tr>
								<td>Geburtsdatum</td>
								<td><?=  ($client->client_birthdate =='1970-01-01') ? '': date( 'd.m.Y' ,strtotime( $client->client_birthdate)); ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_nationality): ?>
							<tr>
								<td>Nationalität</td>
								<td><?=$client->client_nationality; ?> </td>
							</tr>
						<?php endif; ?>


						<?php if($client->client_job): ?>
							<tr>
								<td>Beruf</td>
								<td><?=$client->client_job; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_email): ?>
							<tr>
								<td>Email</td>
								<td><?=$client->client_email; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_phone): ?>
							<tr>
								<td>Festnetz</td>
								<td><?=$client->client_phone; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_mobile): ?>
							<tr>
								<td>Mobil</td>
								<td><?=$client->client_mobile; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_country): ?>
							<tr>
								<td>Land</td>
								<td><?=$client->client_country; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_state): ?>
							<tr>
								<td>Bundesland</td>
								<td><?=$client->client_state; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_zipcode): ?>
							<tr>
								<td>PLZ</td>
								<td><?=$client->client_zipcode; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_city): ?>
							<tr>
								<td>Stadt</td>
								<td><?=$client->client_city; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client->client_street): ?>
							<tr>
								<td>Straße</td>
								<td><?=$client->client_street; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if(strtotime($client->client_last_contact)): ?>
							<tr>
								<td>Letzter kontakt:</td>
								<td><?=( date('d.m.Y', strtotime($client->client_last_contact)) == "0000-00-00" )? '---------': date('d.m.Y - H:i', strtotime($client->client_last_contact)).' Uhr'; ?> </td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-xs-12 col-sm-6 col-md-4">
			<!-- ------------------------------------------ Case explanation --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-institution m-right-6"></i> Details zum Anliegen
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table">
							<tr>
								<td>Hinzugefügt von</td>
								<td><?= $client->user_firstname.' '.$client->user_lastname ?></td>
							</tr>

							<tr>
								<td>Hinzugefügt am</td>
								<td><?= date('d.m.Y', strtotime($client->lawyer_help_timestamp)) ?></td>
							</tr>

							<tr>
								<td>Rechtsart des Anliegens</td>
								<td><?= $client->l_help_type_name ?></td>
							</tr>
						</table>
					</div>
					<div class="col-md-12">
						<hr>
						<h4 style="font-weight: bold">Erklärung des Anliegens</h4>
						<p>
							<?= $client->lawyer_help_notes ?>
						</p>
					</div>

				</div>

			</div>

			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-institution m-right-6"></i> Dokumente verwalten
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table">
							<form action="<?= base_url('Dashboard/add_lawyer_documents/').my_cryption($client->lawyer_help_id,'e','hco_url').'/'.my_cryption($client->client_id,'e','hco_url') ?>" method="POST" enctype="multipart/form-data">
								<tr>
									<td><h4>Dokumente hochladen</h4></td>
								</tr>
								<tr>
									<td>
										<label for="upload-photo" class="btn btn-primary">Dokumente auswählen</label>
										<input type="file" accept=".doc,.docx,.pdf,application/pdf,.png,.jpeg,.jpg" name="lawyer_docs[]" id="upload-photo" class="pull-right hidden" multiple>
									</td>
									<td style="text-align: right">
										<button class=" btn btn-primary"><i class="fa fa-upload"></i></button>
									</td>
								</tr>

							</form>
							<tr>
								<td><h4>Dokumente</h4></td>
							</tr>
							<?php foreach($documents as $doc): ?>
								<tr>
									<td>
										<a style="font-size: 17px;" href="<?= base_url($doc->client_attach_path) ?>" target="_blank">
											<?php if(strpos($doc->client_attach_path, '.pdf') == true): ?>
												<i class="fa fa-file-pdf"></i>
											<?php elseif(strpos($doc->client_attach_path, '.png') == true || strpos($doc->client_attach_path, '.jpg') == true || strpos($doc->client_attach_path, '.jpeg') == true): ?>
												<i class="fa fa-file-image"></i>
											<?php elseif(strpos($doc->client_attach_path, '.doc') == true || strpos($doc->client_attach_path,'.docx') == true): ?>
												<i class="fa fa-file-word"></i>
											<?php endif; ?>
											<?= str_replace('dokuments/clients/'.$doc->client_attach_client_id.'/lawyer/_'.$client->lawyer_help_id.'/', '', $doc->client_attach_path) ?>
										</a>
									</td>
									<td><a href="#" class="pull-right delete_lawyer_doc" data-toggle="modal" data-target="#delete_attachment" data-path="<?= my_cryption($doc->client_attach_path, 'e', 'hco_url') ?>"><i class="fas fa-trash fa-2x"></i></a></td>
								</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-4">
			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Neue Notizen hinzufügen
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<form action="<?= base_url('Dashboard/add_notes') ?>" method="POST">
					<div class="panel-body">
						<div class="form-group">
							<textarea name="notes"cols="30" rows="10" class="form-control client_notes" required></textarea>
						</div>
						<div class="form-group">
							<input type="hidden" name="type" value="<?= my_cryption($client->l_help_type_name, 'e', 'hco_url') ?>">
							<input type="hidden" id="client_id" name="client_id" value="<?= my_cryption($client->client_id,'e','hco_url') ?>">
							<input type="submit" value="Notizen einfügen" class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>

			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Notizen zum Kunden
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->


				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-12">
							<div class="textareaElement" contenteditable>
								<?php foreach($ext_notes as $note): ?>
									<p class="hc_notes"><?= $note->user_firstname.' '.$note->user_lastname ?></p>
									<p class="hc_notes"><?= date('d.m.Y, H:i ', strtotime($note->c_notes_external_timestamp)) ?>Uhr</p>
									<p><?= $note->c_notes_external_text ?></p>
									<hr>
								<?php endforeach; ?>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="loan_details" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Historie</h5>
			</div>

			<div class="modal-body">
				<div id="loan_history">

				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Schließen</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="show_attachments" role="dialog">
	<div class="modal-dialog" style="width: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Dokumente</h4>
			</div>

			<div class="modal-body">
				<div id="doc_container"></div>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="delete_attachment" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/delete_lawyer_attachment') ?>" method="POST">
				<div class="modal-header">
					<h4>Dokument löschen</h4>
				</div>

				<div class="modal-body">
					Soll das Dokument wirklich gelöscht werden?
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="delete_path" name="path">
						<input type="submit" value="Ja" class="btn btn-primary">
						<button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<style>
	label {
		cursor: pointer;
		/* Style as you please, it will become the visible UI component. */
	}

	.upload-photo {
		opacity: 0;
		position: absolute;
		z-index: -1;
	}
</style>
