<?php setlocale(LC_MONETARY, 'nl_NL'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="<?= base_url('Dashboard/show_clients') ?>"><i class="fa fa-home"></i>Kunden Liste</a></li>
                <li><a href="">Kunden liste</a>
            </ol>

        </div><!-- /.breadcrumb-wrapper -->
    </div>
</div>
<section>
	<div class="row">

		<h2 class="page-title text-center">
			<p>
				<?=$client_info->client_firstname.' '.$client_info->client_lastname;?> 
				<a href="<?= base_url('Dashboard/edit_lead/').my_cryption($client_info->client_id,'e','hco_url') ?>"><span id="edit" class="badge"><i style="font-size: 15px; position: relative; bottom: 3px; left:2px;" class="fa fa-edit"></i></span></a>
			</p>
		</h2>
		<hr>
		<div class="col-xs-12 col-sm-6 col-md-6">
			<!-- ------------------------------------------ PERSONAL INFORMATION --------------------------------------------------- -->
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-address-card m-right-5"></i> Personendaten
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>
							<tr>
							<td>Name</td>
							<td><?=$client_info->client_firstname.' '.$client_info->client_lastname; ?></td>
							</tr>

						<?php if($client_info->client_gender):?>
							<tr>
								<td>Geschlecht</td>
										<?php if($client_info->client_gender == "male"):?>
											<td>Männlich <i class="fas fa-mars"></i></td>
										<?php else:?>
											<td>Weiblich <i class="fas fa-venus"></i></td>
										<?php endif;?>
								</td>
							</tr>
						<?php endif;?>

						<?php if($client_info->client_birthdate): ?>
							<tr>
								<td>Geburtsdatum</td>
								<td><?= date('d.m.Y',strtotime($client_info->client_birthdate)) ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_birth_city): ?>
							<tr>
								<td>Geburtsort</td>
								<td><?=$client_info->client_birth_city; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_family_stand): ?>
							<tr>
								<td>Familienstand</td>
								<td><?=  ($client_info->client_family_stand)?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_homeland): ?>
							<tr>
								<td>Herkunft</td>
								<td><?=$client_info->client_homeland; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_nationality): ?>
							<tr>
								<td>Nationalität</td>
								<td><?=$client_info->client_nationality; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_job_type): ?>
							<tr>
								<td>Berufs-Typ</td>
								<td><?=$client_info->client_job_type; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_job): ?>
							<tr>
								<td>Beruf</td>
								<td><?=$client_info->client_job; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_email): ?>
							<tr>
								<td>Email</td>
								<td><?=$client_info->client_email; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_phone): ?>
							<tr>
								<td>Festnetz</td>
								<td><?=$client_info->client_phone; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_mobile): ?>
							<tr>
								<td>Mobil</td>
								<td><?=$client_info->client_mobile; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_country): ?>
							<tr>
								<td>Land</td>
								<td><?=$client_info->client_country; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_state): ?>
							<tr>
								<td>Bundesland</td>
								<td><?=$client_info->client_state; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_zipcode): ?>
							<tr>
								<td>PLZ</td>
								<td><?=$client_info->client_zipcode; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_city): ?>
							<tr>
								<td>Stadt</td>
								<td><?=$client_info->client_city; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_street): ?>
							<tr>
								<td>Straße</td>
								<td><?=$client_info->client_street; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if(strtotime($client_info->client_last_contact)): ?>
							<tr>
								<td>Letzter kontakt:</td>
								<td><?=( date('d.m.Y', strtotime($client_info->client_last_contact)) == "0000-00-00" )? '---------': date('d.m.Y - H:i', strtotime($client_info->client_last_contact)).' Uhr'; ?> </td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>

			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-link m-right-6"></i> Kontakt
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>
							<?php if($client_info->client_know_us_by):?>
								<tr>
									<td>Kennt uns durch</td>
									<td><?= $client_info->client_know_us_by. " " .$client_info->client_know_us_by_custom?></td>
								</tr>
							<?php endif;?>

							<?php if($client_info->client_generated_at):?>
								<tr>
									<td>Generiert am</td>
									<td><?= date('d.m.Y',strtotime($client_info->client_generated_at))?></td>
								</tr>
							<?php endif;?>

							<?php if($client_info->client_recommended_by):?>
								<tr>
									<td>Empfholen von</td>
									<td><?= $client_info->client_recommended_by?></td>
								</tr>
							<?php endif;?>
						</tbody>
					</table>
				</div>
			</div>
						<!-- ------------------------------------------ NEUE NOTIZ HINZUFÜGEN ------------------------------------------------------>
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="fa fa-cog m-right-5"></i> Neue Notizen hinzufügen
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<form action="<?= base_url('Dashboard/add_notes') ?>" method="POST">
					<div class="panel-body">
						<div class="form-group">
							<textarea name="notes"cols="30" rows="10" class="form-control client_notes" required></textarea>
						</div>
						<div class="form-group">
							<input type="hidden" id="client_id" name="client_id" value="<?= my_cryption($client_info->client_id, 'e', 'hco_url') ?>">
							<input type="submit" value="Notizen einfügen" class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-6">
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-people-arrows m-right-5"></i> Beratung
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>
						<?php if($client_info->client_madhab):?>
							<tr>
							<td>Rechtschule</td>
							<td><?=$client_info->client_madhab?></td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_decision_maker):?>
							<tr>
							<td>Wer entscheidet?</td>
							<td><?=$client_info->client_decision_maker?></td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_alt_decision_maker): ?>
							<tr>
								<td>Anderer Entscheidungspartner</td>
								<td><?= $client_info->client_alt_decision_maker?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_first_wish): ?>
							<tr>
								<td>Wunsch 1</td>
								<td><?=$client_info->client_first_wish; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_second_wish): ?>
							<tr>
								<td>Wunsch 2</td>
								<td><?=  ($client_info->client_second_wish)?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_third_wish): ?>
							<tr>
								<td>Wunsch 3</td>
								<td><?=$client_info->client_third_wish; ?> </td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_nationality): ?>
							<tr>
								<td>Nationalität</td>
								<td><?=$client_info->client_nationality; ?> </td>
							</tr>
						<?php endif; ?>
						</tbody>
					</table>
				</div>
    		</div>
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-exclamation m-right-5"></i>Produktkriterien
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>
						<?php if($client_info->client_criteria):?>
							<tr>
							<td>Kriterien</td>
							<td><?=$client_info->client_criteria?></td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_custom_req):?>
							<tr>
							<td>1. Individuelle Kriterien</td>
							<td><?=$client_info->client_custom_req?></td>
							</tr>
						<?php endif; ?>

						<?php if($client_info->client_custom_req2): ?>
							<tr>
								<td>2. Individuelle Kriterien</td>
								<td><?= $client_info->client_custom_req2?> </td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-scroll m-right-5"></i>Versicherungen
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>

						<?php if($client_info->client_insurance_id):?>
							<tr>
							<td>Versicherungen</td> 
						<td><?php foreach($insurance as $i):?><?= $i->insurance_text.'<br>'?><?php endforeach;?></td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-clipboard-list m-right-5"></i>Interessen
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>

						<?php if($client_info->client_interest):?>
							<tr>
							<td>Interessen</td>
							<td><?=$client_info->client_interest?></td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>			
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-coins m-right-5"></i>Eigenkapital
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>

						<?php if($client_info->client_capital):?>
							<tr>
							<td>Kapital</td>
							<td><?=$client_info->client_capital?></td>
							</tr>
						<?php endif; ?>

						</tbody>
					</table>
				</div>
			</div>
			<div class="panel panel-default-light border-default">
				<div class="panel-heading">
					<div class="panel-title">
					<i class="fas fa-quote-left m-right-5"></i> Notizen: Erfahrung mit Islamic Finance
					</div>
					<!-- /.panel-title -->
					<div class="panel-tools panel-action">
						<button class="btn btn-min"></button>
						<button class="btn btn-expand"></button>
					</div>
					<!-- /.panel-tools -->
				</div>
				<!-- /.panel-heading -->

				<div class="panel-body table-responsive">
					<table class="table table-hover">
						<tbody>
							<?php if($client_info->client_experience_islamic_finance):?>
							<tr>
							<td><?=$client_info->client_experience_islamic_finance?>	</td>
							</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>

			<!-- ------------------------------------------ END INFORMATION --------------------------------------------------- -->
		</div>
	</div>
</section>


<div class="modal fade" id="loan_history" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Historie</h5>
			</div>

			<div class="modal-body">
				<div id="loan_details">

				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Schließen</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="show_attachments" role="dialog">
	<div class="modal-dialog" style="width: 500px;">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Dokumente</h4>
			</div>

			<div class="modal-body">
				<div id="doc_container"></div>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="delete_attachment" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/delete_lawyer_attachment') ?>" method="POST">
				<div class="modal-header">
					<h4>Dokument löschen</h4>
				</div>

				<div class="modal-body">
					Soll das Dokument wirklich gelöscht werden?
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="delete_path" name="path">
						<input type="submit" value="Ja" class="btn btn-primary">
						<button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
					</div>
				</div>
			</form>
		</div>
	</div>



<style>
    label {
        cursor: pointer;
        /* Style as you please, it will become the visible UI component. */
    }

    .upload-photo {
        opacity: 0;
        position: absolute;
        z-index: -1;
    }
</style>
