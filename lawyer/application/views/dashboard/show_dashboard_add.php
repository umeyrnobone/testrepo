      <div class="row">
        <div class="col-lg-6">
          <div class="panel panel-default-light panel-chart border-default">
            <div class="panel-heading">
              <div class="panel-title">
                <i class="fa fa-bar-chart m-right-5"></i> Orders: Daily - $700
              </div><!-- /.panel-title -->
              <div class="panel-tools panel-action">
                <button class="btn btn-close"></button>
                <button class="btn btn-min"></button>
                <button class="btn btn-expand"></button>
              </div><!-- /.panel-tools -->
            </div><!-- /.panel-heading -->

            <div class="panel-body">
              <h4 class="chart-heading">Total Orders: <span class="label label-success">$450,520</span></h4>
              <div class="chart-body" id="chart-orders"></div>
            </div><!-- /.panel-body -->

            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-3 col-item">
                  <small>TODAY</small>
                  <span>$700</span>
                </div><!-- /.col-xs-3 -->

                <div class="col-xs-3 col-item">
                  <small>1 MONTH AGO</small>
                  <span>$25,000</span>
                </div><!-- /.col-xs-3 -->

                <div class="col-xs-3 col-item">
                  <small>3 MONTHS AGO</small>
                  <span>$70,000</span>
                </div><!-- /.col-xs-3 -->

                <div class="col-xs-3 col-item">
                  <small>6 MONTHS AGO</small>
                  <span>$150,000</span>
                </div><!-- /.col-xs-3 -->
              </div><!-- /.row -->
            </div><!-- /.panel-footer -->
          </div><!--/.panel-->
        </div><!-- /.col-lg-6 -->

        <div class="col-lg-6">
          <div class="panel panel-default-light panel-chart border-default">
            <div class="panel-heading">
              <div class="panel-title">
                <i class="fa fa-line-chart m-right-5"></i> Expenses: Daily - $200
              </div><!-- /.panel-title -->
              <div class="panel-tools panel-action">
                <button class="btn btn-close"></button>
                <button class="btn btn-min"></button>
                <button class="btn btn-expand"></button>
              </div><!-- /.panel-tools -->
            </div><!-- /.panel-heading -->

            <div class="panel-body">
              <h4 class="chart-heading">Total Expenses: <span class="label label-primary">$1,050,520</span></h4>
              <div class="chart-body" id="chart-expenses"></div>
            </div><!-- /.panel-body -->

            <div class="panel-footer">
              <div class="row">
                <div class="col-xs-3 col-item">
                  <small>TODAY</small>
                  <span>$700</span>
                </div><!-- /.col-xs-3 -->

                <div class="col-xs-3 col-item">
                  <small>1 MONTH AGO</small>
                  <span>$25,000</span>
                </div><!-- /.col-xs-3 -->

                <div class="col-xs-3 col-item">
                  <small>3 MONTHS AGO</small>
                  <span>$70,000</span>
                </div><!-- /.col-xs-3 -->

                <div class="col-xs-3 col-item">
                  <small>6 MONTHS AGO</small>
                  <span>$150,000</span>
                </div><!-- /.col-xs-3 -->
              </div><!-- /.row -->
            </div><!-- /.panel-footer -->
          </div><!--/.panel-->
        </div><!-- /.col-lg-6 -->
      </div><!-- /.row -->