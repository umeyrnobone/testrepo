<?php

function checkStatus($status, $current_status){
	if($status == $current_status){
		return 'selected';
	} else{
		return '';
	}
}

function statusColor($status){
	$status_array = array(
		3		=> '#fffaa1',
		6		=> '#ed6f6f',
		4		=> '#82bfec',
		1		=> '#D3D3D3',
		2		=> '#D3D3D3',
		5		=> '#ed6f6f'
	);

	return $status_array[$status];
}

?>
<section class="" style="position: relative">
	<div class="tab-content">
		<div class="tab-pane tab-pane-dashboard active fade in"  id="dashboard" style="margin-bottom: 80px">

			<div class="page-title-wrapper">
			</div><!-- /.page-titile-wrapper -->

			<div class="trip_detail_site">
				<div class="col-md-4 col-sm-12">
					<div class="panel user-all">
                        <a href="<?= base_url('Dashboard/show_clients') ?>">
                            <h2>Alle Kunden</h2>
                            <span> <?=count($clients)?> </span>
                        </a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="col panel user-new">
						<a href="">
							<h2>Neue Kunden</h2>
							<span> <?=count($new_clients)?> </span>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="panel user-done">
						<a href="">
							<h2>Erfolge</h2>
							<span> 0</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="padding-top: 120px">
				<h2 class="text-center">Neusten Kunden</h2>

		</div>

		<div class="col-md-12">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default-light border-default">
						<div class="panel-heading">
							<div class="panel-title pull-left">
								<i class="fa fa-table m-right-5"></i> Kunden mit Krediten
							</div><!-- /.panel-title -->
							<div class="panel-tools">
								<div class="tools-content">
									<!-- Though the light panel structure does not have div.tools-content (only default panel has), we put it here for the Datatables input that is appended in here via jQuery universal script dynamically -->
									<!-- NOTE: THERE GOES SEARCH INPUT WHICH WE ARE APPENDING HERE VIA JQUERY -->
								</div>
							</div><!-- /.panel-tools -->
						</div><!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">

								<div class="table-responsive">

									<table class="table table-striped table-bordered datatable-buttons">
										<thead>
										<tr>
											<th>Kunde</th>
											<th>Rufnummer</th>
                                            <th>E-Mail</th>
											<th>Kredit</th>
											<th>Gesamtsumme</th>
											<th>Offene Summe</th>
											<th>Beginn</th>
											<th>Ende</th>
                                            <th>Rechtschutz vorhanden</th>
											<th>Status</th>
											<th></th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($new_clients as $loan): ?>
                                            <?php $insurances = explode(', ', $loan->client_insurance_id); ?>
											<tr>
												<td><a href="<?= base_url('Dashboard/show_client_details/').my_cryption($loan->client_id,'e','hco_url') ?>" target="_blank"><?= $loan->client_firstname.' '.$loan->client_lastname ?></a></td>
												<td><?= $loan->client_mobile != '' ? $loan->client_mobile : $loan->client_phone ?></td>
                                                <td><?= $loan->client_email ?></td>
												<td><?= $loan->client_loan_name ?></td>
												<td><?= @money_format('%!n', $loan->client_loan_amount) ?>€</td>
												<td><?= @money_format('%!n', $loan->client_loan_open_sum) ?>€</td>
												<td><span class="hidden"><?= strtotime($loan->client_loan_begin) ?></span><?= date('d.m.Y', strtotime($loan->client_loan_begin)) ?></td>
												<td><span class="hidden"><?= strtotime($loan->client_loan_end) ?></span><?= date('d.m.Y', strtotime($loan->client_loan_end)) ?></td>
                                                <td><?php if(in_array(3, $insurances) == true){echo 'Vorhanden';} else{echo '-';} ?></td>
												<td style="width: 150px;">
														<span data-toggle="modal" data-id="<?= $loan->client_loan_id ?>" data-status="<?= $loan->c_loan_status_name ?>" data-target="#change_status_modal" class="label block car_status_changer" style="cursor: pointer; background: <?= statusColor($loan->client_loan_status) ?>">
															<?= $loan->c_loan_status_name ?>
														</span>
												</td>
												<td>
													<a href="#" class="btn btn-primary show_loan_history" data-id="<?= my_cryption($loan->client_loan_id,'e','hco_url') ?>" data-toggle="modal" data-target="#loan_notes">Statusänderungen anzeigen</a>
													<a href="#" class="btn btn-primary add_loan_notes" data-id="<?= my_cryption($loan->client_loan_client_id,'e','hco_url') ?>" data-toggle="modal" data-target="#add_notes">Notizen einfügen</a>
												</td>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div> <!-- /.table-responisive -->
							</div> <!-- /.table-responisive -->
						</div><!-- /.panel-body -->
					</div><!--/.panel-->
				</div><!-- /.col-sm-12 -->
			</div>
		</div><!-- /.tab-pane -->
	</div><!-- /.tab-content -->
</section>

<div class="modal fade" id="change_status_modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/update_client_loan_status') ?>" method="POST">
				<div class="modal-header">
					<h4>Status ändern</h4>
				</div>

				<div class="modal-body">
					<table class="table">
						<tr>
							<td>Aktueller Status</td>
							<td><span id="current_status_span"></span></td>
						</tr>

						<tr>
							<td>Neuer Status</td>
							<td>
								<select name="loan_status" required>
									<option value="">Status auswählen</option>
									<?php foreach($status as $status): ?>
										<option value="<?= $status->c_loan_status_id ?>"><?= $status->c_loan_status_name ?></option>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>

						<tr>
							<td>Notizen</td>
							<td>
								<textarea name="update_notes" cols="30" rows="5" class="form-control" required></textarea>
							</td>
						</tr>
					</table>
				</div>

				<div class="modal-footer">
					<div class="pull-left">
						<input type="hidden" id="meta_id" name="loan_id">

						<input type="submit" class="btn btn-primary" value="Status ändern">
						<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="loan_notes" role="dialog">
	<div class="modal-dialog" style="width: 950px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Details</h5>
			</div>

			<div class="modal-body">
				<div id="loan_details">

				</div>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_notes" role="dialog">
	<div class="modal-dialog" style="width: 450px; height: 100px;">
		<div class="modal-content">
			<form action="<?= base_url('Dashboard/add_notes') ?>" method="POST">
			<div class="modal-header">
				<h5>Notizen einfügen</h5>
			</div>

			<div class="modal-body">
				<h4>Notizen zum Kunden einfügen</h4>
				<textarea name="notes"cols="30" rows="5" class="form-control client_notes"></textarea>
			</div>

			<div class="modal-footer">
				<div class="pull-left">
					<input type="hidden" id="client_id" name="client_id">
					<input type="submit" value="Einfügen" class="btn btn-primary">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
				</div>
			</div>
		</div>
	</div>
</div>
