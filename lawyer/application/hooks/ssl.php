<?php
/**
 * Created by PhpStorm.
 * User: Amar Moufti
 * Date: 29.05.2018
 * Time: 03:16
 */

function force_ssl() {

    $CI =& get_instance();
    $CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
    if ($_SERVER['SERVER_PORT'] != 443) redirect($CI->uri->uri_string());
}