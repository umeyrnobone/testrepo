<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
        $this->load->helper('crm_helper');
        $this->load->model('Client_model');
	}


    public function index() {
		$new_date = new DateTime();
		date_sub($new_date, date_interval_create_from_date_string("1 week"));

		$data['clients'] 		= $this->Client_model->get_loans_with_clients();
		$data['new_clients'] 	= $this->Client_model->get_new_clients_with_loans($new_date);
		$data['status']			= $this->Client_model->get_loan_status_list();

        $this->template['content']  = $this->load->view('dashboard/show_dashboard', $data, TRUE);
		$this->load->view('templates/main', $this->template);

	}

	public function new_lead(){

		$data['clients']			= $this->Client_model->get_loan_status_list();

        $this->template['content']  = $this->load->view('clients/new_lead',$data, TRUE);
		$this->load->view('templates/main', $this->template);
	}

	public function set_new_lead(){

		$gender = $this->form_validation->set_rules('client_gender', 'Geschlecht', 'trim|required');
		$firstname = $this->form_validation->set_rules('client_firstname', 'Vorname', 'trim|required');
		$lastname = $this->form_validation->set_rules('client_lastname', 'Nachname', 'trim|required|min_length[2]|max_length[12]');

		if ($this->form_validation->run() == FALSE) {


			$data = array(
				'errors' => validation_errors()
			);

			$this->session->set_flashdata($data);

			redirect("Dashboard/new_lead");
		}else{

		//Checkboxen
		$ins = $this->input->post("client_ins");
		$criteria = $this->input->post("client_criteria");
		$inter = $this->input->post("client_inter");
		//Checkboxen

		$data = array(
			"client_gender" 					=> $this->input->post("client_gender"),
			"client_firstname" 					=> $this->input->post("client_firstname"),
			"client_lastname" 					=> $this->input->post("client_lastname"),
			"client_family_stand" 				=> $this->input->post("client_family_stand"),
			"client_birthdate" 					=> $this->input->post("client_birthdate"),
			"client_birth_city" 				=> $this->input->post("client_birth_city"),
			"client_nationality" 				=> $this->input->post("client_nationality"),
			"client_homeland" 					=> $this->input->post("client_homeland"),
			"client_job_type" 					=> $this->input->post("client_job_type"),
			"client_job" 						=> $this->input->post("client_job"),
			"client_email" 						=> $this->input->post("client_email"),
			"client_mobile" 					=> $this->input->post("client_mobile"),
			"client_phone" 						=> $this->input->post("client_phone"),
			"client_country" 					=> $this->input->post("client_country"),
			"client_state" 						=> $this->input->post("client_state"),
			"client_zipcode" 					=> $this->input->post("client_zipcode"),
			"client_city" 						=> $this->input->post("client_city"),
			"client_street" 					=> $this->input->post("client_street"),
			//"client_file"						=> $this->input->post("client_file"),
			"client_know_us_by"					=> $this->input->post("client_know_us_by"),
			"client_know_us_by_custom"			=> $this->input->post("client_know_us_by_custom"),
			"client_generated_at" 				=> $this->input->post("client_generated_at"),
			"client_recommended_by" 			=> $this->input->post("client_recommended_by"),
			"client_experience_islamic_finance" => $this->input->post("client_experience_islamic_finance"),
			"client_madhab"						=> $this->input->post("client_madhab"),
			"client_decision_maker"				=> $this->input->post("client_decision_maker"),
			"client_alt_decision_maker"			=> $this->input->post("client_alt_decision_maker"),
			"client_first_wish"					=> $this->input->post("client_first_wish"),
			"client_second_wish"				=> $this->input->post("client_second_wish"),
			"client_third_wish"					=> $this->input->post("client_third_wish"),
			"client_capital"					=> $this->input->post("client_capital"),
			"client_criteria"					=> $criteria = implode(", ",$criteria),
			"client_custom_req"					=> $this->input->post("client_custom_req"),
			"client_custom_req2"				=> $this->input->post("client_custom_req2"),
			"client_interest"					=> $inter = implode(", ",$inter),
			"client_insurance_id"				=> $ins = implode(", ",$ins),
			"client_req_question_confirm"		=> $this->input->post("client_req_question_confirm"),
			"client_req_question"				=> $this->input->post("client_req_question"),
			"client_mvv_notes"					=> $this->input->post("client_mvv_notes")
		);
		

		if(!empty($data)){
		$this->Client_model->insert_new_lead($data);
		};


		$notice =array(
				"c_notes_external_text" 		=> $this->input->post("client_notice"),
				"c_notes_external_client_id" 	=> $this->db->insert_id()
		);

		if(!empty($notice)){
			$this->Client_model->insert_external_notice($notice);
			};

		}
	}

	public function edit_lead($client_id){
		$client_id = my_cryption($client_id, 'd', 'hco_url');

		$data['client_info']		= $this->Client_model->get_client_by_id($client_id);
		$data['ext_notes']	 		= $this->Client_model->get_client_external_notes($client_id);
		$data['checkbox']			= $this->Client_model->get_client_checkbox($client_id);

		
		$this->template["content"] = $this->load->view("clients/edit_lead",$data,TRUE);
		$this->load->view("templates/main",$this->template);
	}

	public function set_edit_lead($client_id){
		$client_id = my_cryption($client_id, 'd', 'hco_url');

		//Form Validation
		$gender = $this->form_validation->set_rules('client_gender', 'Geschlecht', 'trim|required');
		$firstname = $this->form_validation->set_rules('client_firstname', 'Vorname', 'trim|required');
		$lastname = $this->form_validation->set_rules('client_lastname', 'Nachname', 'trim|required|min_length[2]|max_length[12]');

		if ($this->form_validation->run() == FALSE) {
			$data = array(
				'errors' => validation_errors()
			);

			$this->session->set_flashdata($data);

			redirect("Dashboard/edit_lead/$client_id");
		}else{

		//Checkboxen
		$ins 		= $this->input->post("client_ins");
		$criteria 	= $this->input->post("client_criteria");
		$inter	 	= $this->input->post("client_inter");
		//Checkboxen

		$data = array(
			"client_gender" 					=> $this->input->post("client_gender"),
			"client_firstname" 					=> $this->input->post("client_firstname"),
			"client_lastname" 					=> $this->input->post("client_lastname"),
			"client_family_stand" 				=> $this->input->post("client_family_stand"),
			"client_birthdate" 					=> $this->input->post("client_birthdate"),
			"client_birth_city" 				=> $this->input->post("client_birth_city"),
			"client_nationality" 				=> $this->input->post("client_nationality"),
			"client_homeland" 					=> $this->input->post("client_homeland"),
			"client_job_type" 					=> $this->input->post("client_job_type"),
			"client_job" 						=> $this->input->post("client_job"),
			"client_email" 						=> $this->input->post("client_email"),
			"client_mobile" 					=> $this->input->post("client_mobile"),
			"client_phone" 						=> $this->input->post("client_phone"),
			"client_country" 					=> $this->input->post("client_country"),
			"client_state" 						=> $this->input->post("client_state"),
			"client_zipcode" 					=> $this->input->post("client_zipcode"),
			"client_city" 						=> $this->input->post("client_city"),
			"client_street" 					=> $this->input->post("client_street"),
			//"client_file"						=> $this->input->post("client_file"),
			"client_know_us_by"					=> $this->input->post("client_know_us_by"),
			"client_know_us_by_custom"			=> $this->input->post("client_know_us_by_custom"),
			"client_generated_at" 				=> $this->input->post("client_generated_at"),
			"client_recommended_by" 			=> $this->input->post("client_recommended_by"),
			"client_experience_islamic_finance" => $this->input->post("client_experience_islamic_finance"),
			"client_madhab"						=> $this->input->post("client_madhab"),
			"client_decision_maker"				=> $this->input->post("client_decision_maker"),
			"client_alt_decision_maker"			=> $this->input->post("client_alt_decision_maker"),
			"client_first_wish"					=> $this->input->post("client_first_wish"),
			"client_second_wish"				=> $this->input->post("client_second_wish"),
			"client_third_wish"					=> $this->input->post("client_third_wish"),
			"client_capital"					=> $this->input->post("client_capital"),
			"client_criteria"					=> $criteria = implode(", ",$criteria),
			"client_custom_req"					=> $this->input->post("client_custom_req"),
			"client_custom_req2"				=> $this->input->post("client_custom_req2"),
			"client_interest"					=> $inter = implode(", ",$inter),
			"client_insurance_id"				=> $ins = implode(", ",$ins),
			"client_req_question_confirm"		=> $this->input->post("client_req_question_confirm"),
			"client_req_question"				=> $this->input->post("client_req_question"),
			"client_mvv_notes"					=> $this->input->post("client_mvv_notes")

		);

			$this->Client_model->edit_lead($data,$client_id);	

			/*$notice =array(
					"c_notes_external_text" => $this->input->post("client_notice"),
					"c_notes_external_client_id" => $client_id
				) ;

			if(!empty($notice)){
				$this->Client_model->insert_external_notice($notice);
				};*/

		redirect("Dashboard");
	
		}


	}


	public function show_clients() {

		$data['clients'] 		= $this->Client_model->get_loans_with_clients();

        $this->template['content']  = $this->load->view('clients/clients_list', $data, TRUE);
		$this->load->view('templates/main', $this->template);
    }

	public function show_client_details($client_id){
		$client_id = my_cryption($client_id, 'd', 'hco_url');

		$data['client_info'] 	= $this->Client_model->get_client_by_id($client_id);
		$data['loans']		 	= $this->Client_model->get_client_loans($client_id);
		$data['ext_notes']	 	= $this->Client_model->get_client_external_notes($client_id);
		$data['documents'] 		= $this->Client_model->get_client_attachments($client_id);
		$data['insurance']		= $this->Client_model->get_client_insurances();
		
			$this->template['content']  = $this->load->view('clients/client_details', $data, TRUE);
			$this->load->view('templates/main', $this->template);

	}

	public function show_case_details($case_id){
		$case_id = my_cryption($case_id, 'd', 'hco_url');

		$client_data = $data['client'] 	=  $this->Client_model->get_case_by_id($case_id);

		$data['ext_notes']	= $this->Client_model->get_client_external_notes_by_type($client_data->client_id, 'lawyer');
		$data['documents']	= $this->Client_model->get_client_attachments($client_data->client_id, 'Anwalt');

		if(!empty($data['client'])){
			$this->template['content']  = $this->load->view('clients/general_client_details', $data, TRUE);
			$this->load->view('templates/main', $this->template);
		} else{
			redirect('Dashboard');
		}
	}

	public function update_client_loan_status(){
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$loan_id 		= $this->input->post('loan_id');
			$notes 			= $this->input->post('update_notes');
			$status			= $this->input->post('loan_status');
			$update_date	= new DateTime();
			$loan_status = $this->Client_model->get_loan_status_by_id($status)->c_loan_status_name;

			$designed_notes ='<tr class="loan_tr">
								<td class="loan_td">
									<h5 class="loan_author notes_author_data">'.$this->session->userdata("user_fname_session").' '.$this->session->userdata("user_lname_session").'</h5>
									<h6 class="loan_status notes_author_data">Status: <b>'.$loan_status.'</b>, am '.$update_date->format('d.m.Y, H:i').' Uhr <i class="fas fa-clock"></i></h6>
									<p class="loan_notes">'.$notes.'</p>
								</td>
							</tr>';

			$this->db->trans_begin();

			$update_data = array(
				'client_loan_status'	=> $status,
			);
			$this->Client_model->update_loan_by_id($loan_id, $update_data);

			$insert_data = array(
				'loan_history_message' 			=> '<b>'.$loan_status.'</b>',
				'loan_history_included_loan' 	=> $loan_id,
				'loan_history_notes'			=> $designed_notes,
				'loan_history_notes_blank'		=> $notes,
				'loan_history_status'			=> $status,
				'loan_history_added_by'			=> $this->session->userdata('user_id_session')
			);
			$this->Client_model->insert_client_loan_history($insert_data);

			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_notes(){
		$client_id 	= $this->input->post('client_id');
		$client_id  = my_cryption($client_id, 'd', 'hco_url');
		$notes 		= $this->input->post('notes');
		$type		= $this->input->post('type');

		if($type != ''){
			$type = my_cryption($type, 'd', 'hco_url');
		} else{
			$type = 'Loan';
		}

		$insert_data = array(
			'c_notes_external_text' 		=> $notes,
			'c_notes_external_client_id'	=> $client_id,
			'c_notes_external_added_by'		=> $this->session->userdata('user_id_session'),
			'c_notes_external_reason'		=> $type
		);
		$this->Client_model->insert_external_notes($insert_data);

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_loan_documents($loan_id, $client_id){
		if (isset($_FILES['loan_documents']) && $_FILES['loan_documents']['name'][0] != '') {
			$count_images = $_FILES['loan_documents']['name'];
			if (isset($count_images)) {

				$length = count($count_images);
				$path = 'dokuments/clients/' . $client_id . '/loans/_'.$loan_id;
				if (!is_dir($path)) {
					mkdir($path, 0777, true);
				}

				for ($i = 0; $i < $length; $i++) {
					$name = str_replace(array(' ', 'ö', 'ü', 'ä', '.', '/', '-', '<', '>', '?', '!', pathinfo($_FILES['loan_documents']['name'][$i], PATHINFO_EXTENSION)), '', $_FILES['loan_documents']['name'][$i]).'.'.pathinfo($_FILES['loan_documents']['name'][$i], PATHINFO_EXTENSION);

					if (is_file($path . '/' . $name)) {
						unlink($path . '/' . $name);
						$this->Client_model->delete_double_attachs($path.'/'.$name);
					}
					$_FILES['file']['name'] = $name;
					$_FILES['file']['type'] = $_FILES['loan_documents']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['loan_documents']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['loan_documents']['error'][$i];
					$_FILES['file']['size'] = $_FILES['loan_documents']['size'][$i];
					$config['remove_spaces'] = true;
					$config['upload_path'] = $path;
					$config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
					$config['max_size'] = '7800000';
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('file')) {
						/*$upload_error = array('error' => $this->upload->display_errors());
						var_dump($upload_error);
						die(); */
					} else {

						$insert_data = array('client_attach_client_id' => $client_id, 'c_attach_type' => 'Kredit', 'client_attach_path' => $path.'/'.$name);
						$this->Client_model->insert_loan_attachment($insert_data);
					}
				}
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

    public function show_job_lost(){
		$data['type']		= 'Arbeitsrecht / Kündigung';
		$data['clients'] 	= $this->Client_model->get_lawyer_clients_by_types(array(1));

        $this->template['content']  = $this->load->view('clients/general_clients_list', $data, TRUE);
        $this->load->view('templates/main', $this->template);
    }

	public function general_questions(){
		$data['type']		= 'Rechtliches allgemein';
		$data['clients'] 	= $this->Client_model->get_lawyer_clients_by_types(array(2));
		$data['status']  	= $this->Client_model->get_lawyer_status();

		$this->template['content']  = $this->load->view('clients/general_clients_list', $data, TRUE);
		$this->load->view('templates/main', $this->template);
	}

	public function update_case_status(){

		if($this->input->server('REQUEST_METHOD') == 'POST'){
			$case_id 		= $this->input->post('case_id');
			$notes 			= $this->input->post('update_notes');
			$status			= $this->input->post('case_status');
			$update_date	= new DateTime();
			$loan_status 	= $this->Client_model->get_loan_status_by_id($status)->c_loan_status_name;

			$this->db->trans_begin();

			$update_data = array(
				'lawyer_help_status' => $status,
			);
			$this->Client_model->update_case_by_id($case_id, $update_data);

			$insert_data = array(
				'l_help_history_case_id' 	=> $case_id,
				'l_help_history_notes'		=> $notes,
				'l_help_history_status'		=> $status,
				'l_help_history_added_by'	=> $this->session->userdata('user_id_session')
			);
			$this->Client_model->insert_lawyer_data('lawyer_help_history', $insert_data);

			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function add_lawyer_documents($case_id, $client_id){
		$case_id = my_cryption($case_id, 'd','hco_url');
		$client_id = my_cryption($client_id, 'd','hco_url');

		if (isset($_FILES['lawyer_docs']) && $_FILES['lawyer_docs']['name'][0] != '') {
			$count_images = $_FILES['lawyer_docs']['name'];
			if (isset($count_images)) {

				$length = count($count_images);
				$path = 'dokuments/clients/' . $client_id . '/lawyer/_'.$case_id;

				if (!is_dir($path)) {
					mkdir($path, 0777, true);
				}

				for ($i = 0; $i < $length; $i++) {
					$name = str_replace(array(' ', 'ö', 'ü', 'ä', '.', '/', '-', '<', '>', '?', '!', pathinfo($_FILES['lawyer_docs']['name'][$i], PATHINFO_EXTENSION)), '', $_FILES['lawyer_docs']['name'][$i]).'.'.pathinfo($_FILES['lawyer_docs']['name'][$i], PATHINFO_EXTENSION);

					if (is_file($path . '/' . $name)) {
						unlink($path . '/' . $name);
						$this->Client_model->delete_double_attachs($path.'/'.$name);
					}
					$_FILES['file']['name'] = $name;
					$_FILES['file']['type'] = $_FILES['lawyer_docs']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['lawyer_docs']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['lawyer_docs']['error'][$i];
					$_FILES['file']['size'] = $_FILES['lawyer_docs']['size'][$i];
					$config['remove_spaces'] = true;
					$config['upload_path'] = $path;
					$config['allowed_types'] = 'doc|docx|pdf|jpg|jpeg|png';
					$config['max_size'] = '7800000';
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('file')) {
						/*$upload_error = array('error' => $this->upload->display_errors());
						var_dump($upload_error);
						die(); */
					} else {

						$insert_data = array('client_attach_client_id' => $client_id, 'c_attach_type' => 'Anwalt', 'client_attach_path' => $path.'/'.$name);
						$this->Client_model->insert_loan_attachment($insert_data);
					}
				}
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_lawyer_attachment(){
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			$path = $this->input->post('path');
			$path = my_cryption($path, 'd', 'hco_url');

			if(is_file($path) == true){
				unlink($path);
			}

			$this->Client_model->delete_double_attachs($path);
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function ajax_request($type, $loan = ''){
		if($type == 'get_loan_history'){
			$loan = my_cryption($loan, 'd', 'hco_url');
			$loan_text = $this->Client_model->get_loan_by_id_custom('client_loan_text', $loan)->client_loan_text;

			$loan_data = $this->Client_model->get_loan_history($loan);

			$result = '
						<h3>Notizen von Berater</h3>
						<p>'.$loan_text.'</p>
						<hr>
        				<table class="metadata_tr table datatable table-striped datatable-buttons">
							<tr class="metadata_tr">
								<th>Datum</th>
								<th>Status</th>
								<th>Notizen</th>
								<th>Eingefügt von</th>
							</tr>
        	';

			foreach($loan_data as $loan){
				$result .= '
        				<tr class="metadata_tr">
        					<td>'.date("d.m.Y", strtotime($loan->loan_history_timestamp)).'</td>
        					<td>'.$loan->loan_history_message.'</td>
        					<td>'.$loan->loan_history_notes_blank.'</td>
        					<td>'.$loan->user_firstname.' '.$loan->user_lastname.'</td>
        				</tr>
        				';
			}


			$result .= '</table>';

			echo $result;
		}

		if($type == 'show_loan_history'){
			$loan_history = $this->Client_model->get_loan_history($loan);

			$response = '';

			foreach($loan_history as $history){
				$response .= $history->loan_history_notes;
				$response .= '<hr>';
			}

			echo $response;
		}

		if($type == 'show_case_history'){
			$case_id = my_cryption($loan, 'd', 'hco_url');
			$case_history = $this->Client_model->get_case_history($case_id);

			$response = '<div class="table-responsive">
							<table class="table">';
			$response .= '
				<tr>
					<th>Datum</th>
					<th>Status</th>
					<th>Notizen</th>
					<th>Eingefügt von</th>
				</tr>
			';
			if($case_history){
				foreach($case_history as $history){
					$response .= '
						<tr>
							<td>'.date("d.m.Y, H:i", strtotime($history->l_help_history_timestamp)).'</td>
							<td>Statusänderung zu: <b>'.$history->l_status_name.'</b></td>
							<td>'.$history->l_help_history_notes.'</td>
							<td>'.$history->user_firstname.' '.$history->user_lastname.'</td>
						</tr>
					';
				}
			}
			$response .= '</table>
					</div>';

			echo $response;
		}

	}
}
