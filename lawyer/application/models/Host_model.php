<?php
/**
 * Created by PhpStorm.
 * User: Amar Moufti
 * Date: 31.07.2017
 * Time: 12:48
 */

class Host_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function get_cars() {
        return $this->db
            ->join('car_make','car_make.c_make_id = halal_auto.h_car_make')
            ->join('car_model','car_model.c_model_id = halal_auto.h_car_model')
            ->join('users','users.user_id = halal_auto.h_car_InsertedBy')
            ->join('clients','clients.client_id = halal_auto.h_car_belongsto','left')
            ->get('halal_auto')
            ->result_object();
    }

    public function get_car_by_id( $car_id ){
        return $this->db
            ->join('car_make','car_make.c_make_id = halal_auto.h_car_make')
            ->join('car_model','car_model.c_model_id = halal_auto.h_car_model')
            ->join('users','users.user_id = halal_auto.h_car_InsertedBy')
            ->join('clients','clients.client_id = halal_auto.h_car_belongsto','left')
            ->where('h_car_id', $car_id)
            ->get('halal_auto')
            ->row_object();
    }

    public function get_attachments_by_id( $car_id ){
        return $this->db->where('hc_attach_car_id', $car_id)->get('halal_auto_attachments')->result_object();
    }

    public function insert_car($table, $data){
        $this->db->insert($table, $data);
        return  $this->db->insert_id();
    }


    public function get_makes() {
        return $this->db->get('car_make')->result_object();
    }

    public function get_models_by_make_id( $id ) {
        return $this->db->where('c_model_make_id', $id)->get('car_model')->result_object();
    }

    public function update_car($data, $car_id) {
        $this->db->where('h_car_id', $car_id);
        $this->db->update('halal_auto', $data);
    }

    public function get_car_column_by_id($column, $id){
        return $this->db->select($column)->where('h_car_id', $id)->get('halal_auto')->row_object();
    }

    public function insert_car_attachment( $data ) {
        $this->db->insert('halal_auto_attachments', $data);
    }

    public function  delete( $car_id ) {
        $this->db->where('h_car_id', $car_id);
        $this->db->delete('halal_auto');
        $this->db->where('hc_attach_car_id', $car_id);
        $this->db->delete('halal_auto_attachments');
    }

    public function get_contract_by_id( $id ) {
        return $this->db
            ->join('clients','clients.client_id = car_sales.car_sale_client_id')
            ->join('users','users.user_id = car_sales.car_sale_user_id')
            ->join('halal_auto','halal_auto.h_car_id = car_sales.car_sale_car_id')
            ->join('car_make','car_make.c_make_id = halal_auto.h_car_make')
            ->join('car_model','car_model.c_model_id = halal_auto.h_car_model')
            ->where('car_sale_car_id', $id)
            ->get('car_sales')
            ->row_object();
    }

    public function get_seller_rates( $sale_id, $type = null ) {
        $this->db->where('car_pay_sale_id', $sale_id);
        if( $type != null){
            $this->db->where('car_pay_paid',$type);
        }
        return $this->db->get('car_pays') ->result_object();
    }

    public function insert_car_sale($data, $table ) {
        $this->db->insert($table, $data);
        return  $this->db->insert_id();
    }

    public function delete_attachment_by_id( $attach_id ){
        $this->db->where('hc_attach_id', $attach_id);
        $this->db->delete('halal_auto_attachments');
    }

    public function get_searched_notifications( $data ) {

        $features       = explode( ',', $data['h_car_features'] );
        $where = "AND ( ";
        foreach ($features as $feature) {
            $where .= " car_search_features = '%".$feature."%' OR ";
        }
        $where .=' 1=1 )';

        $query = " SELECT car_search_belongs_to FROM car_search ";
        $query .= " WHERE car_search_mailing = 'true' ";
        $query .= " AND car_search_make = ". $data['h_car_make'];
        $query .= " AND car_search_model = " .$data['h_car_model'];
        $query .= " AND car_search_condition LIKE '%". $data['h_car_condition']. "%' ";
        $query .= " AND car_search_from_year <= '". date('Y',strtotime($data['h_car_registration']))."' " ;
        $query .= " AND car_search_until_year >= '". date('Y',strtotime($data['h_car_registration']))."' " ;
        $query .= " AND car_search_vehicle LIKE '%". $data['h_car_vehicle'] . "%' ";
        $query .= " AND car_search_transmission LIKE '%". $data['h_car_transmission']. "%' ";
        $query .= " AND car_search_from_coast <= '". $data['h_car_price'] . "'";
        $query .= " AND car_search_until_coast >= '". $data['h_car_price']."' ";
        $query .= " AND car_search_from_kilometer <= '". $data['h_car_kilometer'] . "' ";
        $query .= " AND car_search_until_kilometer >= '". $data['h_car_kilometer']."' ";
        $query .= $where;
        return $query =  $this->db->query( $query )->result_object();

    }

}
