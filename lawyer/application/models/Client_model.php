<?php
class Client_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	public function insert_new_lead($data,$insurances){
		$this->db->insert("clients",$data);
		$this->db->insert("clients",$insurances);
	
	}

	public function get_client_insurances(){
		return $this->db
			->select('*')
			->from('insurance')
			->join("clients","clients.client_insurance_id = insurance.insurance_id")
			->get()
			->result_object();
	}

	public function insert_external_notice($notice){
		$this->db->insert("client_notes_external",$notice);
		
	}
	public function edit_lead($data,$client_id){
		$this->db->where("client_id",$client_id);
		$this->db->update("clients",$data);
	
	}
	
	public function get_clients_with_loans(){
		return $this->db
			->select('*, count(client_loan_id) as loans')
			->from('clients')
			->join('client_loans', 'client_loans.client_loan_client_id = clients.client_id')
			->where('client_delete', 0)
			->group_by('client_id')
			->get()
			->result_object();
	}

	public function get_new_clients_with_loans($date){
		return $this->db
			->select('client_loans.*, client_loan_status.*, clients.client_firstname, clients.client_lastname, clients.client_email,
					clients.client_mobile, clients.client_phone,clients.client_id, clients.client_insurance_id')
			->from('client_loans')
			->join('clients', 'clients.client_id = client_loans.client_loan_client_id')
			->join('client_loan_status', 'client_loan_status.c_loan_status_id = client_loans.client_loan_status')
			->where('client_delete', 0)
			->where('unix_timestamp(client_loan_timestamp) >= '.strtotime($date->format('Y-m-d')))
			->order_by('client_loan_update', 'ASC')
			->get()
			->result_object();
	}

	public function get_client_by_id($client_id){
		return $this->db
			->select('*')
			->from('clients')
			->where('client_id', $client_id)
			->get()
			->row_object();
	}

	public function get_client_loans($client_id){
		return $this->db
			->select('*')
			->from('client_loans')
			->where('client_loan_client_id', $client_id)
			->get()
			->result_object();
	}

	public function get_loans_with_clients(){
		return $this->db
			->select('*')
			->from('clients')
			->where('client_delete', 0)
			->order_by('client_id')
			->get()
			->result_object();
	}

	public function get_loan_status_list(){
		return $this->db
			->select('*')
			->from('client_loan_status')
			->get()
			->result_object();
	}

	public function update_loan_by_id($id, $data){
		$this->db->where('client_loan_id', $id);
		$this->db->update('client_loans', $data);
	}

	public function update_case_by_id($id, $data){
		$this->db->where('lawyer_help_id', $id);
		$this->db->update('client_lawyer_help', $data);
	}

	public function insert_client_loan_history($data){
		$this->db->insert('client_loan_history', $data);
	}

	public function get_loan_by_id_custom($query, $id){
		return $this->db
			->select($query)
			->from('client_loans')
			->where('client_loan_id', $id)
			->get()
			->row_object();
	}

	public function get_loan_history($id){
		return $this->db
			->select('*')
			->from('client_loan_history')
			->where('loan_history_included_loan', $id)
			->join('client_loan_status', 'client_loan_status.c_loan_status_id = client_loan_history.loan_history_status')
			->join('users', 'users.user_id = client_loan_history.loan_history_added_by')
			->order_by('loan_history_timestamp', 'ASC')
			->get()
			->result_object();
	}

	public function get_loan_status_by_id($id){
		return $this->db
			->select('*')
			->from('client_loan_status')
			->where('c_loan_status_id', $id)
			->get()
			->row_object();
	}

	public function insert_external_notes($notice){
		$this->db->insert('client_notes_external', $notice);
	}

	public function get_client_external_notes($client_id){
		return $this->db
			->select('*')
			->from('client_notes_external')
			->where('c_notes_external_client_id', $client_id)
			->get()
			->result_object();
	}

	public function get_client_checkbox($client_id){
		return $this->db
			->select("client_criteria, client_interest, client_insurance_id")
			->from("clients")
			->where("client_id",$client_id)
			->get()
			->row_object();
	}

	public function insert_loan_attachment($data){
		$this->db->insert('client_attachments', $data);
	}

	public function get_client_attachments($client_id, $case='Kredit'){
		return $this->db
			->select('*')
			->from('client_attachments')
			->where('client_attach_client_id', $client_id)
			->where('c_attach_type', $case)
			->get()
			->result_object();
	}

	public function delete_double_attachs($path){
		$this->db->where('client_attach_path', $path);
		$this->db->delete('client_attachments');
	}

	public function check_client_exist($id, $firstname){
		return $this->db
			->select('*')
			->from('clients')
			->where('client_id', $id)
			->where('client_firstname', $firstname)
			->get()
			->row_object();
	}

	public function delete_client_attachment($attach_id){
		$this->db->where('client_attach_id', $attach_id);
		$this->db->delete('client_attachments');
	}

    public function get_clients_by_job_type($type){
        return $this->db
            ->select('*')
            ->from('clients')
            ->where('client_job_type', $type)
            ->get()
            ->result_object();
    }

	public function get_general_lawyer_clients(){
		return $this->db
			->select('*')
			->from('client_lawyer_help')
			->join('clients', 'clients.client_id = lawyer_help_client_id')
			->get()
			->result_object();
	}

	public function get_lawyer_clients_by_types($types){
		return $this->db
			->select('*')
			->from('client_lawyer_help')
			->join('clients', 'clients.client_id = lawyer_help_client_id')
			->join('lawyer_help_types', 'l_help_type_id = lawyer_help_type')
			->join('lawyer_status', 'l_status_id = lawyer_help_status')
			->where_in('lawyer_help_type', $types)
			->get()
			->result_object();
	}

	public function get_case_by_id($case){
		return $this->db
			->select('*')
			->from('client_lawyer_help')
			->join('clients', 'clients.client_id = lawyer_help_client_id')
			->join('lawyer_help_types', 'lawyer_help_types.l_help_type_id = lawyer_help_type')
			->join('users', 'users.user_id = lawyer_help_added_by')
			->where('lawyer_help_id', $case)
			->get()
			->row_object();
	}

	public function get_client_external_notes_by_type($client_id, $type){
		return $this->db
			->select('*')
			->from('client_notes_external')
			->join('users', 'users.user_id = c_notes_external_added_by')
			->where('c_notes_external_client_id', $client_id)
			->where('c_notes_external_reason', $type)
			->get()
			->result_object();
	}

	public function get_lawyer_status(){
		return $this->db
			->select('*')
			->from('lawyer_status')
			->get()
			->result_object();
	}

	public function insert_lawyer_data($table, $data){
		$this->db->insert($table, $data);
	}

	public function get_case_history($case){
		return $this->db
			->select('*')
			->from('lawyer_help_history')
			->join('users', 'user_id = l_help_history_added_by')
			->join('lawyer_status', 'l_status_id = l_help_history_status')
			->where('l_help_history_case_id', $case)
			->order_by('l_help_history_timestamp', 'DESC')
			->get()
			->result_object();
	}
}
