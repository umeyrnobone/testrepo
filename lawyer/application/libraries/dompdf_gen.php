<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dompdf_gen {
		
	public function __construct() {
		
		require_once APPPATH.'third_party/dompdf/dompdf_config.inc.php';
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new Dompdf($options);

        $CI =get_instance();
        $CI->dompdf = $pdf;
		
	}
	
}