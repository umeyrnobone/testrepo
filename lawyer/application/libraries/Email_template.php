<?php
/**
 * Created by PhpStorm.
 * User: Amar Moufti
 * Date: 16.12.2017
 * Time: 13:51
 */

class Email_template {

    private $ci;


    public function __construct(){
        $this->ci =& get_instance();
    }

    public function send_confirm_booking( $booking_id ) {

        $this->ci->load->model('Mecca_model');
        
        $data['booking_info'] = $this->ci->Mecca_model->get_booking_by_id($booking_id );

        
        $message = '';
        $message = $this->ci->load->view('templates/confirm_email_templete', $data, true); 
       
        $config = $this->get_config('General'); 
        $this->ci->load->library('email',$config);

        $this->ci->email->set_newline("\r\n");
        $this->ci->email->attach($_SERVER["DOCUMENT_ROOT"].'/pdf/checkliste_help2hadj.pdf');
        $this->ci->email->attach($_SERVER["DOCUMENT_ROOT"].'/pdf/schrittfuerschritt_help2hadj.pdf');
        $this->ci->email->from($config['smtp_user'], $config['the_subject']);
        $this->ci->email->to($data['booking_info']->client_email);// change it to Clients
        $this->ci->email->reply_to($config['reply'], $config['the_subject']);
        $this->ci->email->subject('Buchungsbestätigung: '.$data['booking_info']->m_trip_title );
        $this->ci->email->message($message);
        if($this->ci->email->send()){
           $this->send_email_to_admin($data['booking_info']);
        }else{
           echo $this->ci->email->print_debugger();
        }
    }

    public function send_to_moslem_reise( $code, $trip_id ){

        $trip_info = $this->ci->Mecca_model->get_trip_by_id( $trip_id);

        $message = '<h3> As Salam wa Alaikom <h3>';
        $message .= '<p> Passagiere Information des Flug '. $trip_info->m_trip_title .' auf der Seite ';
        $message .= '<a href="http://myadenya.de/External/moslem_trip/'.$code.'">Hier Klicken</a></p>';


        $config = $this->get_config('General'); 
        $message .= $this->add_signatur();

        $this->ci->load->library('email', $config);

        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($config['smtp_user'], $config['the_subject']);
        $this->ci->email->to('amar.moufti@hotmail.com');// change it to yours
        $this->ci->email->reply_to('amar.moufti@hotmail.com', $config['the_subject']);
        $this->ci->email->subject('Passagiere für das Flug:' .$trip_info->m_trip_title);
        $this->ci->email->message($message);
        $this->ci->email->send();
    }

/*    public function send_to_passangers($to, $subject, $message ){

        $this->ci->load->library('email');
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($config['smtp_user'], $config['the_subject']);
        $this->ci->email->to('amar.moufti@hotmail.com');// change it to yours
        $this->ci->email->subject($subject);
        $this->ci->email->message($message.$to);
        $this->ci->email->send();
    }*/

    public function send_cancelled_email($booking_id, $coast){

        $this->ci->load->model('Mecca_model');
        $booking_info = $this->ci->Mecca_model->get_booking_by_id($booking_id );
        $subject = 'Deine '. $booking_info->m_trip_title .' wurde Storniert';
        $message = 'As Salamu Alaikum wa Rahmetullahi wa Barakatuh, <br>';
        $message .= 'Wie gewünscht wurde deine ' . $booking_info->m_trip_type . ' storniert!';
        $message .= 'Die Stornierungsgebühr beträgt '.$coast.' € </br>' ;
        
        
        
        $config = $this->get_config('General'); 
        $message .= $this->add_signatur();

        $this->ci->load->library('email', $config);
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($config['smtp_user'], $config['the_subject']);
        $this->ci->email->to($booking_info->client_email);// change it to yours
        $this->ci->email->reply_to($config['reply'], $config['the_subject']);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
        if($this->ci->email->send()){
           $this->send_email_to_admin($booking_info);
        }else{
           echo $this->ci->email->print_debugger();
        }
    }
    
    // we add third paramater to generate pdf from gmail
    public function send_cancelled_email_url($booking_id, $coast){

        $this->ci->load->model('Mecca_model');
        $booking_info = $this->ci->Mecca_model->get_booking_by_id($booking_id );
        
        $id = $this->mycryption($booking_id ,'e');
         
        $url = 'https://myadenya.de/PDF/create_h2h_bill_cancel_payment_mail/'.$id; 

        $id = $this->mycryption($booking_id ,'e');
        
        $subject = 'Deine '. $booking_info->m_trip_title .' wurde Storniert';
        $message = 'As Salamu Alaikum wa Rahmetullahi wa Barakatuh, <br>';
        $message .= 'Wie gewünscht wurde deine ' . $booking_info->m_trip_type . ' storniert!';
        $message .= 'Die Stornierungsgebühr beträgt '.$coast.' € </br>' ;
        $message .= '<br>
                        mit dem nachfolgenden Link kannst Du Deine Rechnung  Storniert herunterladen. 
                        <br> <br><a href="'.$url.'">>>> PDF-Rechnung-Storniert <<< </a> <br><br><br> ';
         
        $config = $this->get_config('General'); 
        $message .= $this->add_signatur(); 

        $this->ci->load->library('email', $config);
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($config['smtp_user'], $config['the_subject']);
        $this->ci->email->to($booking_info->client_email);// change it to yours
        $this->ci->email->reply_to($config['reply'], $config['the_subject']);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
        $this->ci->email->send();
    }
    
    
 

    public function send_email_to_admin( $booking_info) {


        $message ='';
        $message .= '
        <table>
            <tr>
                <td colspan="2"><b style="font-size: 24px">Kundendaten:</b></td>
            </tr>
          
            <tr>
                <td>Anrede: </td>
                <td>'. $booking_info->client_gender.'</td>
            </tr>
            <tr>
                <td>Vorname:</td>
                <td>'. $booking_info->client_firstname .'</td>
            </tr>
            <tr>
                <td>Nachname:</td>
                <td>'. $booking_info->client_lastname .'</td>
            </tr>
            <tr>
                <td>Strasse:</td>
                <td>'. $booking_info->client_street .'</td>
            </tr>
            <tr>
                <td>PLZ, Ort:</td>
                <td>'. $booking_info->client_zipcode .' '. $booking_info->client_city .'</td>
            </tr>
            <tr>
                <td>Telefone:</td>
                <td>'. $booking_info->client_phone .'</td>
            </tr>
            <tr>
                <td>Mobile:</td>
                <td>'. $booking_info->client_mobile .'</td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>'. $booking_info->client_email .'</td>
            </tr>
            <tr>
               <td>Verwendungszweck</td>        
               <td>'. $booking_info->booking_payment_reason.'</td>
            </tr>
            <tr>
                <td>Gebucht von </td>
                <td> CRM </td>
            </tr>
        </table>';

        $message .= '
        <table width="100%">
            <tr>
                <td>1.</td>
                <td colspan="5"><b style="font-size: 20px">Reisepaket : '.$booking_info->m_trip_type. ' / '  .$booking_info->m_trip_title . ' </td>
            </tr>
          
            <tr>
                <td></td>
                <td>Reise Datum</td>
                <td>Reise Typ</td>
                <td>Reisekategorie</td>
                <td>Zimmer Belegung</td>
                <td>Rail and Fly :</td>        
            </tr>
            <tr>
                <td></td>
                <td>'. date('d.m.Y', strtotime($booking_info->m_trip_go_departure_date)) .'</td>
                <td>'. $booking_info->m_trip_type.'</td>
                <td>'. $booking_info->booking_trip_category.'</td>
                <td>'. $booking_info->booking_room_occupancy.' Bett / Zimmer</td>
                <td>'. $booking_info->booking_rail_fly.'</td>
            </tr>
        </table>
        ';

        if( $booking_info->m_trip_organization_id == 1){

            $this->ci->email->set_newline("\r\n");
            $this->ci->email->from('amar.moufti@hotmail.com');
            $this->ci->email->to('amar.moufti@hotmail.com');// change it to Clients
            $this->ci->email->reply_to('amar.moufti@hotmail.com');
            $this->ci->email->subject('Neue Buchung'. $booking_info->m_trip_title);
            $this->ci->email->message($message);
            $this->ci->email->send();

       } else if(($booking_info->m_trip_organization_id == 2)  || ($booking_info->m_trip_organization_id == 4)){

            $this->ci->email->set_newline("\r\n");
            $this->ci->email->from( 'info@bakkah-reisen.de' );
            $this->ci->email->to('info@bakkah-reisen.de');// change it to Clients
            $this->ci->email->reply_to('info@bakkah-reisen.de');
            $this->ci->email->subject('Neue Buchung'. $booking_info->m_trip_title);
            $this->ci->email->message($message);
            $this->ci->email->send();

        } else if($booking_info->m_trip_organization_id == 3){

            $this->ci->email->set_newline("\r\n");
            $this->ci->email->from( 'info@umra-hadsch.de' );
            $this->ci->email->to('info@umra-hadsch.de');// change it to Clients
            $this->ci->email->reply_to('info@umra-hadsch.de');
            $this->ci->email->subject('Neue Buchung'. $booking_info->m_trip_title);
            $this->ci->email->message($message);
            $this->ci->email->send();

        }

    }

    public function send_payment_confirm($payment_info , $paid) {

        $this->ci->load->model('Mecca_model');
        $payments_list = $this->ci->Mecca_model->get_payment_by_booking_id($payment_info->booking_id);

        $message = '';
        $message .= '<p>Lieber eMail-Empfaenger, falls diese Mail nicht fuer Dich bestimmt ist, leite sie bitte umgehend weiter! </p>';
        $message .= '<p style="font-size: 20px"> Bitte bei Antworten/Anfragen immer angeben: Vorgang '. $payment_info->booking_id .'  </p>';
        $message .= '<p style="font-size: 20px">  Bismillah Ar-rahman Ar-rahim </p>';

        if($payment_info->client_gender == 'Frau'){
            $gender = 'Schwester ';
        }
        if($payment_info->client_gender == 'Herr') {
            $gender = 'Bruder ';
        }
        $message .= '<h3> As Salam Alaikom '.$gender. $payment_info->client_firstname.' '.$payment_info->client_lastname.'</h3>';

        $to_pay = ($payment_info->booking_sum_price + $payment_info->booking_extra_payment) -  $payment_info->booking_discount  - $paid;
        
        

        $message .='
        
        <table width="100%">
            <tr>
                <td colspan="3"><b style="font-size: 24px">Zahlungs Information :</td>
            </tr>
            <tr>
                <td>Gesammte Reise Kosten</td>
                <td>Rabatt</td>
                <td>Gezahlt</td>
                <td>Offene zahlung</td>        
            </tr>
            ';

        $message .= '
            <tr>
                <td>'. $payment_info->booking_sum_price .' €</td>
                <td>'. $payment_info->booking_discount .' €</td>
                <td>'. $paid.' €</td>
                <td>'.$to_pay =  $to_pay >=0 ? $to_pay:''  .' €</td>
            </tr>
        ';
        $message .= '</table><br><br>';

        $message .='
        
        <table width="50%">
            <tr>
                <td colspan="2"><b style="font-size: 24px">Zahlungstabelle :</td>
            </tr>
            <tr>
                <td>Anzahlungs Summe</td>
                <td>Eingangsdatum</td>
            </tr>

            ';

        foreach ($payments_list  as $payment) {
            $message .= '
            <tr>
                <td>'. $payment->b_payment_amount.' €</td>
                <td>'. date('d.m.Y',  strtotime($payment->b_payment_paid_date)).' </td>
            </tr>
        ';
        }

        $message .= '</table><br><br>';


        $config   = $this->get_config('General');
        $message .= $this->add_signatur();

        $this->ci->load->library('email', $config);

        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from('help2hadj@gmail.com', 'Help2Hadj'); // change it to yours
        $this->ci->email->to($payment_info->client_email);// change it to yours
        $this->ci->email->subject('Deine Statusübersicht');
        $this->ci->email->message($message);
        $this->ci->email->send();

    }
    
    public function send_confirm_termine( $booking_id ,$note) {
        
        $this->ci->load->model('Mecca_model');
        $data['booking_info']                  = $this->ci->Mecca_model->get_booking_by_id($booking_id );
        $data['current_pdf_termine']           = $this->ci->Mecca_model->get_booking_attachments_specific_by_id($booking_id);
        
        $message  = $note.' '.date("d.m.Y H:i:s",strtotime($data['booking_info']->booking_fingerprint_date)); 
        
       
        $config = $this->get_config('General'); 
        
        $this->ci->load->library('email',$config);

        $this->ci->email->set_newline("\r\n");
        
        $this->ci->email->attach($data['current_pdf_termine']->b_attach_path);
        
        
        $this->ci->email->from($config['smtp_user'], $config['the_subject']);
        $this->ci->email->to($data['booking_info']->client_email);// change it to Clients
        $this->ci->email->reply_to($config['reply'], $config['the_subject']);
        $this->ci->email->subject('Fingerabdruck Termine: '.$data['booking_info']->m_trip_title );
        $this->ci->email->message($message);
        if($this->ci->email->send()){
            
           $this->send_email_to_admin($data['booking_info']);
           
        }else{
            
           echo $this->ci->email->print_debugger();
           
        }
    }
     
    
    
      public function get_config( $config_type ){

        $config = array(
            'protocol'  => 'pop3',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => '587',
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'wordwrap'  => 'TRUE',
            'newline'   => '\r\n',
            'allowed_types'=> 'doc|pdf'
        );

        if( $config_type == 'GoMecca'){

            $config['the_subject'] = 'GoMecca';
            $config['smtp_user'] = 'info.umrah.hadj@gmail.com';
            $config['smtp_pass'] = 'umrahaj13579';
            $config['reply']    = '';
            $config['signatur' ] =  '';


        }

        if( $config_type == 'IME'){

            $config['the_subject'] = 'IME Reisen';
            $config['smtp_user'] = 'ime.reise@gmail.com';
            $config['smtp_pass'] = '2468!me2468!';
            $config['reply']    = 'info@umra-hadsch.de';
            $config['signatur' ] = '<p>IME-Reisen</p>

                            <p>Carl-Reuther- Str. 1</p>
                            
                            <p>D - 68305 Mannheim</p>
                            
                            <p>Mobil + 49 157-73761183</p>
                            
                            <p>E-Mail: info@umra-hadsch.de</p>
                            
                            <p>www.umra-hadsch.de</p>';

        }

        if( $config_type == 'Bakkah'){

            $config['the_subject'] = 'Bakkah Reisen';
            $config['smtp_user'] = 'bakkah.reise@gmail.com';
            $config['smtp_pass'] = '2468b@kk@2468!';
            $config['reply']    = 'info@bakkah-reisen.de';
            $config['signatur' ] = '<p>Bakkah Reisen</p>

                            <p>Carl-Reuther- Str. 1</p>
                            
                            <p>D - 68305 Mannheim</p>
                            
                            <p>Tel. 0176-35711896</p>
                            
                            <p>E-Mail: info@bakkah-reisen.de</p>
                            
                            <p>www.bakkah-reisen.de</p>';
                            
        } elseif($config_type == 'General') {
            
            
            $config['the_subject'] = 'Help2Hadj';
            $config['smtp_user']   = 'help2hadj@gmail.com';
            $config['smtp_pass']   = 'Waikiki1501';
            $config['reply']       = 'help2hadj@gmail.com';
            
      }

        return $config;

    }
    
    public function add_signatur() {
        return  '<br><br>
                    Mit freundlichen Grüßen <br>
Thank you and best regards <br>
السلام عليكم ورحمة الله وبركاته 
<br>
​Help2Hadj
<br><br>
<img src="https://bakkah-reisen.com/h2h.png" style="width: 300px" alt="">
<br><br>

Mob. <b>+49 621 74818556</b><br>
E-Mail: ​help2hadj@gmail.com​<br>
<br><br>
Der Inhalt dieser E-Mail ist vertraulich. Falls Sie nicht der angegebene
Empfänger sind oder falls diese E-Mail irrtümlich an Sie adressiert
wurde, verständigen Sie bitte den Absender sofort und löschen Sie die
E-Mail sodann. Das unerlaubte Kopieren sowie die unbefugte Übermittlung
sind nicht gestattet. Die Sicherheit von Übermittlungen per E-Mail kann
nicht garantiert werden. Falls Sie eine Bestätigung wünschen, fordern Sie
bitte den Inhalt der E-Mail als Hardcopy an. 
<br><br>
The contents of this e-mail are confidential. If you are not the named
addressee or if this transmission has been addressed to you in error,
please notify the sender immediately and then delete this e-mail. Any
unauthorized copying and transmission is forbidden. E-mail transmission
cannot be guaranteed to be secure. If verification is required, please
request a hard copy Version.';
    }
    
    
        public function mycryption( $string, $action = 'e' ) {

        // you may change these values to your own
        $secret_key = 'thisIsmySecretKey:)';
        $secret_iv = 'thisIsmySecretIv:)';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }

        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }

        return $output;
    }
}