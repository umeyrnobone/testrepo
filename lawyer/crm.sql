-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 22. Sep 2019 um 12:04
-- Server-Version: 10.4.6-MariaDB
-- PHP-Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `crm`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sp_contracts`
--

CREATE TABLE `sp_contracts` (
`s_contract_id` int(11) NOT NULL,
`s_contract_client_id` int(11) NOT NULL,
`s_contract_added_by` int(11) NOT NULL,
`s_contract_adviser_id` int(11) NOT NULL,
`s_contract_monthly_payments` decimal(8,2) NOT NULL,
`s_contract_years` tinyint(3) NOT NULL,
`s_contract_signature_date` date NOT NULL,
`s_contract_begin_date` date NOT NULL,
`s_contract_payment_date` date NOT NULL,
`s_contract_payment_rates` varchar(15) NOT NULL,
`s_contract_discount` decimal(6,2) DEFAULT NULL,
`s_contract_discount_euro` decimal(6,2) DEFAULT NULL,
`s_contract_status` varchar(20) NOT NULL DEFAULT 'Aktive',
`s_contract_notice` text DEFAULT NULL,
`s_contract_invest_id` int(11) DEFAULT NULL,
`s_contract_transaction_code` varchar(20) DEFAULT NULL,
`s_contract_units` decimal(6,2) NOT NULL,
`s_contract_euro_unit` decimal(6,2) NOT NULL,
`s_contract_adviser_provision` decimal(6,2) NOT NULL,
`s_contract_fees` decimal(8,2) NOT NULL,
`s_contract_fees_percent` decimal(6,2) NOT NULL,
`s_contract_sms_confirmation` tinyint(4) DEFAULT 0,
`s_contract_child_sp` tinyint(4) DEFAULT 0,
`s_contract_child_firstname` varchar(50) DEFAULT NULL,
`s_contract_child_lastname` varchar(50) DEFAULT NULL,
`s_contract_child_birthdate` date DEFAULT NULL,
`s_contract_authorized_fname` varchar(50) DEFAULT NULL,
`s_contract_authorized_lname` varchar(50) DEFAULT NULL,
`s_contract_authorized_birthdate` date DEFAULT NULL,
`s_contract_authorized_address` varchar(250) DEFAULT NULL,
`s_contract_authorized_zipcode` int(6) DEFAULT NULL,
`s_contract_authorized_city` varchar(40) DEFAULT NULL,
`s_contract_printed` tinyint(4) NOT NULL DEFAULT 0,
`s_contract_sent_post` tinyint(4) NOT NULL DEFAULT 0,
`s_contract_timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sp_options`
--

CREATE TABLE `sp_options` (
`s_option_id` int(3) NOT NULL,
`s_option_key` varchar(50) NOT NULL,
`s_option_value` varchar(50) NOT NULL,
`s_option_foreign_id` int(3) DEFAULT NULL,
`s_option_description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `sp_options`
--

INSERT INTO `sp_options` (`s_option_id`, `s_option_key`, `s_option_value`, `s_option_foreign_id`, `s_option_description`) VALUES
(1, 'buy_gold_fees', '1.7', NULL, 'Gebühren beim Gold kauf von Kunden (Nicht Löschbar)'),
(2, 'sell_gold_fees', '1.6', NULL, 'Gebühren beim Gold Verkauf an Kunde (Nicht Löschbar)'),
(3, 'provision_capital', '2000', NULL, 'Summe der Maximale Storno Kapital betrag  (Nicht Löschbar)'),
(4, 'provision_percent', '10', NULL, 'Wieviel % werden aus provision für die Storno abgezogen (Nicht Löschbar)'),
(5, 'contract_fees', '6.72', NULL, 'Gebühren eines Neues Vertrag (Nicht Löschbar)'),
(6, 'direct_discount', '11.4', NULL, 'Gebühren eines Neues Vertrag bei sofort Zhalung(Nicht Löschbar)'),
(7, 'monthly_rate_discount', '16.36', NULL, 'Rabatt, der bei Ratenzahlung gültig wird (nicht löschbar)'),
(19, 'silver_amount', '431.6221', NULL, 'Silberkapital in gramm (Nicht Löschbar)'),
(38, 'silent_units', '7', 1, 'Stiller Beitrag von Berater'),
(41, 'level_units', '20', 1, 'Einheitsstuffe von Berater'),
(42, 'silent_units', '5', 27, 'Stiller Beitrag von Berater'),
(43, 'silent_units', '3', 25, 'Stiller Beitrag von Berater'),
(44, 'silent_units', '2', 43, 'Stiller Beitrag von Berater'),
(45, 'level_units', '15', 3, 'Einheitsstuffe von Berater'),
(46, 'level_units', '17', 5, 'Einheitsstuffe von Berater'),
(47, 'silent_units', '3', 32, 'Stiller Beitrag von Berater'),
(48, 'level_units', '0', 32, 'Einheitsstuffe von Berater');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sp_payments`
--

CREATE TABLE `sp_payments` (
`s_payment_id` int(11) NOT NULL,
`s_payment_contract_id` int(11) NOT NULL,
`s_payment_amount` decimal(6,2) NOT NULL,
`s_payment_gram` decimal(8,4) NOT NULL,
`s_payment_expected_date` date NOT NULL,
`s_payment_date` date DEFAULT NULL,
`s_payment_status` varchar(50) NOT NULL DEFAULT 'Offen',
`s_payment_reason` varchar(50) NOT NULL,
`s_payment_notice` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sp_silent_units`
--

CREATE TABLE `sp_silent_units` (
`s_silent_unit_id` int(11) NOT NULL,
`s_silent_unit_user_id` int(11) NOT NULL,
`s_silent_unit_contract_id` int(11) NOT NULL,
`s_silent_unit_commission` decimal(8,2) NOT NULL,
`s_silent_unit_amount` decimal(5,2) NOT NULL,
`s_silent_unit_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `sp_contracts`
--
ALTER TABLE `sp_contracts`
ADD PRIMARY KEY (`s_contract_id`);

--
-- Indizes für die Tabelle `sp_options`
--
ALTER TABLE `sp_options`
ADD PRIMARY KEY (`s_option_id`);

--
-- Indizes für die Tabelle `sp_payments`
--
ALTER TABLE `sp_payments`
ADD PRIMARY KEY (`s_payment_id`);

--
-- Indizes für die Tabelle `sp_silent_units`
--
ALTER TABLE `sp_silent_units`
ADD PRIMARY KEY (`s_silent_unit_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
